import {
  PRODUCT, PRODUCT_ID, DETAIL_PRODUCT,
} from "./type";
import { USER_LOGIN, GET_TOKEN, REMOVE_TOKEN } from "./type";
import { logistical } from '../../logistical'
import AsyncStorage from "@react-native-community/async-storage";
import { Alert } from "react-native";



export function getProductsData(data) {
  return {
    type: PRODUCT,
    getProductsData: data,
  };
}

export const AllProduct = (catId, compId, data, navigation) => dispatch => {

  return new Promise(async (resolve, reject) => {

    const response = await logistical.get('/product-categories', data);

    if (response.status == '1') {
      // AsyncStorage.setItem("login",JSON.stringify(response.data.token));
      console.log("========================================>", response.data.productCategories);

      dispatch({
        type: PRODUCT,
        getCartData: response.data.productCategories,
      });

      // navigation.navigate('ServicesScreen');
      //Alert.alert(response.response[0])
      resolve(response);

    }

    else {
      console.log('errrrrrrrrrrrrrrr>>>>>>>>>>>>>>>')
      reject(response);
    }
  });
};
export const AllParticularProduct = (idProduct, data, navigation) => dispatch => {

  return new Promise(async (resolve, reject) => {

    const response = await logistical.get('/products/' + idProduct, data);
    // const response = await logistical.get('/service-categories/' + Id, data);
    if (response.status == '1') {
      // AsyncStorage.setItem("login",JSON.stringify(response.data.token));
      // console.log("========================================>", response.data.products);

      dispatch({
        type: PRODUCT_ID,
        productData: response.data.products,
      });
      // navigation.navigate('ServicesScreen');
      //Alert.alert(response.response[0])
      resolve(response);

    }

    else {
      console.log('errrrrrrrrrrrrrrr>>>>>>>>>>>>>>>')
      reject(response);
    }
  });
};
export const ParticularDetailProduct = (CategoryId, particular_id, data, navigation) => dispatch => {

  return new Promise(async (resolve, reject) => {

    const response = await logistical.get('/products-details/' + CategoryId + '/' + particular_id, data);

    console.log('response show ', response)
    // const response = await logistical.get('/service-categories/' + Id, data);
    if (response.status == '1') {
      // AsyncStorage.setItem("login",JSON.stringify(response.data.token));
      console.log("==================================== producttcttct====>", response.data.product);

      dispatch({
        type: DETAIL_PRODUCT,
        detailProduct: response.data.product,
      });
      // navigation.navigate('ServicesScreen');
      //Alert.alert(response.response[0])
      resolve(response);

    }

    else {
      console.log('errrrrrrrrrrrrrrr>>>>>>>>>>>>>>>')
      reject(response);
    }
  });
};




import {
  BOOKING, DASHBOARD_BOOKING, BOOKING_LIST, BOOKING_DETAIL_LIST, PRODUCTBOOKING, SERVICEBOOKING, PRODUCT_BOOKING_DETAIL_LIST, SERVICE_CANCEl
} from "./type";
import { USER_LOGIN, GET_TOKEN, REMOVE_TOKEN } from "./type";
import { logistical } from '../../logistical'
import AsyncStorage from "@react-native-community/async-storage";
import { Alert, ToastAndroid } from "react-native";



export const AllBooking = ( data, navigation ) => dispatch => {
  dispatch( {

    type: 'LOADING',
    payload: true

  } );
  return new Promise( async ( resolve, reject ) => {
    console.log( '>>>>>>', data )
    const response = await logistical.post( '/get-service-booking', data );
    if ( response.status == '1' && response.message == 'Successfull Booking' ) {
      // console.log("========================================>", response.data);

      dispatch( {
        type: BOOKING,
        getBookingData: response.data,
      } );


      Alert.alert(
        'Booking',
        'Press ok to confirm your booking', [{
          text: 'Cancel',
          onPress: () => console.log( 'Cancel Pressed' ),
          style: 'cancel'
        }, {
          text: 'OK',
          onPress: () => navigation.navigate( 'MainHome' )
        },], {
        cancelable: false
      }
      )
      resolve( response );

    }
    else {
      // Alert.alert(response.message)
      //   Alert.alert(response.response[0])

      dispatch( {

        type: 'LOADING',
        payload: false

      } );
      console.log( 'errrrrrrrrrrrrrrr>>>>>>>>>>>>>>>' )
      reject( response );
    }
  } );
};

export const DashboardBooking = ( data, navigation ) => dispatch => {
  dispatch( {
    type: 'LOADING',
    payload: true
  } );
  return new Promise( async ( resolve, reject ) => {

    const response = await logistical.get( '/get-count-bookings', data );

    if ( response.status == '1' ) {

      dispatch( {
        type: DASHBOARD_BOOKING,
        getDashboardData: response.data.data,
      } );
      dispatch( {
        type: 'LOADING',
        payload: false
      } );
      // navigation.navigate('ServicesScreen');
      resolve( response );

    }

    else {
      console.log( 'errrrrrrrrrrrrrrr>>>>>>>>>>>>>>>' )
      reject( response );
    }
  } );
};

export const GetBookingList = ( data, navigation ) => dispatch => {
  dispatch( {
    type: 'LOADING',
    payload: true
  } );
  return new Promise( async ( resolve, reject ) => {

    const response = await logistical.get( '/get-booking-details', data );
    if ( response.status == '1' ) {

      dispatch( {
        type: BOOKING_LIST,
        getBookingListData: response.data.bookings,
      } );
      dispatch( {
        type: 'LOADING',
        payload: false
      } );


      resolve( response );

    }
    else {
      // Alert.alert(response.message)
      // Alert.alert(response.response[0])

      dispatch( {

        type: 'LOADING',
        payload: false

      } );
      console.log( 'errrrrrrrrrrrrrrr>>>>>>>>>>>>>>>' )
      reject( response );
    }
  } );
};


export const GetBookingDetail = ( data, navigation ) => dispatch => {

  dispatch( {
    type: 'LOADING',
    payload: true

  } );
  return new Promise( async ( resolve, reject ) => {

    const response = await logistical.post( '/get-service-booking-details', data );
    console.log( "========================================>", response.data );

    if ( response.status == '1' ) {
      // AsyncStorage.setItem("login",JSON.stringify(response.data.token));
      console.log( "========================================>", response );

      dispatch( {
        type: BOOKING_DETAIL_LIST,
        getBookingDetailData: response.data,
      } );
      resolve( response );

      dispatch( {

        type: 'LOADING',
        payload: false

      } );
    }
    else {
      // Alert.alert(response.message)
      //  Alert.alert(response.response[0])

      dispatch( {

        type: 'LOADING',
        payload: false

      } );
      console.log( 'errrrrrrrrrrrrrrr>>>>>>>>>>>>>>>' )
      reject( response );
    }
  } );
};

export const GetProductBookingDetail = ( data, navigation ) => dispatch => {

  dispatch( {
    type: 'LOADING',
    payload: true

  } );
  return new Promise( async ( resolve, reject ) => {

    const response = await logistical.post( '/get-product-order-detail', data );
    console.log( "========================================>", response.data );

    if ( response.status == '1' ) {
      // AsyncStorage.setItem("login",JSON.stringify(response.data.token));
      console.log( "========================================>", response );

      dispatch( {
        type: PRODUCT_BOOKING_DETAIL_LIST,
        getProductBookingDetailData: response.data,
      } );
      resolve( response );

      dispatch( {

        type: 'LOADING',
        payload: false

      } );
    }
    else {
      // Alert.alert(response.message)
      //  Alert.alert(response.response[0])

      dispatch( {

        type: 'LOADING',
        payload: false

      } );
      console.log( 'errrrrrrrrrrrrrrr>>>>>>>>>>>>>>>' )
      reject( response );
    }
  } );
};
export const CreatingProductCard = ( data, navigation ) => dispatch => {

  return new Promise( async ( resolve, reject ) => {
    console.log( '>>>>>>', data )
    const response = await logistical.post( '/create-product-order', data );
    if ( response.status == '1' && response.message == 'Your Order is Successfull' ) {
      // AsyncStorage.setItem("login",JSON.stringify(response.data.token));
      // console.log("========================================>", response.data);

      dispatch( {
        type: PRODUCTBOOKING,
        productBooking: response.data,
      } );


      Alert.alert(
        'Place Order',
        'Press ok to confirm your booking', [{
          text: 'Cancel',
          onPress: () => console.log( 'Cancel Pressed' ),
          style: 'cancel'
        }, {
          text: 'OK',
          onPress: () => navigation.navigate( 'MainHome' )
        },], {
        cancelable: false
      }
      )
      resolve( response );

    }
    else {
      // Alert.alert(response.message)
      //   Alert.alert(response.response[0])

      dispatch( {

        type: 'LOADING',
        payload: false

      } );
      console.log( 'errrrrrrrrrrrrrrr>>>>>>>>>>>>>>>' )
      reject( response );
    }
  } );
};

export const CreatingServices = ( data, navigation ) => dispatch => {

  return new Promise( async ( resolve, reject ) => {
    const response = await logistical.post( '/create-service-booking', data );
    console.log( 'resposne===============>', response.status )
    if ( response.status == 1 && response.message == 'Successfull Booking' ) {

      console.log( 'response.fata', response.data )
      dispatch( {
        type: SERVICEBOOKING,
        serviceBooking: response.data,
      } );


      // navigation.navigate('MainHome')
      resolve( response );
      Alert.alert(
        'Booking',
        'Press ok to confirm your booking', [{
          text: 'Cancel',
          onPress: () => console.log( 'Cancel Pressed' ),
          style: 'cancel'
        }, {
          text: 'OK',
          onPress: () => navigation.navigate( 'MainHome' )
        },], {
        cancelable: false
      }
      )
    }
    else {
      dispatch( {

        type: 'LOADING',
        payload: false

      } );
      console.log( 'errrrrrrrrrrrrrrr>>>>>>>>>>>>>>>' )
      console.log( 'datattatatat' )
      reject( response );
    }
  } );
};


export const CancelServices = ( data, navigation ) => dispatch => {
  return new Promise( async ( resolve, reject ) => {

    const response = await logistical.get( '/get-product-cancel?booking_id=' + data );
    if ( response.status == 1 ) {

      dispatch( {
        type: SERVICE_CANCEl,
        serviceCancelBooking: response.data,
      } );
      resolve( response );

      ToastAndroid.showWithGravity(
        'Succesfully cancel!',
        ToastAndroid.LONG,
        ToastAndroid.BOTTOM,
      );

      Alert.alert(
        'Booking',
        'Are you sure you want to cancel', [{
          text: 'Cancel',
          onPress: () => console.log( 'Cancel Pressed' ),
          style: 'cancel'
        }, {
          text: 'OK',
          onPress: () => navigation.navigate( 'MainHome' )
        },], {
        cancelable: false
      }
      )
    }
    else {
      dispatch( {

        type: 'LOADING',
        payload: false

      } );
      console.log( 'errrrrrrrrrrrrrrr>>>>>>>>>>>>>>>' )
      Alert.alert( response.message )
      reject( response );
    }
  } );
};

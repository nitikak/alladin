import {
  GET_SERVICES,
  COMPANY_LIST, ALL_SERVICES, SERVICE_DETAIL, LODING, SERVICE_LIST, PRODUCT_LIST
} from "./type";
import { USER_LOGIN, GET_TOKEN, REMOVE_TOKEN } from "./type";
import { logistical } from '../../logistical'
import AsyncStorage from "@react-native-community/async-storage";
import { Alert } from "react-native";




export const GetServices = (data) => dispatch => {

  dispatch({
    type: 'LOADING',
    payload: true
  });

  console.log("All Services >>>>>>>>>>");
  return new Promise(async (resolve, reject) => {

    const response = await logistical.get('/service-categories', data);

    console.log('response????????', response.data.serviceCategory);

    if (response.status == '1') {
      // AsyncStorage.setItem("login",JSON.stringify(response.data.token));



      dispatch({
        type: GET_SERVICES,
        Services: response.data.serviceCategory,
      });

      dispatch({
        type: 'LOADING',
        payload: false
      });

      //Alert.alert(response.response[0])
      resolve(response);

    }

    else {
      console.log('errrrrrrrrrrrrrrr>>>>>>>>>>>>>>>')
      reject(response);
    }
  });
};


export const CompanyList = (Id, data, navigation) => dispatch => {

  console.log("All Services >>>>>>>>>>");

  return new Promise(async (resolve, reject) => {

    const response = await logistical.get('/service-categories/' + Id, data);

    console.log('comapanies>>>>>>>>>>>>????????', response.data.companies);

    if (response.status == '1') {
      // AsyncStorage.setItem("login",JSON.stringify(response.data.token));

      dispatch({
        type: COMPANY_LIST,
        Companies: response.data.companies,
      });




      navigation.navigate('Auth1');
      //Alert.alert(response.response[0])
      resolve(response);




    }

    else {
      console.log('errrrrrrrrrrrrrrr>>>>>>>>>>>>>>>')
      reject(response);
    }
  });
};



export const AllServices = (catId, compId, data, navigation) => dispatch => {

  console.log("All Services >>>>>>>>>>");
  return new Promise(async (resolve, reject) => {

    const response = await logistical.get('/service-categories/' + catId + '/' + compId, data);

    console.log('consnnssssssssssssssssss', response.data)
    console.log('comapanies>>>>>>>>>>>>????????', catId, compId, response.data.services);

    if (response.status == '1') {
      // AsyncStorage.setItem("login",JSON.stringify(response.data.token));

      dispatch({
        type: ALL_SERVICES,
        AllService: response.data.services,
      });

      navigation.navigate('ServicesScreen');
      //Alert.alert(response.response[0])
      resolve(response);

    }

    else {
      console.log('errrrrrrrrrrrrrrr>>>>>>>>>>>>>>>')
      reject(response);
    }
  });
};



export const ServiceDetail = (catId, slug, data, navigation) => dispatch => {

  // dispatch({
  //   type: 'LOADING',
  //   payload: true

  // });

  console.log("All Services >>>>>>>>>>", catId, slug);

  return new Promise(async (resolve, reject) => {

    const response = await logistical.get('/service-categories/service-details/' + catId + '/' + slug, data);

    if (response.status == '1') {

      dispatch({
        type: SERVICE_DETAIL,
        servicedetail: response.data,
      });
      // dispatch({
      //   type: 'LOADING',
      //   payload: false
      // });

      // navigation.navigate('ServiceDetail');
      //Alert.alert(response.response[0])
      resolve(response);

    }

    else {
      dispatch({
        type: 'LOADING',
        payload: false
      });

      console.log('errrrrrrrrrrrrrrr>>>>>>>>>>>>>>>')
      reject(response);
    }
  });
};

export const ServicesListing = (data) => dispatch => {

  // dispatch({
  //   type: 'LOADING',
  //   payload: true

  // });

  return new Promise(async (resolve, reject) => {

    console.log('abbabababbaba ')

    const response = await logistical.get('/get-service-booking-list', data);
    console.log('response==================>', response)

    if (response.status == '1' && response.message == 'Customer Booking list') {

      dispatch({
        type: SERVICE_LIST,
        servicesListing: response.data.service_bookings,
      });
      // dispatch({
      //   type: 'LOADING',
      //   payload: false
      // });

      // navigation.navigate('ServiceDetail');
      //Alert.alert(response.response[0])
      resolve(response);

    }

    else {
      dispatch({
        type: 'LOADING',
        payload: false
      });

      console.log('errrrrrrrrrrrrrrr>>>>>>>>>>>>>>>')
      reject(response);
    }
  });
};


export const ProductListing = (data) => dispatch => {

  // dispatch({
  //   type: 'LOADING',
  //   payload: true

  // });

  return new Promise(async (resolve, reject) => {


    const response = await logistical.get('/get-product-order-list', data);
    console.log('response==================>', response)

    if (response.status == '1' && response.message == 'Customer Order list') {

      dispatch({
        type: PRODUCT_LIST,
        productListing: response.data.product_orders,
      });
      // dispatch({
      //   type: 'LOADING',
      //   payload: false
      // });

      // navigation.navigate('ServiceDetail');
      //Alert.alert(response.response[0])
      resolve(response);

    }

    else {
      dispatch({
        type: 'LOADING',
        payload: false
      });

      console.log('errrrrrrrrrrrrrrr>>>>>>>>>>>>>>>')
      reject(response);
    }
  });
};

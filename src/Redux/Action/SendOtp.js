import { SEND_OTP, FORGOT_SEND_OTP, VERIFY_RESEND_OTP, RESEND_OTP } from "./type";
import { VERIFY_OTP } from "./type";
import { Globals } from "../../config";
import { logistical } from '../../logistical'
import { Alert } from "react-native";
import { CREATE_PASSWORD } from "./type";
import { useIsFocused, useNavigation } from '@react-navigation/native';

export const sentOtp = ( data, navigation ) => dispatch => {

  console.log( "hsdsdh>>>>>>>>>>", data );
  return new Promise( async ( resolve, reject ) => {

    const response = await logistical.post( '/send-otp', data );
    console.log( 'response????????', response );
    if ( response.status == '1' && response.message == 'Mobile Number exist' ) {

      Alert.alert( 'Mobile Number already exist' );

    }
    else if ( response.status == '1' && response.message == 'Otp Send' ) {

      dispatch( {
        type: SEND_OTP,
        payload: response,
      } );

      resolve( response );
      navigation.navigate( 'OtpScreen', {
        mobilenumber: response.data.mobile.mobile,
        callingCode: response.data.mobile.calling_code,
        OTP: response.data.mobile.otp,
        Id: response.data.mobile.id
      } );

    }

    else if ( response.status == '0' && response.message == 'This phone number is already registered with us' ) {



      resolve( response );
      Alert.alert( 'This phone number is already registered with us' );


    }
    else {
      console.log( 'errrrrrrrrrrrrrrr>>>>>>>>>>>>>>>' )
      reject( response );
    }
  } );
};


export const ForgotsentOtp = ( data, navigation ) => dispatch => {

  console.log( "hsdsdh>>>>>>>>>>", data );
  return new Promise( async ( resolve, reject ) => {

    const response = await logistical.post( '/reset-password', data );
    console.log( 'response????????', response );

    if ( response.status == '1' && response.message == 'Mobile Number exist' ) {

      console.log( 'response=================================>', response.data.mobiles )
      Alert.alert( 'Mobile Number already exist' );

    }

    else if ( response.status == '1' && response.message == 'Otp Send' ) {

      dispatch( {
        type: FORGOT_SEND_OTP,
        payload: response,
      } );

      resolve( response );
      navigation.navigate( 'ForgotPasswordOTP', {
        mobilenumber: response.data.mobiles.mobile,
        callingcode: response.data.mobiles.calling_code,
        Resend: response.data.mobiles.resetotp,
        Id: response.data.mobiles.id
      } );

    }

    else if ( response.status == '0' && response.message == 'This phone number is already registered with us' ) {



      resolve( response );
      Alert.alert( 'This phone number is already registered with us' );


    }
    else {
      console.log( 'errrrrrrrrrrrrrrr>>>>>>>>>>>>>>>' )
      Alert.alert( response.response[0] )
      reject( response );
    }
  } );
};



export const verifyOtp = ( data, navigation ) => dispatch => {

  console.log( "Verify>>>>>>>>>>", data );

  return new Promise( async ( resolve, reject ) => {

    const response = await logistical.post( '/verify-otp', data );
    console.log( 'response verify otp?', response );

    if ( response.status == '1' ) {

      dispatch( {
        type: RESEND_OTP,
        payload: response,
      } )

      dispatch( {
        type: VERIFY_OTP,
        payload: response,
      } );

      Alert.alert( response.response[0] )
      resolve( response );

      navigation.navigate( 'CreatePassword', {
        type: 'signUp',
        Id: response.data.mobileOTP.id,
        token: response.data.mobileOTP.token

      } );

    }

    else {
      Alert.alert( response.message )
      reject( response );
    }
  } );
};

export const VerifyForgetOtp = ( data, navigation ) => dispatch => {

  console.log( "Verify>>>>>>>>>>", data );

  return new Promise( async ( resolve, reject ) => {

    const response = await logistical.post( '/verify-reset-otp', data );
    console.log( 'response verify otp?', response );

    if ( response.status == '1' ) {

      dispatch( {
        type: VERIFY_OTP,
        payload: response,
      } );

      Alert.alert( response.response[0] )
      resolve( response );

      navigation.navigate( 'CreatePassword', {

        Id: response.data.mobileOTP.id,

      } );

    }

    else {
      Alert.alert( response.message )
      reject( response );
    }
  } );
};


export const createpassword = ( data, navigation ) => dispatch => {

  console.log( "createpassword>>>>>>>>>>", data );

  return new Promise( async ( resolve, reject ) => {

    const response = await logistical.post( '/register', data );
    console.log( 'response verify otp?', response );

    if ( response.status == '1' ) {

      dispatch( {
        type: CREATE_PASSWORD,
        payload: response,
      } );

      Alert.alert( response.message )
      resolve( response );
      navigation.navigate( 'Home' );

    }

    else {
      console.log( 'errrrrrrrrrrrrrrr>>>>>>>>>>>>>>>' )
      reject( response );
    }
  } );
};

export const createForgetpassword = ( data, navigation ) => dispatch => {

  console.log( "createpassword>>>>>>>>>>", data );

  return new Promise( async ( resolve, reject ) => {

    const response = await logistical.post( '/update-password', data );
    console.log( 'response verify otp?', response );

    if ( response.status == '1' ) {

      dispatch( {
        type: CREATE_PASSWORD,
        payload: response,
      } );

      Alert.alert( response.message )
      resolve( response );
      navigation.navigate( 'Home' );

    }

    else {
      Alert.alert( response.response[0] )
      console.log( 'errrrrrrrrrrrrrrr>>>>>>>>>>>>>>>' )
      reject( response );
    }
  } );
};
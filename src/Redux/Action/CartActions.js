import {
  ADD_TO_CART, REMOVE_FROM_CART, EMPTY_CART, TOTAL_CART
} from "./type";
import { USER_LOGIN, GET_TOKEN, REMOVE_TOKEN } from "./type";
import { logistical } from '../../logistical'
import AsyncStorage from "@react-native-community/async-storage";
import { Alert } from "react-native";


export const addItemToCart = (data) => dispatch => {
  dispatch({
    type: ADD_TO_CART,
    payload: data
  })
}

export const removeItem = (item) => ({
  type: REMOVE_FROM_CART,
  payload: item,
});

export const emptyCart = (item) => dispatch => {
  dispatch({
    type: EMPTY_CART,
    payload:item
  })
};





import {
    PRODUCT, PRODUCT_ID, DETAIL_PRODUCT,
} from "./type";
import { ORDER } from "./type";
import { logistical } from '../../logistical'
import AsyncStorage from "@react-native-community/async-storage";
import { Alert } from "react-native";




export const AllOrderList = (data, navigation) => dispatch => {

    return new Promise(async (resolve, reject) => {

        const response = await logistical.get('/get-product-order-list', data);

        if (response.status == '1') {

            dispatch({
                type: ORDER,
                getorderData: response.data.product_bookings,
            });

            resolve(response);

        }

        else {
            console.log('errrrrrrrrrrrrrrr>>>>>>>>>>>>>>>')
            reject(response);
        }
    });
};





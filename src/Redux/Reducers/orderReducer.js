

import { ORDER } from "../Action/type";


const initialstate = {


    getorderData: [],
    loading: false,
}

const orderReducer = (state = initialstate, action) => {

    console.log('>>>>>>>>>>>>>>>>Token from reducer.', action.type, action.getorderData);
    switch (action.type) {

        case ORDER:

            return { ...state, getorderData: action.getorderData };

        case 'LOADING':

            if (action.payload) {
                return {
                    ...state,
                    loading: action.payload,
                    error: null,
                    success: null,
                };
            }

            return { ...state, loading: action.payload };


    }

    return state;

}

export default orderReducer;

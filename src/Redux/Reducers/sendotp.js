import { SEND_OTP,FORGOT_SEND_OTP } from "../Action/type";

const initialstate ={

    otp:[],
    forgototp:[]
}

const sendotp = (state = initialstate, action) => {

    switch(action.type){

        case SEND_OTP:
        return { ...state,otp:action.payload }

        case FORGOT_SEND_OTP:
            return {...state,forgototp:action.payload}
      

    }
    
    return state;

}

export default sendotp;

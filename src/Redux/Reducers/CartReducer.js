

import AsyncStorage from "@react-native-community/async-storage";
import { DETAIL_PRODUCT, ADD_TO_CART, REMOVE_FROM_CART, TOTAL_CART, EMPTY_CART } from "../Action/type";


const initialState = {

  loading: false,
  cartItems: [],
  totalPrice: 0,
}

const cartItemsReducer = (state = initialState, action) => {

  // console.log('caritem', cartItem)
  switch (action.type) {
    case ADD_TO_CART:
      return {
        ...state,
        cartItems: addItemTocart(state.cartItems, action.payload),

      };

    case REMOVE_FROM_CART:
      return {
        ...state,
        cartItems: removeItemFromCart(state.cartItems, action.payload),
      };

    case EMPTY_CART:
      console.log('REMOVE_REDUCER', action.payload, state.cartItems);
      return {
        ...state,
        cartItems: state.cartItems.filter(item => item.id !== action.payload)
      }
    // case EMPTY_CART:
    //   return {

    //     ...state,

    //     cartItems: state.cartItems.filter((cartItem) =>{ 
    //       console.log('oddddddddd',cartItem.id)
    //       console.log('payloadddddddd',action.payload.id)

    //       console.log(' cartItem.id !== action.payload.id', cartItem.id !== action.payload.id)
    //       cartItem.id !== action.payload.id}),

    //   };
  }
  return state
}

export default cartItemsReducer;


export const addItemTocart = (cartItems, cartItemToAdd) => {
  const existingCartItem = cartItems.find(
    (cartItem) => cartItem.id === cartItemToAdd.id
  );

  if (existingCartItem) {
    return cartItems.map((cartItem) =>
      cartItem.id === cartItemToAdd.id
        ? { ...cartItem, quantity: cartItem.quantity + 1 }
        : cartItem
    );
  }

  return [...cartItems, { ...cartItemToAdd, quantity: 1 }];
};

export const removeItemFromCart = (cartItems, cartItemToRemove) => {
  const existingCartItem = cartItems.find(
    (cartItem) => cartItem.id === cartItemToRemove.id
  );
  if (existingCartItem.quantity === 1) {
    return cartItems.filter((cartItem) => cartItem.id !== cartItemToRemove.id);
  }
  return cartItems.map((cartItem) =>
    cartItem.id === cartItemToRemove.id
      ? { ...cartItem, quantity: cartItem.quantity - 1 }
      : cartItem
  );
};

export const totalCartPrice2 = (cartItems) => {

  const total = cartItems.reduce(
    //reduce go through the array and cartItem is the each item in the array
    (accumulatedTotal, cartItem) =>
      accumulatedTotal + cartItem.price * cartItem.quantity,
    0 //0 is the start point of accumulatedTotal
  );
  console.log('total', total)
};



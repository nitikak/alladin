

import { BOOKING, BOOKING_DETAIL_LIST, BOOKING_LIST, DASHBOARD_BOOKING, PRODUCTBOOKING, SERVICEBOOKING, PRODUCT_BOOKING_DETAIL_LIST, SERVICE_CANCEl } from "../Action/type";


const initialstate = {
  getBookingData: [],
  getDashboardData: [],
  getBookingListData: [],
  getBookingDetailData: [],
  productBooking: [],
  serviceBooking: [],
  loading: false,
  getProductBookingDetailData: [],
  serviceCancelBooking: [],

}

const bookingReducer = (state = initialstate, action) => {

  console.log('>>>>>>>>>>>>>>>>Token from reducer.', action.type);
  switch (action.type) {

    case BOOKING:

      return { ...state, getBookingData: action.getBookingData };

    case DASHBOARD_BOOKING:
      return { ...state, getDashboardData: action.getDashboardData };
    case BOOKING_LIST:
      return { ...state, getBookingListData: action.getBookingListData };
    case BOOKING_DETAIL_LIST:
      return { ...state, getBookingDetailData: action.getBookingDetailData };

    case PRODUCTBOOKING:
      return { ...state, productBooking: action.productBooking };

    case SERVICEBOOKING:
      return { ...state, serviceBooking: action.serviceBooking };

    case PRODUCT_BOOKING_DETAIL_LIST:
      return { ...state, getProductBookingDetailData: action.getProductBookingDetailData }
    case SERVICE_CANCEl:
      return { ...state, serviceCancelBooking: action.serviceCancelBooking }
    case 'LOADING':

      if (action.payload) {
        return {
          ...state,
          loading: action.payload,
          error: null,
          success: null,
        };
      }

      return { ...state, loading: action.payload };


  }

  return state;

}

export default bookingReducer;

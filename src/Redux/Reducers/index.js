import { combineReducers } from 'redux';

import sendotp from './sendotp';
import UserReducers from './UserReducers';
import Services from './Services'
import productsReducer from './productReducer';
import bookingReducer from './bookingReducer'
import cartItemsReducer from './CartReducer';
import orderReducer from './orderReducer';


export default combineReducers({
    UserReducers,
    Services,
    sendotp,
    productsReducer,
    bookingReducer,
    cartItemsReducer,
    orderReducer,
});



import { PRODUCT_ID, PRODUCT, DETAIL_PRODUCT } from "../Action/type";


const initialstate = {


  getCartData: [],
  productData: [],
  detailProduct: [],
  loading: false,
  cart: [],
  total: 0,
}

const productsReducer = (state = initialstate, action) => {

  console.log('>>>>>>>>>>>>>>>>Token from reducer.', action.type, action.getCartData);
  switch (action.type) {

    case PRODUCT:

      return { ...state, getCartData: action.getCartData };

    case PRODUCT_ID:
      return { ...state, productData: action.productData };

    case DETAIL_PRODUCT:
      return { ...state, detailProduct: action.detailProduct };

    case 'LOADING':

      if (action.payload) {
        return {
          ...state,
          loading: action.payload,
          error: null,
          success: null,
        };
      }

      return { ...state, loading: action.payload };


  }

  return state;

}

export default productsReducer;

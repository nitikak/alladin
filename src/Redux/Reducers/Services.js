
import { GET_SERVICES, COMPANY_LIST, ALL_SERVICES, SERVICE_DETAIL, SERVICE_LIST, PRODUCT_LIST } from "../Action/type";


const initialstate = {

  Services: [],
  companies: [],
  ALLSERVICES: [],
  ServiceDetail: [],
  loading: false,
  servicesListing: [],
  productListing: []
}

const Services = (state = initialstate, action) => {

  console.log('>>>>>>>>>>>>>>>>Token from reducer.', action.type, action.servicedetail);
  switch (action.type) {

    case GET_SERVICES:

      return { ...state, Services: action.Services }

    case COMPANY_LIST:

      return { ...state, companies: action.Companies }

    case ALL_SERVICES:

      return { ...state, ALLSERVICES: action.AllService }


    case SERVICE_DETAIL:

      return { ...state, ServiceDetail: action.servicedetail }

    case SERVICE_LIST:

      return { ...state, servicesListing: action.servicesListing }

    case PRODUCT_LIST:

      return { ...state, productListing: action.productListing }

    case 'LOADING':

      if (action.payload) {

        return {
          ...state,
          loading: action.payload,
          error: null,
          success: null,
        };
      }


      return { ...state, loading: action.payload };


  }

  return state;

}

export default Services;
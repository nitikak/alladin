import React, { useEffect, useState } from 'react';
import {
  Text, TextInput, View, StyleSheet, Image, Pressable, KeyboardAvoidingView, SafeAreaView,
  FlatList, useWindowDimensions, TouchableOpacity, ActivityIndicator, ScrollView,
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import OTP from '../components/OtpInput';
import Login from './Login';
import Register from './Register'
import Colors from '../Layout/Colors'
import Device from './Device';
import { verifyOtp } from '../Redux/Action/SendOtp'
import { useDispatch, useSelector } from 'react-redux';
import { useIsFocused, useNavigation } from '@react-navigation/native';
import RenderHtml from 'react-native-render-html';

// import {ServiceDetail} from '../Redux/Action/ServiceAction';

const ServiceDetailScreen = ({ route }) => {
  const { width } = useWindowDimensions();
  const dispatch = useDispatch();
  const navigation = useNavigation();
  // const [loading, setLoading] = useState(false);
  const { ServiceDetail, loading } = useSelector(state => state.Services);
  // const { loading } = useSelector(state => state.Services);

  const [like, setLike] = useState(false)
  const serviceId = route.params.itemId;
  console.log('loading', loading)
  return (

    <View style={styles.container}>
      <SafeAreaView>
        {ServiceDetail.service && <View style={styles.UpperBlock}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Image style={styles.listIcon} source={require('../assets/left-arrow.png')} />

          </TouchableOpacity>
          <Text numberOfLines={1} style={{ width: wp('70%'), marginLeft: 20, fontSize: 14, paddingTop: 5, color: '#fff', fontFamily: 'Montserrat-SemiBold' }}>{ServiceDetail.service.name}</Text>

        </View>}

        {(loading) &&
          <View style={{ flex: 1, justifyContent: 'center', position: 'absolute', top: '50%', left: '40%' }}>

            <ActivityIndicator
              size="large"
              style={{
                backgroundColor: "rgba(20,116,240,.8)",
                height: 80,
                width: 80,
                zIndex: 999,
                borderRadius: 15
              }}
              color="#ffffff"
            />
          </View>}

        {ServiceDetail.service &&
          <View style={{ paddingBottom: hp('10%') }}>

            <ScrollView>
              <View style={styles.catBox}>
                <Text style={styles.ServiceText}>{ServiceDetail.service.name}</Text>
                <Image resizeMode='contain' style={styles.caticon} source={{ uri: ServiceDetail.service.service_image_url }} />

                <View style={{ marginHorizontal: wp(5), flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                  <View style={{ flexDirection: 'row' }}>
                    <TouchableOpacity onPress={() => setLike(!like)}>
                      <Image source={like === true ? require('../assets/redheart.png') : require('../assets/heart.png')} resizeMode='contain' style={{ height: hp(6), width: wp(10) }} />
                    </TouchableOpacity>
                    <TouchableOpacity>
                      <Image source={require('../assets/messages.png')} resizeMode='contain' style={{ height: hp(6), width: wp(10), marginLeft: wp(2) }} />
                    </TouchableOpacity>
                  </View>
                  <TouchableOpacity
                    style={{ backgroundColor: '#9066e6', height: hp(4), width: wp(25), alignItems: 'center', justifyContent: 'center', borderRadius: 5 }}
                    onPress={() => navigation.navigate('PropertyDetail', {
                      serviceId: route.params.itemId,
                      typeservice: 'service'

                    })}>
                    <Text style={{ fontSize: 13, color: '#fff', fontFamily: 'Montserrat-SemiBold' }}>Book Now</Text>
                  </TouchableOpacity>

                </View>
                <View style={[styles.lineWrapper, { marginTop: 0 }]} />

                <View style={{ marginHorizontal: wp(5), marginVertical: hp(1.5) }}>
                  <Text style={styles.PriceText}>{ServiceDetail.service.formated_price}</Text>
                </View>


                <View style={{ flexDirection: 'row', marginHorizontal: wp(5), }}>
                  <View style={{ width: wp(20) }}>
                    <Text style={styles.NameText}>Name</Text>
                  </View>
                  <Text style={styles.NameText}>{ServiceDetail.service.company.company_name}</Text>

                </View>
                <View style={{ flexDirection: 'row', marginHorizontal: wp(5), }}>
                  <View style={{ width: wp(20) }}>
                    <Text style={styles.NameText}>Location</Text>
                  </View>
                  <Text style={styles.NameText}>{ServiceDetail.service.location.name}</Text>
                </View>

                <View style={styles.lineWrapper} />

                <View style={{ marginHorizontal: wp(5), paddingTop: hp(2) }}>
                  <RenderHtml
                    contentWidth={width}

                    source={{ html: `${ServiceDetail.service.description}` }}
                  />
                </View>
              </View>
            </ScrollView>
          </View>}
      </SafeAreaView>
    </View>
  )
}

export default ServiceDetailScreen;


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#dfdfdf'
  },

  caticon: {
    height: hp('40%'),
    width: wp('100%'),
    backgroundColor: 'transparent'
  },
  ServiceText: {
    color: '#000',
    fontSize: 14,
    paddingHorizontal: wp(5),
    paddingTop: hp(3),
    fontFamily: 'Montserrat-SemiBold'
  },
  searchSection: {
    flexDirection: 'row',
    width: wp('95%'),
    backgroundColor: '#fff',
    borderRadius: 5,
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 10,
    alignSelf: 'center',
    marginTop: 10,
    marginBottom: 10
  },
  searchIcon: {
    marginTop: 10,
    height: 25,
    width: 25,
  },
  input: {
    width: '80%',
    fontFamily: 'Montserrat-Regular',
    fontSize: 12,
    color: 'black',
  },
  PriceText: {
    color: '#000',
    fontSize: 14,
    fontFamily: 'Montserrat-SemiBold'
  },

  NameText: {
    color: '#000',
    fontSize: 12,
    fontFamily: 'Montserrat-Regular'
  },
  description: {
    color: '#c2c2c2',
    fontSize: 12,
    paddingLeft: 10,
    paddingRight: 10,
    fontFamily: 'Montserrat-Regular'
  },
  UpperBlock: {
    height: hp('7%'),
    backgroundColor: '#9066e6',
    alignItems: 'center',
    flexDirection: 'row'
  },
  listIcon: {
    height: 20,
    width: 20,
    marginLeft: 10,
  },
  lineWrapper: {
    backgroundColor: '#c2c2c2',
    height: hp(.1),
    marginTop: hp(2)
  }
})


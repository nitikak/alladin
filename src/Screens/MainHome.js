import React, { useEffect, useState } from 'react';
import {
  Text, TextInput, View, StyleSheet, Image, Pressable, SafeAreaView, KeyboardAvoidingView, ImageBackground,
  ScrollView, useWindowDimensions, TouchableOpacity, Alert, BackHandler, ActivityIndicator, Platform
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Header from '../components/Header/index';
import Category from '../components/Categories'
import Services from '../components/Services'
import { RemoveToken } from '../Redux/Action/userAction';
import { GetServices } from '../Redux/Action/ServiceAction';
import { useDispatch, useSelector } from 'react-redux';
import { useIsFocused, useNavigation } from '@react-navigation/native';
import AsyncStorage from "@react-native-community/async-storage";
import { AllProduct } from '../Redux/Action/productAction';

function MainHome() {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const { loading } = useSelector(state => state.Services);
  const productdata = useSelector((state) => state.productsReducer.getCartData);
  const [filterData, setFilterData] = useState([]);
  const addCart = useSelector((state) => state.cartItemsReducer.cartItems);
  console.log('addcart', addCart.length)
  const Logout = () => {

    dispatch(RemoveToken(null, navigation))
    AsyncStorage.removeItem('login');
    navigation.navigate('Auth');
  }

  function handleBackButtonClick() {
    // navigation.isFocused() helps to exit the app on this component rather than in whole app.
    if (navigation.isFocused()) {
      BackHandler.exitApp();
      return true;
    }
  }


  useEffect(() => {

    dispatch(AllProduct())
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);

    return () => {

      BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
      // Anything in here is fired on component unmount.
    };


  }, [])
  const SearchFilterFunction = (text) => {
    const searchWord = text;
    const newFilter = productdata.filter((value) => {
      const itemData = value.name ? value.name.toUpperCase() : ''.toUpperCase();
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });

    console.log('search word================>', searchWord, newFilter)
    if (searchWord === '') {
      setFilterData([]);
    } else {
      setFilterData(newFilter)
    }
  }


  const handleBackButton = () => {
    Alert.alert(
      'Exit App',
      'Exiting the application?', [{
        text: 'Cancel',
        onPress: () => { navigation.goBack(), console.log('Cancel Pressed') },
        style: 'cancel'
      }, {
        text: 'OK',
        onPress: () => BackHandler.exitApp()
      },], {
        cancelable: false
      }
    )
    return true;
  }
  return (

    <View style={styles.container}>
      <SafeAreaView>
      <ScrollView style={{ paddingBottom: hp(2)}}>

        {/* <Header navigation={navigation}/> */}
        <View style={styles.container}>

          <View style={styles.headerContainer1}>

            <View style={styles.headerContainer}>
              {/* <View style={styles.drawerContainer}>
       <Image  style={styles.burgerlogo} source={require('../../assets/hemburger.png')}/>
    </View> */}
              <View style={styles.LogoContainer}>
                <Image style={styles.logo} source={require('../assets/Logo.png')} />
              </View>
              <View style={styles.CartContainer}>
                <TouchableOpacity onPress={() => navigation.navigate('AddCart')}>
                  <View style={styles.cartWrapper}>
                    <Text style={styles.cartText}>{addCart.length}</Text>
                  </View>
                  <Image style={styles.cartlogo} source={require('../assets/cart.png')} />
                </TouchableOpacity>



                <Image style={styles.belllogo} source={require('../assets/23.png')} />
              </View>
            </View>


            <View style={styles.searchSection}>
              <Image style={styles.searchIcon} source={require('../assets/search.png')} />

              <TextInput
                style={styles.input}
                underlineColorAndroid="rgba(0,0,0,0)"
                placeholder={'Find Your Product'}
                keyboardType="default"
                returnKeyType="done"
                autoCapitalize="none"
                value={filterData}
                onChangeText={(text) => SearchFilterFunction(text)}
              />

            </View>
          </View>
        </View>
          <View style={styles.catContainer}>
            <Text style={styles.shopOnlineText}>Shop Online</Text>
            <TouchableOpacity onPress={() => navigation.navigate('Category')}>
              <Text style={styles.seeMoreText}>See More</Text>
            </TouchableOpacity>
          </View>

          {/* <Category/> */}
          {
            filterData.length > 0 ?
              <ScrollView
                horizontal={true}
              >
                {
                  filterData.map((item) => {
                    return (
                      <TouchableOpacity style={{ borderRadius: 4, marginLeft: wp(1.5), marginTop: hp(2), marginRight: wp(1) }} onPress={() => navigation.navigate('ParticularCategories', {
                        id: item.id,

                      })}>
                        <ImageBackground source={{ uri: item.category_image_url }} style={{ height: hp(12), width: wp(25), borderRadius: 4, }} >
                          <View style={{ height: '100%', width: '100%', flex: 1, backgroundColor: '#000', opacity: 0.8, borderRadius: 4, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ color: '#fff', paddingTop: 2, fontSize: 10, fontFamily: 'Montserrat-Bold', textAlign: 'center' }}>{item.name}</Text>
                          </View>
                        </ImageBackground>
                      </TouchableOpacity>

                    )
                  })
                }



              </ScrollView> : <ScrollView
                horizontal={true}
              >
                {
                  productdata.map((item) => {
                    return (
                      <TouchableOpacity style={styles.fliterDataWrapper} onPress={() => navigation.navigate('ParticularCategories', {
                        id: item.id,

                      })}>
                        <ImageBackground source={{ uri: item.category_image_url }} style={styles.blackShadeWrapper} >
                          <View style={styles.nameWrapper}>
                            <Text style={styles.nameText}>{item.name}</Text>
                          </View>
                        </ImageBackground>
                      </TouchableOpacity>

                    )
                  })
                }



              </ScrollView>
          }


          <View style={{ width: wp('85%'), alignSelf: 'center' }}>

            <View style={styles.serviceContainer}>
              <Text
                style={{

                  fontSize: 16,
                  fontFamily: 'Montserrat-Regular'
                }}>Select Maintenance Service</Text>

              <Text
                style={{
                  fontSize: 10,
                  fontFamily: 'Montserrat-Regular',
                  paddingTop: 8,
                  paddingBottom: 10
                }}>Lorem Ipsum is simply dummy text
              {'\n'} of the printing
  
            </Text>

              {/* <TouchableOpacity onPress={() => Logout()}>
                      <Text>lOGOUT</Text>
                  </TouchableOpacity> */}

            </View>

            {(loading) &&
              <View style={{ flex: 1, justifyContent: 'center', position: 'absolute', top: '50%', left: '40%' }}>

                <ActivityIndicator


                  size="large"
                  style={{
                    //  backgroundColor: "rgba(144,102,230,.8)",
                    height: 80,
                    width: 80,
                    zIndex: 999,
                    borderRadius: 15
                  }}
                  color="rgba(144,102,230,.8)"
                />
              </View>}

            <Services />
          </View>



        </ScrollView>
      </SafeAreaView>
    </View>
  )

}

export default MainHome;

const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: '#dfdfdf'
  },

  shopOnlineText: {
    fontSize: 16,
    color: '#000',
    fontFamily: 'Montserrat-Regular'
  },
  seeMoreText: {
    fontSize: 10,
    fontFamily: 'Montserrat-Regular'
  },
  fliterDataWrapper: { borderRadius: 4, marginLeft: wp(1.5), marginTop: hp(2), marginRight: wp(1) },
  blackShadeWrapper: { height: hp(12), width: wp(25), borderRadius: 4, },
  nameWrapper: { height: '100%', width: '100%', flex: 1, backgroundColor: '#000', opacity: 0.8, borderRadius: 4, justifyContent: 'center', alignItems: 'center' },
  nameText: { color: '#fff', paddingTop: 2, fontSize: 10, fontFamily: 'Montserrat-Bold', textAlign: 'center' },
  catContainer: {

    // flexDirection:'row',
    // width:wp('90%'),
    // alignSelf:'center',
    // paddingTop:20,
    flexDirection: 'row',
    justifyContent: 'space-between', paddingTop: 20,
    marginHorizontal: wp(5),
    alignItems: 'center'

  },

  serviceContainer: {

    width: wp('80%'),
    alignSelf: 'center',
    paddingTop: 15,
    paddingBottom: 10,


  },
  catBox: {
    height: 80,
    width: 80,
    backgroundColor: 'black',
    justifyContent: 'center',
    borderRadius: 5,
    // marginRight: 5
  },
  caticon: {

    height: 50,
    borderRadius: 25,
    width: wp('90%'),
    padding: 90,
    alignSelf: 'center',
    backgroundColor: '#fff'
  },

  container: {
    // flex:1
  },
  headerContainer1: {
    backgroundColor: '#9066e6',
    height: hp('20%'),

  },

  headerContainer: {
    width: wp('95%'),
    height: hp('9%'),
    flexDirection: 'row',
    alignSelf: 'center',
    paddingTop: 10,
    // backgroundColor:'yellow'

  },
  searchSection: {
    flexDirection: 'row',
    width: wp('90%'),
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: '#7c8791',
    borderRadius: 5,
    // paddingTop: 3,
    paddingLeft: 10,
    alignSelf: 'center',
    alignItems: 'center',
    // justifyContent:'center',
    paddingVertical: Platform.OS === 'ios' ? hp(1) : hp(0)
  },
  drawerContainer: {
    width: wp('15%'),
    justifyContent: 'center',
    height: 50,
    // backgroundColor:'red'

  },
  burgerlogo: {
    height: 30,
    width: 30
  },
  logo: {

    height: hp('7%'),
    width: wp('40%'),
    resizeMode: 'contain',




  },

  belllogo: {
    height: 30,
    width: 30,
    alignSelf: 'center',
    marginTop: 10

  },
  LogoContainer: {
    width: wp('75%'),
    // alignItems:'flex-start',
    // backgroundColor:'red',
    justifyContent: 'center'

  },
  CartContainer: {
    width: wp('20%'),
    height: 50,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',

  },
  cartWrapper: {
    position: 'absolute',
    left: 23,
    top: 12,
    backgroundColor: '#fff',
    height: hp(2),
    width: wp(4),
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',

  },
  cartText: {
    color: '#000',
    fontFamily: 'Montserrat-Regular',

    fontSize: 9
  },
  cartlogo: {
    height: 40,
    width: 40,
    alignSelf: 'center'
  },
  searchIcon: {

    // marginTop: 10,
    height: 28,
    width: 28,
  },


  input: {
    width: '80%',
    fontFamily: 'Montserrat-Regular',
    fontSize: 12,
    color: 'black',


  },


})
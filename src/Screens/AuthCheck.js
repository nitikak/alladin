import React, {useEffect, useState} from 'react';
import { Text, TextInput, View,StyleSheet,Image, Pressable,KeyboardAvoidingView,SafeAreaView,ActivityIndicator,
    useWindowDimensions,TouchableOpacity,BackHandler, Alert } from 'react-native';

import {UserLogin} from '../Redux/Action/userAction';
import { useDispatch,useSelector } from 'react-redux';  
import AsyncStorage from "@react-native-community/async-storage";
import {useIsFocused, useNavigation} from '@react-navigation/native';


    const AuthCheck = () => {
        const navigation = useNavigation();
        const dispatch = useDispatch();
        const {Token} = useSelector(state => state.UserReducers);  
        const [loading,setLoading] = useState(false);

        useEffect(() => {

            setLoading(true);

        // setTimeout(() => {
        //    // BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        //     setLoading(false);
        // }, 2000);
          
        readData();
           // 
        //    BackHandler.addEventListener('hardwareBackPress', handleBackButton);
        //     return () => {
        //         BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
               
                
        //         // Anything in here is fired on component unmount.
        //       };
        
        })

        const handleBackButton = () => {
            

            Alert.alert(
                'Exit App',
                'Exiting the application?', [{
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel'
                }, {
                    text: 'OK',
                    onPress: () => BackHandler.exitApp()
                }, ], {
                    cancelable: false
                }
             )

             return true;
             
           } 
      

     

        const  readData = async () => {
 
         //   console.log('llllllllllllll',Token);
             const login = await AsyncStorage.getItem('login');

             console.log('login>>>>>>>',login);
      
           
             if (login !== null) {
              
       
              navigation.navigate('Homes');
               
             } else {
               
             
                navigation.navigate('Home');
             }
          
         };

        return(

            <View style={styles.container}>
                {(loading) &&
                    <View style={{ flex: 1, justifyContent: 'center', position: 'absolute', top: '50%', left: '40%' }}>

                        <ActivityIndicator

                            
                            size="large"
                            style={{
                              //  backgroundColor: "rgba(144,102,230,.8)",
                                height: 80,
                                width: 80,
                                zIndex: 999,
                                borderRadius: 15
                            }}
                            size="small"
                            color="rgba(144,102,230,.8)"
                        />
                    </View>}


            </View>
        )


    }

    export default AuthCheck;

    const styles = StyleSheet.create({
        container: {
          flex: 1,
         // backgroundColor: Colors.background,
        },
        borderStyleHighLighted: {
          borderColor: "#03DAC6",
        },
    })
import React, { useState } from 'react';
import {
    Text, TextInput, View, StyleSheet, Image, Pressable, KeyboardAvoidingView, SafeAreaView,
    useWindowDimensions, TouchableOpacity
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { createpassword } from '../Redux/Action/SendOtp'
import { useDispatch, useSelector } from 'react-redux';
import { useIsFocused, useNavigation } from '@react-navigation/native';



const ShopScreen = ({ route }) => {

    const dispatch = useDispatch();
    const navigation = useNavigation();
    const [password, setPassword] = useState('');

    return (

        <View style={styles.container}>

            <SafeAreaView>

                <View style={styles.OTPContainer}>
                    {/* <Text> {route.params.OTP}</Text> */}

                    <Text style={styles.HeaderText}>Shop</Text>

                </View>
                <View style={styles.profileContainer}>

                </View>
            </SafeAreaView>
        </View>

    )
}

export default ShopScreen;


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#dfdfdf'

    },
    HeaderText: {
        fontFamily: 'Montserrat-Bold',
        fontSize: 20

    },

    searchSection: {
        flexDirection: 'row',
        width: wp('90%'),
        margin: 1,
        borderRadius: 10,
        //paddingTop:2,
        paddingLeft: 10,
        alignSelf: 'center',
        backgroundColor: '#fff'
    },

    searchIcon: {

        marginTop: 15,
        height: 20,
        padding: 5,
        width: 20,

        justifyContent: 'center',
        borderRightWidth: 1,
        borderColor: '#7c8791',
        //backgroundColor:'red'
    },
    inputpassword: {
        width: '90%',
        marginBottom: 2,
        fontFamily: 'Montserrat-Regular',
        fontSize: 14,
        color: 'black',
        textAlign: 'left'
    },

    input: {

        width: '80%',
        // backgroundColor:'red',
        fontFamily: 'Montserrat-Regular',
        fontSize: 12,
        color: 'black',
        textAlign: 'center',

    },
    button: {
        marginTop: 30,
        marginHorizontal: 40,
        backgroundColor: '#9066e6',
        padding: 10,
        width: wp('80%'),
        borderRadius: 10,
        alignSelf: 'center'

    },

    buttonText: {
        color: 'white',
        textAlign: 'center',
        fontFamily: 'Montserrat-Regular',
        //  textTransform:'uppercase'
    },
    UpperBlock: {
        height: hp('7%'),
        backgroundColor: '#9066e6',
        justifyContent: 'center'
    },
    OTPContainer: {
        width: wp('90%'),
        marginTop: 20,
        alignSelf: 'center'
    },


})


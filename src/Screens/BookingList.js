import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, ActivityIndicator, BackHandler, FlatList, Button, ScrollView, } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { SafeAreaView } from 'react-native-safe-area-context';
import DatePicker from 'react-native-date-picker'
import DateTimePicker from "@react-native-community/datetimepicker";
import moment from "moment";
import { useIsFocused, useNavigation } from '@react-navigation/native';
import RNPickerSelect from 'react-native-picker-select';
import { useDispatch, useSelector } from 'react-redux';
import { GetBookingList } from '../Redux/Action/bookingAction';
import { ServicesListing } from '../Redux/Action/ServiceAction';


export default function BookingList ( props ) {
  const navigation = useNavigation();


  const [state, setState] = useState( 'pending' )
  const state_list = [
    { label: 'pending', value: 'pending' },
    { label: 'completed', value: 'completed' },
    { label: 'canceled', value: 'canceled' },

  ];

  const dispatch = useDispatch();
  const { loading } = useSelector( ( state ) => state.Services );

  const listingData = useSelector( ( state ) => state.Services.servicesListing );

  console.log( 'loading========>', loading )

  useEffect( () => {
    dispatch( ServicesListing() )
  }, [] )



  return (
    <View style={styles.container}>
      <SafeAreaView>
        <View style={{ backgroundColor: '#9066e6', paddingVertical: hp( 2 ), flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <Image resizeMode='contain' source={require( '../assets/left-arrow.png' )} style={{ height: hp( 3 ), width: wp( 8 ), marginRight: wp( 6 ), marginLeft: wp( 2 ) }} />
            </TouchableOpacity>
            <Text style={{ fontSize: 14, color: '#fff', fontFamily: "Montserrat-Bold" }}>BookingList</Text>
          </View>
        </View>




        <TouchableOpacity style={{ backgroundColor: '#fff', marginTop: hp( 4 ), elevation: 3, paddingVertical: hp( 2 ), marginHorizontal: wp( 5 ) }}>
          <RNPickerSelect
            onValueChange={( value ) => setState( value )}
            items={state_list}
            value={state}
            placeholder={{}}
          >
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: wp( 4 ) }}>
              {state_list.map(
                ( item ) =>
                  item.value === state && (
                    <Text
                      key={item.value}
                      style={{ fontSize: 14, color: '#000', fontFamily: 'Montserrat-Medium' }}>
                      {item.label}
                    </Text>
                  )
              )}
              <Image source={require( '../assets/Downarrow.png' )} resizeMode='contain' style={{ height: hp( 3 ), width: wp( 8 ) }} />

            </View>
          </RNPickerSelect>

        </TouchableOpacity>
        {( loading ) &&
          <View style={{ flex: 1, justifyContent: 'center', position: 'absolute', top: '40%', left: '40%' }}>

            <ActivityIndicator


              size="large"
              style={{
                //  backgroundColor: "rgba(144,102,230,.8)",
                height: 80,
                width: 80,
                zIndex: 999,

                borderRadius: 15

              }}

              color="rgba(144,102,230,.8)"
            />
          </View>}

        {
          state === 'completed' ?
            <FlatList
              key={'_'}
              keyExtractor={item => "_" + item.id}
              data={listingData.data}
              horizontal={false}
              renderItem={( { item, index } ) => (
                <>
                  {item.status === state ? <TouchableOpacity onPress={() => navigation.navigate( 'BookingProductList', {
                    bookingId: item.id
                  } )} style={styles.bookingWrapper}>
                    {( loading ) &&
                      <View style={{ flex: 1, justifyContent: 'center', position: 'absolute', top: '30%', left: '40%' }}>

                        <ActivityIndicator


                          style={{
                            //  backgroundColor: "rgba(144,102,230,.8)",
                            height: 80,
                            width: 80,
                            zIndex: 999,
                            borderRadius: 15
                          }}
                          size="small"
                          color="rgba(144,102,230,.8)"
                        />
                      </View>}
                    <View style={styles.bookingTopWrapper}>
                      <Text style={styles.bookingLeftText}>Booking #{item.id}</Text>
                      {/* <Text style={styles.bookingLeftText}>{item.servie_provider.company_name}</Text> */}
                    </View>
                    <View style={styles.leftImageWrapper}>
                      <View style={styles.imagebackgroundwrappper}>
                        <Image resizeMode='contain' source={require( '../assets/userProfile.png' )} style={styles.imageBox} />
                      </View>
                      <View style={styles.rightBoxWrapper}>
                        {/* <Text style={styles.bookingLeftText}>Karim</Text> */}
                        {/* <Text style={styles.bookingLeftText}>{item.servie_provider.company_phone}</Text>
                      <Text style={styles.bookingLeftText}>{item.servie_provider.company_email}</Text> */}
                      </View>
                      <View >
                        <Text style={styles.bookingLeftText}>{item.date}</Text>
                        <Text style={styles.bookingLeftText}>{item.time}</Text>
                      </View>
                    </View>

                    <View style={styles.approvedWrapper}>
                      <Text style={styles.approvedText}>{item.status === state ? item.status : null}</Text>
                    </View>
                  </TouchableOpacity> : null}
                </>
              )}

            />
            : state === 'pending' ?

              <FlatList
                key={'_'}
                keyExtractor={item => "_" + item.id}
                data={listingData.data}
                horizontal={false}
                renderItem={( { item, index } ) => (
                  <>
                    {item.status === state ? <TouchableOpacity onPress={() => navigation.navigate( 'BookingProductList', {
                      bookingId: item.id,
                    } )} style={styles.bookingWrapper}>

                      <View style={styles.bookingTopWrapper}>
                        <Text style={styles.bookingLeftText}>Booking #{item.id}</Text>
                        {/* <Text style={styles.bookingLeftText}>{item.servie_provider.company_name}</Text> */}
                      </View>
                      <View style={styles.leftImageWrapper}>
                        <View style={styles.imagebackgroundwrappper}>
                          <Image resizeMode='contain' source={require( '../assets/userProfile.png' )} style={styles.imageBox} />
                        </View>
                        <View style={styles.rightBoxWrapper}>
                          {/* <Text style={styles.bookingLeftText}>Karim</Text> */}
                          {/* <Text style={styles.bookingLeftText}>{item.servie_provider.company_phone}</Text>
                        <Text style={styles.bookingLeftText}>{item.servie_provider.company_email}</Text> */}
                        </View>
                        <View >
                          <Text style={styles.bookingLeftText}>{item.date}</Text>
                          <Text style={styles.bookingLeftText}>{item.time}</Text>
                        </View>
                      </View>

                      <View style={[styles.approvedWrapper, { backgroundColor: '#f2ac00' }]}>
                        <Text style={styles.approvedText}>{item.status === state ? item.status : null}</Text>
                      </View>
                    </TouchableOpacity> : null}
                  </>
                )}

              /> : state === 'canceled' ?

                <FlatList
                  key={'_'}
                  keyExtractor={item => "_" + item.id}
                  data={listingData.data}
                  horizontal={false}
                  renderItem={( { item, index } ) => (
                    <>
                      {item.status === state ? <TouchableOpacity onPress={() => navigation.navigate( 'BookingProductList', {
                        bookingId: item.id
                      } )} style={styles.bookingWrapper}>
                        <View style={styles.bookingTopWrapper}>
                          <Text style={styles.bookingLeftText}>Booking #{item.id}</Text>
                          {/* <Text style={styles.bookingLeftText}>{item.servie_provider.company_name}</Text> */}
                        </View>
                        <View style={styles.leftImageWrapper}>
                          <View style={styles.imagebackgroundwrappper}>
                            <Image resizeMode='contain' source={require( '../assets/userProfile.png' )} style={styles.imageBox} />
                          </View>
                          <View style={styles.rightBoxWrapper}>
                            {/* <Text style={styles.bookingLeftText}>Karim</Text> */}
                            {/* <Text style={styles.bookingLeftText}>{item.servie_provider.company_phone}</Text>
                          <Text style={styles.bookingLeftText}>{item.servie_provider.company_email}</Text> */}
                          </View>
                          <View >
                            <Text style={styles.bookingLeftText}>{item.date}</Text>
                            <Text style={styles.bookingLeftText}>{item.time}</Text>
                          </View>
                        </View>

                        <View style={[styles.approvedWrapper, { backgroundColor: '#da3348' }]}>
                          <Text style={styles.approvedText}>{item.status === state ? item.status : null}</Text>
                        </View>
                      </TouchableOpacity> : null}
                    </>
                  )}

                /> : null
        }



      </SafeAreaView>
    </View>
  );
}

const styles = StyleSheet.create( {
  container: { flex: 1, backgroundColor: '#f7f5f1', },
  bookingWrapper: { backgroundColor: '#fff', paddingTop: hp( 2 ), elevation: 3, marginHorizontal: wp( 5 ), borderRadius: 4, marginTop: hp( 2 ) },
  bookingTopWrapper: { flexDirection: 'row', paddingHorizontal: wp( 3 ), justifyContent: 'space-between', alignItems: 'center' },
  bookingLeftText: { fontSize: 14, fontFamily: 'Montserrat-Medium', color: '#000' },
  leftImageWrapper: { flexDirection: 'row', alignItems: 'center', },
  imagebackgroundwrappper: { height: hp( 9 ), width: wp( 18 ), borderRadius: 50, backgroundColor: '#f7f5f1', marginLeft: wp( 3 ), marginTop: hp( 2 ) },
  imageBox: { height: hp( 9 ), width: wp( 18 ) },
  rightBoxWrapper: { paddingLeft: wp( 2 ), width: wp( 45 ), paddingTop: hp( 1 ) },
  approvedWrapper: { paddingVertical: hp( 2 ), backgroundColor: '#2ea749', marginTop: hp( 1 ), justifyContent: 'center', alignItems: 'center', borderBottomLeftRadius: 4, borderBottomRightRadius: 4 },
  approvedText: { color: '#fff', fontFamily: 'Montserrat-Medium', fontSize: 14 }

} );

import React, { useState } from 'react';
import {
  Text, TextInput, View, StyleSheet, Image, Pressable, KeyboardAvoidingView, SafeAreaView,
  useWindowDimensions,
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { TabView } from 'react-native-tab-view';
import Login from './Login';
import Register from './Register'
import Colors from '../Layout/Colors'
import OtpScreen from './OtpScreen';
import Device from './Device';

function Home() {

  const layout = useWindowDimensions();

  const [index, setIndex] = React.useState(0);
  const [loading, setLoading] = useState(false);
  const [routes, setRoutes] = React.useState([
    { key: 'first', title: 'Log in', selected: true },
    { key: 'second', title: 'Sign Up', selected: false },
  ]);

  const _renderScene = ({ route, navigation }) => {
    switch (route.key) {
      default:
      case 'first':
        return <Login setLoading={setLoading} />;
      case 'second':
        return <Register setLoading={setLoading} navigation={navigation} />;
    }
  };

  const select = i => {
    setIndex(i);
    check(i);
  };

  const check = i => {
    const newRoutes = routes.map((item, j) => {
      if (j === i) {
        return { ...item, selected: true };
      } else {
        return { ...item, selected: false };
      }
    });
    setRoutes(newRoutes);
  };

  const _handleIndexChange = i => select(i);

  const _renderTabBar = props => {
    return (
      <View style={styles.tabBar}>
        {props.navigationState.routes.map((route, i) => {
          return (
            <Pressable
              key={i.toString()}
              style={[
                styles.tabItem,
                {
                  backgroundColor: route.selected
                    ? '#C5AAF9'
                    : '#9066e6',
                },
              ]}
              onPress={() => select(i)}>
              <Text
                style={[
                  styles.tabTitle,
                  { color: route.selected ? Colors.white : Colors.white, fontFamily: 'Montserrat-Regular' },

                ]}>
                {route.title}
              </Text>
            </Pressable>
          );
        })}
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <View style={styles.UpperBlock}>
        <Image style={styles.logo} source={require('../assets/NEWLOGOALADDIN1.png')} />
        <Text style={styles.HomeText}>Home Services</Text>

      </View>

      <View style={styles.tabViewContainer}>
        <TabView
          swipeEnabled={false}
          scrollEnable={true}
          navigationState={{ index, routes }}
          renderTabBar={_renderTabBar}
          renderScene={_renderScene}
          onIndexChange={_handleIndexChange}
          initialLayout={{ width: layout.width }}
        />

      </View>


    </View>
  )
}

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#dfdfdf'

  },
  UpperBlock: {
    height: hp('30%'),
    backgroundColor: '#9066e6',
    justifyContent: 'center'
  },
  logo: {
    // resizeMode:'contain',
    alignSelf: 'center',
    height: 120,
    width: 150,
    // backgroundColor:'red'

  },
  HomeText: {
    color: '#fff',
    alignSelf: 'center'
  },
  content: {
    //  padding: 10,
    marginTop: 20,
  },
  tabViewContainer: {
    //  marginVertical: 50,
    height: Device.height * 0.70,
  },
  maxHeight: { height: '100%' },
  tabBar: {
    height: 50,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  tabItem: {
    width: '50%',
    height: 50,
    backgroundColor: Colors.white,
    justifyContent: 'center',
    alignItems: 'center',
  },
  tabTitle: {
    fontSize: 15,
    color: Colors.black,
  },


})
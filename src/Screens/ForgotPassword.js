import React, { useEffect, useState } from 'react';
import {
  Text, TextInput, View, StyleSheet, Image, Pressable, KeyboardAvoidingView, SafeAreaView,
  useWindowDimensions, TouchableOpacity, ActivityIndicator, Alert
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import OTP from '../components/OtpInput';
import Login from './Login';
import Register from './Register'
import Colors from '../Layout/Colors'
import Device from './Device';
import { ForgotsentOtp } from '../Redux/Action/SendOtp'
import { useDispatch, useSelector } from 'react-redux';
import { useIsFocused, useNavigation } from '@react-navigation/native';

import RNPickerSelect from 'react-native-picker-select';


const ForgotPassword = ( { } ) => {

  const navigation = useNavigation();
  const dispatch = useDispatch();
  const [mobile, setMobile] = useState( '' );
  const [loading, setLoading] = useState( false );

  const GotoOtp = () => {

    console.log( '??????????????????/', mobile );
    if ( mobile == '' ) {
      Alert.alert( 'Mobile number is required' );
    }
    else if ( mobile.length < 10 || mobile.length > 10 ) {
      Alert.alert( 'Mobile number should be 10 digit' );
    }
    else {

      setLoading( true );
      setTimeout( () => {

        dispatch( ForgotsentOtp( { mobile: mobile, calling_code: state }, navigation ) )
        setLoading( false );
      }, 2000 );
    }

  }

  const [state, setState] = useState( '+964' )
  const state_list = [
    { label: '+964', value: '+964' },
    { label: '+91', value: '+91' },
    { label: '+971', value: '+971' },

  ];


  return (

    <View style={styles.container}>
      {( loading ) &&
        <View style={{ flex: 1, justifyContent: 'center', position: 'absolute', top: '20%', left: '40%' }}>

          <ActivityIndicator


            style={{
              backgroundColor: "rgba(144,102,230,.8)",
              height: 80,
              width: 80,
              zIndex: 999,
              borderRadius: 15
            }}
            size="small"
            color="#ffffff"
          />
        </View>}

      <View style={styles.UpperBlock}>

        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Image style={styles.listIcon} source={require( '../assets/left-arrow.png' )} />

        </TouchableOpacity>

      </View>

      <View style={styles.UpperBlock1}>

        <Image style={styles.logo} source={require( '../assets/NEWLOGOALADDIN1.png' )} />
        <Text style={{ fontSize: 16, paddingTop: 5, color: '#fff', textAlign: 'center', fontFamily: 'Montserrat-SemiBold' }}>Forgot Password</Text>

      </View>


      <View style={styles.content}>
        <View style={styles.form}>

          <Text
            style={{
              textAlign: 'center',
              color: '#000',
              marginBottom: 20,
              fontSize: 14,
              fontFamily: 'Montserrat-Regular',
              width: wp( '90%' ),
              textAlign: 'center',
              lineHeight: 22

            }}>
            Enter your mobile number to get{'\n'} OTP for verification.
          </Text>
          <View style={styles.searchSection}>
            <TouchableOpacity style={{
              borderColor: '#7c8791',
              alignItems: 'center',
              justifyContent: 'center',
              width: wp( 20 ),
              borderRightWidth: 1
            }}>
              <RNPickerSelect
                onValueChange={( value ) => setState( value )}
                items={state_list}
                value={state}
                placeholder={{}}
              >
                <View style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  paddingHorizontal: wp( 4 )
                }}>
                  {state_list.map(
                    ( item ) =>
                      item.value === state && (
                        <Text
                          key={item.value}
                          style={{ fontSize: 12, color: '#000', fontFamily: 'Montserrat-Regular' }}>
                          {item.label}
                        </Text>
                      )
                  )}
                  <Image source={require( '../assets/Downarrow.png' )} resizeMode='contain' style={{ height: hp( 3 ), width: wp( 8 ) }} />

                </View>
              </RNPickerSelect>

            </TouchableOpacity>
            <View style={{ width: wp( 70 ), justifyContent: 'center', alignItems: 'center', }}>
              <TextInput
                // maxLength={10}
                onChangeText={mobile => setMobile( mobile?.replace( /[^0-9]/g, '' ) )}
                // onChangeText={(mobile) => setMobile(mobile)}
                keyboardType='numeric'
                style={{
                  fontFamily: 'Montserrat-Regular',
                  fontSize: 12,
                  width: wp( 50 ),
                  color: 'black',
                }}
                placeholder={'Enter Mobile Number'}
                value={mobile}
              />
            </View>
          </View>

          {/* <View style={styles.searchSection}> 
         
                    <View style={styles.searchIcon} >
                    <Text>+91</Text>
                    </View>
                <TextInput
                 onChangeText={mobile=> setMobile(mobile?.replace(/[^0-9]/g, ''))}
                 maxLength={10}
                  // onChangeText={mobile => setMobile(mobile)}
                  value={mobile}
                 keyboardType='numeric'
                  style={styles.input}
                  placeholder={'Enter Mobile Number'}
                />
              
              </View> */}

          <TouchableOpacity style={styles.button}
            onPress={() => GotoOtp()
              // navigation.navigate('OtpScreen')
              // onPress={() =>   
              //  dispatch(sentOtp({mobile: mobile,calling_code: '+91'}))
            }
          >
            <Text style={styles.buttonText}>Request OTP</Text>
          </TouchableOpacity>



        </View>

      </View>
    </View>
  )

}

export default ForgotPassword;



const styles = StyleSheet.create( {
  container: {
    flex: 1,
    backgroundColor: '#dfdfdf'

  },

  caticon: {

    height: 50,
    borderRadius: 25,
    width: wp( '90%' ),
    padding: 90,
    alignSelf: 'center',
    backgroundColor: '#fff'
  },
  UpperBlock1: {
    height: hp( '30%' ),
    backgroundColor: '#9066e6',
    justifyContent: 'center'
  },
  logo: {
    // resizeMode:'contain',
    alignSelf: 'center',
    height: 120,
    width: 150,
    // backgroundColor:'red'

  },
  ServiceText: {
    color: '#000',
    textAlign: 'left',
    fontSize: 14,
    paddingLeft: 20,
    paddingTop: 15,
    fontFamily: 'Montserrat-SemiBold'
  },
  searchSection: {
    flexDirection: 'row',
    width: wp( '90%' ),
    backgroundColor: '#fff',
    //borderWidth:1,
    //  borderColor: '#7c8791',
    borderRadius: 5,
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 10,
    alignSelf: 'center',
    marginTop: 10,
    marginBottom: 10
  },
  searchIcon: {

    // marginTop: 10,
    // height: 25,
    // width: 27,
    // backgroundColor: 'red'
  },


  input: {
    width: '80%',
    fontFamily: 'Montserrat-Regular',
    fontSize: 12,
    color: 'black',


  },
  PriceText: {
    color: '#9066e6',
    textAlign: 'left',
    fontSize: 14,
    paddingLeft: 20,

    fontFamily: 'Montserrat-SemiBold'
  },
  discountText: {
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid',
    color: '#000',
    textAlign: 'left',
    fontSize: 14,
    paddingLeft: 20,

    fontFamily: 'Montserrat-Regular'
  },
  button: {
    marginTop: 30,
    marginHorizontal: 40,
    backgroundColor: '#9066e6',
    padding: 10,
    width: wp( '40%' ),
    borderRadius: 10,
    alignSelf: 'center'

  },
  buttonText: {
    color: 'white',
    textAlign: 'center',
    fontFamily: 'Montserrat-Regular',
    //  textTransform:'uppercase'
  },
  UpperBlock: {
    height: hp( '7%' ),
    //  width:wp('95%'),

    backgroundColor: '#9066e6',
    //   justifyContent:'center',
    alignItems: 'center',
    //  alignSelf:'center',
    flexDirection: 'row'

  },
  OTPContainer: {
    width: wp( '90%' ),
    marginTop: 20,
    alignSelf: 'center'
  },

  listIcon: {
    height: 20,
    width: 20,
    marginLeft: 10,
    // width:wp('10%')
  },
  filterIcon: {
    height: 20,
    width: 20,
    marginLeft: 15,
    //  alignSelf:'flex-end',
    width: wp( '10%' )

  },

  HomeText: {
    color: '#9066e6',
    fontSize: 24,
    fontFamily: 'Montserrat-Bold'
    // alignSelf:'center'
  },
  content: {
    //  padding: 10,
    marginTop: 20,
  },
  tabViewContainer: {
    //  marginVertical: 50,
    height: Device.height * 0.20,
  },
  maxHeight: { height: '100%' },
  tabBar: {
    height: 50,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  tabItem: {
    width: '50%',
    height: 50,
    backgroundColor: Colors.white,
    justifyContent: 'center',
    alignItems: 'center',
  },
  tabTitle: {
    fontSize: 15,
    color: Colors.black,
  },


  content: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 16,
    marginTop: 50,

  },

  searchSection: {
    flexDirection: 'row',
    width: wp( '75%' ),
    margin: 1,
    borderWidth: 1,
    borderColor: '#7c8791',
    borderRadius: 30,
    //paddingTop:2,
    paddingLeft: 10,
    alignSelf: 'center'
  },

  OtpsearchSection: {

    flexDirection: 'row',
    width: wp( '75%' ),
    margin: 15,
    borderWidth: 1,
    borderColor: '#7c8791',
    borderRadius: 30,
    paddingTop: 3,
    paddingLeft: 10,
    alignSelf: 'center'

  },

  searchIcon: {

    //marginTop:10,
    //height:25,
    padding: 5,
    // width: 35,
    width: wp( 12 ),
    textAlign: 'center',
    justifyContent: 'center',
    borderRightWidth: 1,
    borderColor: '#7c8791',
    //backgroundColor:'red'
  },

  form: {
    width: '100%',
  },

  input: {

    width: '80%',
    // backgroundColor:'red',
    fontFamily: 'Montserrat-Regular',
    fontSize: 12,
    color: 'black',
    textAlign: 'center',

  },

  inputpassword: {

    width: '80%',
    marginBottom: 2,
    fontFamily: 'Montserrat-Regular',
    fontSize: 12,
    color: 'black',
    textAlign: 'center'

  },

  submitBtn: {
    borderRadius: 40,
    marginTop: 40,
  },

  button: {
    marginTop: 30,
    marginHorizontal: 40,
    backgroundColor: '#9066e6',
    padding: 10,
    width: wp( '40%' ),
    borderRadius: 40,
    alignSelf: 'center'

  },

  buttonText: {
    color: 'white',
    textAlign: 'center',
    fontFamily: 'Montserrat-Regular',
    //  textTransform:'uppercase'
  },


} )


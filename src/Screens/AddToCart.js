import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, ActivityIndicator, TextInput, BackHandler, FlatList, Button, ScrollView, } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import moment from "moment";
import { useIsFocused, useNavigation } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import { Rating, AirbnbRating } from 'react-native-ratings';
import { addItemToCart, emptyCart, removeItem } from '../Redux/Action/CartActions';
import { SafeAreaView } from 'react-native-safe-area-context';


export default function AddToCart({ props, route }) {
    const navigation = useNavigation();

    const dispatch = useDispatch();

    const { loading } = useSelector(state => state.UserReducers);

    const cartList = useSelector((state) => state.cartItemsReducer.cartItems);


    const total = cartList.reduce((prev, cur) => {
        return prev + cur.quantity * cur.price;
    }, 0);


    const [cart, setCart] = useState([
        {
            id: 1,
            name: 'Painting',
            Location: 'Loreum Ipsum',
            price: '$500',
            image: require('../assets/serices.jpg'),
        },
        {
            id: 2,
            name: 'Painting',
            Location: 'Loreum Ipsum',
            price: '$500',
            image: require('../assets/serices.jpg'),

        },


    ]);

    const ratingCompleted = (rating) => {
        console.log("Rating is: " + rating)
    }


    return (
        <View style={styles.container}>
            <SafeAreaView>
                <View style={styles.headerWrapper}>
                    <View style={styles.headerAligWrapper}>
                        <TouchableOpacity onPress={() => navigation.goBack()}>
                            <Image resizeMode='contain' source={require('../assets/left-arrow.png')} style={styles.headerImageWrapper} />
                        </TouchableOpacity>
                        <Text style={styles.headingText}>Add to cart</Text>
                    </View>
                </View>

                {(loading) &&
                    <View style={{ flex: 1, justifyContent: 'center', position: 'absolute', top: '40%', left: '40%' }}>

                        <ActivityIndicator


                            size="large"
                            style={{
                                //  backgroundColor: "rgba(144,102,230,.8)",
                                height: 80,
                                width: 80,
                                zIndex: 999,

                                borderRadius: 15

                            }}

                            color="rgba(144,102,230,.8)"
                        />
                    </View>}
                <ScrollView contentContainerStyle={{ paddingBottom: hp(4) }}>
                    <View>
                        <FlatList
                            key={'_'}
                            keyExtractor={item => "_" + item.id}
                            data={cartList}
                            horizontal={false}
                            renderItem={({ item, index }) => (
                                <>
                                    <View style={styles.listWrapper}>
                                        <View style={styles.listLeftWrapper}>
                                            <Image source={{ uri: item.product_image_url }} resizeMode='contain' style={styles.listLeftImageWrapper} />
                                        </View>
                                        <View style={styles.listRightWrapper}>
                                            <View style={styles.listingRowWrapp}>
                                                <View style={styles.listingWidthWrapp}>
                                                    <Text style={styles.listingHeadingWrapper}>Name :</Text>
                                                </View>
                                                <Text style={styles.listingSubHeading}>{item.name}</Text>
                                            </View>
                                            <View style={styles.listingRowWrapp}>
                                                <View style={styles.listingWidthWrapp}>
                                                    <Text style={styles.listingHeadingWrapper}>Quantity :</Text>
                                                </View>
                                                <Text style={styles.listingSubHeading}>{item.quantity}</Text>
                                            </View>
                                            <View style={styles.listingRowWrapp}>
                                                <View style={styles.listingWidthWrapp}>
                                                    <Text style={styles.listingHeadingWrapper}>Price :</Text>
                                                </View>
                                                <Text style={styles.listingSubHeading}>$ {item.price * item.quantity}</Text>
                                            </View>

                                            <View style={{ flexDirection: 'row', marginTop: hp(1), alignItems: 'center' }}>
                                                <TouchableOpacity onPress={() => dispatch(removeItem(item))} style={{ borderWidth: 1, alignItems: 'center', paddingHorizontal: wp(2), paddingVertical: hp(.3) }}>
                                                    <Text>-</Text>
                                                </TouchableOpacity>
                                                <Text style={{ fontSize: 14, color: '#000', paddingLeft: wp(2) }}>{item.quantity}</Text>

                                                <TouchableOpacity onPress={() => dispatch(addItemToCart(item))} style={{ borderWidth: 1, marginLeft: wp(3), alignItems: 'center', paddingHorizontal: wp(2), paddingVertical: hp(.3) }}>
                                                    <Text>+</Text>
                                                </TouchableOpacity>
                                            </View>

                                            <View style={styles.ratingWrapper}>
                                                <View>
                                                    <Rating
                                                        onFinishRating={ratingCompleted}
                                                        imageSize={20}
                                                        style={{ paddingVertical: 10 }}
                                                    />
                                                </View>
                                                <TouchableOpacity onPress={() => dispatch(emptyCart(item.id))} style={styles.removeWrapper}>
                                                    <Text style={styles.listingHeadingWrapper}>Remove</Text>
                                                </TouchableOpacity>
                                            </View>

                                        </View>
                                    </View>
                                </>
                            )} />
                        <Text style={styles.cartHeading}>Cart Total </Text>
                        {/* <View style={styles.coupanwrapper}>
                        <View style={styles.counpanInputWrapper}>
                            <TextInput
                                  onChangeText={mobile=> setMobile(mobile?.replace(/[^0-9]/g, ''))}
                                style={styles.coupanInput}
                                placeholder={'Apply coupon'}
                              value={mobile}
                            />
                        </View>
                        <View style={styles.buttonCoupanWrapper}>
                            <Text style={styles.buttonCoupanText}>Apply coupon </Text>
                        </View>
                    </View> */}
                        <View style={styles.amountWhiteWrapper}>
                            <View style={styles.subTotalWrapper}>
                                <Text style={styles.listingSubHeading}>Sub Total</Text>

                                <Text style={styles.listingHeadingWrapper}>$ {total}</Text>

                            </View>
                            <View style={styles.taxWrapper}>
                                <Text style={styles.listingSubHeading}>Total Tax</Text>
                                <Text style={styles.listingHeadingWrapper}>$ 0</Text>
                            </View>
                            <View style={styles.discountWrapper}>
                                <Text style={styles.listingSubHeading}>Discount</Text>
                                <Text style={styles.discountText}>-$0</Text>
                            </View>
                            <View style={styles.amountWrapper}>
                                <Text style={styles.listingSubHeading}>Total Amount</Text>
                                <Text style={styles.listingHeadingWrapper}>$ {total}</Text>
                            </View>
                        </View>
                    </View>
                    <View>
                        <View style={styles.nextButtonWrapper}>
                            <TouchableOpacity style={styles.leftCancelButton} onPress={() => navigation.navigate('MainHome')}>
                                <Text style={styles.leftText}>Cancel</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => navigation.navigate('AddAddressCart', {
                                type: 'delimp_product',
                                productId: cartList,
                                totalPrice: total
                            })}
                                style={styles.rightNextWrappper}>
                                <Text style={styles.rightNextText}>Next</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: '#f7f5f1', },
    bookingWrapper: { backgroundColor: '#fff', paddingTop: hp(2), elevation: 3, marginHorizontal: wp(5), borderRadius: 4, marginTop: hp(2) },
    bookingTopWrapper: { flexDirection: 'row', paddingHorizontal: wp(3), justifyContent: 'space-between', alignItems: 'center' },
    bookingLeftText: { fontSize: 14, fontFamily: 'Montserrat-Medium', color: '#000' },
    headerWrapper: { backgroundColor: '#9066e6', paddingVertical: hp(2), flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' },
    headerAligWrapper: { flexDirection: 'row', alignItems: 'center' },
    headerImageWrapper: { height: hp(3), width: wp(8), marginRight: wp(6), marginLeft: wp(2) },
    headingText: { fontSize: 14, color: '#fff', fontFamily: "Montserrat-Bold" },
    listWrapper: { backgroundColor: '#fff', flexDirection: 'row', paddingVertical: hp(2), elevation: 2, marginTop: hp(2), marginHorizontal: wp(5), borderRadius: 5 },
    listLeftWrapper: { width: wp(30), alignItems: 'center', },
    listLeftImageWrapper: { height: hp(15), width: wp(30) },
    listRightWrapper: { width: wp(60), paddingLeft: wp(2.5) },
    listingRowWrapp: { flexDirection: 'row' },
    listingWidthWrapp: { width: wp(22) },
    listingHeadingWrapper: { fontSize: 14, fontFamily: "Montserrat-Bold", color: '#000' },
    listingSubHeading: { fontSize: 14, fontFamily: "Montserrat-Medium", color: '#000' },
    discountText: { fontSize: 14, fontFamily: 'Montserrat-Bold', color: '#9066e6' },
    ratingWrapper: { justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center' },
    removeWrapper: { borderWidth: 1, marginRight: wp(3), borderColor: '#000', width: wp(20), height: hp(4), justifyContent: 'center', alignItems: 'center', borderRadius: 2 },
    cartHeading: {
        fontFamily: 'Montserrat-Bold',
        fontSize: 12,
        color: '#000', marginLeft: wp(5), marginTop: hp(2)
    },
    coupanwrapper: { marginHorizontal: wp(5), marginTop: hp(2), justifyContent: 'space-between', flexDirection: 'row' },
    counpanInputWrapper: { backgroundColor: '#fff', width: wp(57), borderRadius: 4 },
    coupanInput: {
        fontFamily: 'Montserrat-Bold',
        fontSize: 12,
        color: '#000', width: wp(57), paddingLeft: wp(2),

    },
    buttonCoupanWrapper: { backgroundColor: '#9066e6', width: wp(30), borderRadius: 4, justifyContent: 'center', alignItems: 'center' },
    buttonCoupanText: {
        fontFamily: 'Montserrat-Bold',
        fontSize: 12,
        color: '#fff'
    },
    amountWhiteWrapper: { backgroundColor: '#fff', paddingHorizontal: wp(2), elevation: 2, marginHorizontal: wp(5), marginTop: hp(2), paddingVertical: hp(1) },
    subTotalWrapper: { justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center' },
    taxWrapper: { justifyContent: 'space-between', flexDirection: 'row', marginTop: hp(1), alignItems: 'center' },
    discountWrapper: { justifyContent: 'space-between', borderBottomWidth: 1, marginTop: hp(1), borderBottomColor: '#c2c2c2', paddingBottom: hp(1.5), flexDirection: 'row', alignItems: 'center' },
    amountWrapper: { justifyContent: 'space-between', flexDirection: 'row', marginTop: hp(1), alignItems: 'center' },
    nextButtonWrapper: { marginHorizontal: wp(5), marginTop: hp(2), justifyContent: 'space-between', flexDirection: 'row', },
    leftCancelButton: { width: wp(43), backgroundColor: '#fff', paddingVertical: hp(2), justifyContent: 'center', alignItems: 'center', elevation: 2 },
    leftText: {
        fontFamily: 'Montserrat-Bold',
        fontSize: 12,
        color: '#000',
    },
    rightNextWrappper: { width: wp(43), backgroundColor: '#9066e6', paddingVertical: hp(2), justifyContent: 'center', alignItems: 'center', elevation: 2 },
    rightNextText: {
        fontFamily: 'Montserrat-Bold',
        fontSize: 12,
        color: '#fff',
    },
});

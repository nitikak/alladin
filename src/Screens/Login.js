import React, { useEffect, useState } from 'react';
import {
  Text, TextInput, View, StyleSheet, Image, Keyboard, Pressable, SafeAreaView, KeyboardAvoidingView, ImageBackground,
  ScrollView, useWindowDimensions, TouchableOpacity, Alert, BackHandler, ActivityIndicator
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import OTPInputView from '@twotalltotems/react-native-otp-input'
import OTP from '../components/OtpInput';
import { UserLogin } from '../Redux/Action/userAction'
import { loading } from '../Redux/Action/userAction';
import { connect } from 'react-redux';
import { useDispatch, useSelector } from 'react-redux';
import { useIsFocused, useNavigation } from '@react-navigation/native';

function Login () {
  const navigation = useNavigation();
  const [password, setPassword] = useState( '' );
  const [mobile, setMobile] = useState( '' );

  const [loginval, setLoginval] = useState( '' );
  const [passval, setPassval] = useState( '' );
  const { loading } = useSelector( state => state.UserReducers );
  const [showpassword, setShowpassword] = useState( true )
  // const {otp} = useSelector(state => state.sendotp);

  const dispatch = useDispatch();

  useEffect( () => {
    BackHandler.addEventListener( 'hardwareBackPress', handleBackButton );

    return () => {
      BackHandler.removeEventListener( 'hardwareBackPress', handleBackButton );

      // Anything in here is fired on component unmount.

    };

  } )


  const handleBackButton = () => {

    Alert.alert(
      'Exit App',
      'Exiting the application?', [{
        text: 'Cancel',
        onPress: () => console.log( 'Cancel Pressed' ),
        style: 'cancel'
      }, {
        text: 'OK',
        onPress: () => BackHandler.exitApp()
      },], {
      cancelable: false
    }
    )

    return true;

  }

  useEffect( () => {
    const unsubscribe = navigation.addListener( 'focus', () => {
      setMobile( '' );
      setPassword( '' );
    } );

    return unsubscribe;
  }, [navigation] );


  const Login = ( mobile, password ) => {

    console.log( '>>>>>>', mobile, password )
    if ( mobile == '' ) {
      Alert.alert( 'Mobile number is required' );
    }
    else if ( mobile.length < 10 || mobile.length > 10 ) {
      Alert.alert( 'Mobile number should be 10 digit' );
    }
    else if ( password == '' ) {
      Alert.alert( 'password must be 6 characters' );
    }

    else {
      console.log( 'send data>>>>>>', { mobile: mobile, password: password } )
      dispatch( UserLogin( { mobile: mobile, password: password }, navigation ) );

    }


  }

  return (

    <ImageBackground style={{ flex: 1, width: '100%', height: '100%', }} source={require( '../assets/loginbottom.png' )} >

      {( loading ) &&
        <View style={{ flex: 1, justifyContent: 'center', position: 'absolute', top: '40%', left: '40%' }}>

          <ActivityIndicator


            size="large"
            style={{
              //  backgroundColor: "rgba(144,102,230,.8)",
              height: 80,
              width: 80,
              zIndex: 999,

              borderRadius: 15

            }}

            color="rgba(144,102,230,.8)"
          />
        </View>}

      <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{ paddingBottom: 130 }}>
        <View style={styles.content}>
          <View style={styles.form}>

            <KeyboardAvoidingView scrollEnabled>
              {/* <OTP/> */}
              <View style={styles.searchSection}>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                  <Image style={styles.searchIcon} source={require( '../assets/3.png' )} />
                </View>
                <View style={{ width: wp( 60 ), alignItems: 'center' }}>
                  <TextInput
                    // maxLength={10}
                    // onChangeText={(mobile) => setMobile(mobile.replace(/[^0-9]/g, 'mobile')}
                    onChangeText={mobile => setMobile( mobile?.replace( /[^0-9]/g, '' ) )}
                    style={{
                      width: wp( 30 ),
                      fontFamily: 'Montserrat-Regular',
                      fontSize: 11.5,
                      color: 'black',
                    }}
                    keyboardType='numeric'
                    clearButtonMode="always"
                    placeholder={'Mobile Number'}
                    onSubmitEditing={Keyboard.dismiss}
                    value={mobile}

                  />
                </View>

              </View>


              <View style={styles.searchSection}>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Image style={styles.searchIcon} source={require( '../assets/1.png' )} />
                  </View>
                  <View style={{ width: wp( 50 ), alignItems: 'center' }}>
                    <TextInput
                      secureTextEntry={showpassword}
                      onChangeText={password => setPassword( password )}
                      style={{
                        fontFamily: 'Montserrat-Regular',
                        fontSize: 11.5,
                        color: 'black', textAlign: 'center'
                      }}
                      clearButtonMode='always'
                      placeholder={'Password'}
                      value={password}
                    />
                  </View>
                </View>
                <TouchableOpacity onPress={() => setShowpassword( !showpassword )} style={{ alignItems: 'center', justifyContent: 'center' }}>

                  <Image style={styles.searchIcon} source={showpassword ? require( '../assets/HidePassword.png' ) : require( '../assets/show_Password_copy.png' )} />

                </TouchableOpacity>
              </View>

              <View
                style={{
                  marginTop: 10,
                  flexDirection: 'row',
                  width: wp( '80%' ),
                  alignSelf: 'center'

                }}>

                <Text
                  style={{
                    textAlign: 'center',
                    color: '#000',
                    marginBottom: 10,
                    fontSize: 12,
                    fontFamily: 'Montserrat-Regular',
                    width: wp( '40%' ),

                    textAlign: 'left'
                  }}>
                  Need Help
                </Text>
                <TouchableOpacity onPress={() => navigation.navigate( 'ForgotPassword' )}>
                  <Text
                    style={{
                      fontSize: 12,
                      color: '#000',
                      marginBottom: 10,
                      fontFamily: 'Montserrat-Regular',
                      width: wp( '40%' ),

                      textAlign: 'right'
                    }}>
                    Forgot Password
                  </Text>
                </TouchableOpacity>
              </View>

              <TouchableOpacity style={styles.button} onPress={() => Login( mobile, password )}>
                <Text style={styles.buttonText}>Login</Text>
              </TouchableOpacity>
            </KeyboardAvoidingView>


          </View>
        </View>
      </ScrollView>
    </ImageBackground>

  )
}

// const mapStateToProps = (sentOtp) => {
//   return {

//     sentOtp: sentOtp,



//   }
// }

export default Login;



const styles = StyleSheet.create( {
  container: {
    flex: 1,
    // backgroundColor: Colors.background,
  },
  borderStyleHighLighted: {
    borderColor: "#03DAC6",
  },

  underlineStyleBase: {
    width: 30,
    height: 45,
    borderWidth: 0,
    borderBottomWidth: 1,
  },

  underlineStyleHighLighted: {
    borderColor: "#03DAC6",
  },
  content: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 16,
    // marginTop:30,
    marginVertical: hp( 6 )


  },
  searchSection: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: wp( '75%' ),

    marginVertical: hp( 2 ),
    // margin:15,
    borderWidth: 1,
    borderColor: '#7c8791',
    borderRadius: 30,
    // paddingTop:3,
    // paddingLeft:10,
    paddingHorizontal: wp( 3 ),
    alignSelf: 'center',
    // justifyContent:'center'
  },

  OtpsearchSection: {
    flexDirection: 'row',
    width: wp( '75%' ),
    margin: 15,
    borderWidth: 1,
    borderColor: '#7c8791',
    borderRadius: 30,
    paddingTop: 3,
    paddingLeft: 10,
    alignSelf: 'center'
  },

  searchIcon: {
    height: hp( 5 ),
    width: wp( 8 ),
    alignItems: 'center',
    // marginTop:10,
    // height:25,
    // width:25,
    // backgroundColor:'red'
  },

  form: {
    width: '100%',
  },

  input: {
    width: '80%',
    fontFamily: 'Montserrat-Regular',
    fontSize: 12,
    color: 'black',
    textAlign: 'center',

  },

  inputpassword: {
    // width:'80%',
    // marginBottom: 2,
    fontFamily: 'Montserrat-Regular',
    fontSize: 12,
    color: 'black',
    textAlign: 'center'
  },

  submitBtn: {
    borderRadius: 40,
    marginTop: 40,
  },
  button: {
    marginTop: 28,
    marginHorizontal: 40,
    backgroundColor: '#9066e6',
    padding: 5,
    width: wp( '30%' ),
    borderRadius: 40,
    alignSelf: 'center'

  },
  buttonText: {
    color: 'white',
    textAlign: 'center',
    fontFamily: 'Montserrat-Regular'
  },
} );

import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, BackHandler, FlatList, Button, ScrollView, ActivityIndicator } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { SafeAreaView } from 'react-native-safe-area-context';
import DatePicker from 'react-native-date-picker'
import DateTimePicker from "@react-native-community/datetimepicker";
import moment from "moment";
import { useIsFocused, useNavigation } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import { DashboardBooking } from '../Redux/Action/bookingAction';


export default function Dashboard(props) {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const countDashboard = useSelector((state) => state.bookingReducer.getDashboardData);
  console.log('countDashboard', countDashboard)
  const { loading } = useSelector(state => state.UserReducers);

  useEffect(() => {
    dispatch(DashboardBooking());
  }, [])

  const [bookings, setBookings] = useState([
    {
      id: 1,
      color: '#2ea749',
      image: require('../assets/Booking.png'),
      title: 'Completed Booking',
      number: '100',
    },
    {
      id: 2,
      color: '#f2ac00',
      image: require('../assets/Booking.png'),
      title: 'Pending Booking',
      number: '50',
    },
    {
      id: 3,
      color: '#23a2b7',
      image: require('../assets/Booking.png'),
      title: 'Approved Booking',
      number: '5',
    },
    {
      id: 4,
      color: '#157dfc',
      image: require('../assets/Booking.png'),
      title: 'In Progress Booking',
      number: '140',
    },
    {
      id: 5,
      color: '#da3348',
      image: require('../assets/Booking.png'),
      title: 'Cancelled Booking',
      number: '21',
    },
    {
      id: 6,
      color: '#6c757d',
      image: require('../assets/WalkInBookings.png'),
      title: 'Walk-In Booking',
      number: '1',
    },
    {
      id: 7,
      color: '#23a2b7',
      image: require('../assets/Booking.png'),
      title: 'Online Booking',
      number: '32',
    },
    {
      id: 8,
      color: '#343a40',
      image: require('../assets/TotalCustomers.png'),
      title: 'Total Customers',
      number: '425',
    },
    {
      id: 9,
      color: '#2ea749',
      image: require('../assets/TotalEarnings.png'),
      title: 'Total Earings',
      number: '1200',
    }
  ]);

  const [date, setDate] = useState(new Date(1598051730000));
  const [date2, setDate2] = useState(new Date(1598051730000));
  const [show, setShow] = useState(false);
  const [showfix, setShowFix] = useState(false);

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(Platform.OS === 'ios');
    setDate(currentDate);
  };
  const onChange2 = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShowFix(Platform.OS === 'ios');
    setDate2(currentDate);
  };

  const showMode = (currentMode) => {
    setShow(true);
    setShowFix(false);
  };
  const showMode2 = (currentMode) => {
    setShow(false);
    setShowFix(true);
  };

  const showDatepicker = () => {
    showMode('date');
  };

  const showDatepicker2 = () => {
    showMode2('date');
  };


  return (
    <View style={styles.container}>
      <SafeAreaView>
        {(loading) &&
          <View style={{ flex: 1, justifyContent: 'center', position: 'absolute', top: '40%', left: '40%' }}>

            <ActivityIndicator


              size="large"
              style={{
                //  backgroundColor: "rgba(144,102,230,.8)",
                height: 80,
                width: 80,
                zIndex: 999,

                borderRadius: 15

              }}

              color="rgba(144,102,230,.8)"
            />
          </View>}
        <View style={{ backgroundColor: '#9066e6', paddingVertical: hp(2), flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <Image resizeMode='contain' source={require('../assets/left-arrow.png')} style={{ height: hp(3), width: wp(8), marginRight: wp(6), marginLeft: wp(2) }} />
            </TouchableOpacity>
            <Text style={{ fontSize: 14, color: '#fff', fontFamily: "Montserrat-Bold" }}>DashBoard</Text>
          </View>
        </View>

        <ScrollView contentContainerStyle={{ paddingBottom: hp(2) }}>
          <View style={{ marginTop: hp(2), marginHorizontal: wp(5) }}>
            <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 13, color: '#000' }}>From</Text>

          </View>
          <TouchableOpacity onPress={showDatepicker} style={{ marginTop: hp(2), backgroundColor: '#fff', marginHorizontal: wp(5), paddingVertical: hp(1.5), elevation: 2 }}>
            <Text style={{ fontFamily: "Montserrat-Regular", fontSize: 14, paddingLeft: wp(3), color: '#c2c2c2' }}>{moment(date).format("DD-MM-YYYY ")}</Text>
          </TouchableOpacity>
          <View style={{ marginTop: hp(2), marginHorizontal: wp(5) }}>
            <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 13, color: '#000' }}>To</Text>

          </View>
          <TouchableOpacity onPress={showDatepicker2} style={{ marginTop: hp(3), backgroundColor: '#fff', marginHorizontal: wp(5), paddingVertical: hp(1.5), elevation: 2 }}>
            <Text style={{ fontFamily: "Montserrat-Regular", fontSize: 14, paddingLeft: wp(3), color: '#c2c2c2' }}>{moment(date2).format("DD-MM-YYYY ")}</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: '#9066e6', width: wp(50), borderRadius: 20, paddingVertical: hp(1.2), marginVertical: hp(2), alignSelf: 'center' }}>
            <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 13, color: '#fff' }}>Apply</Text>
          </TouchableOpacity>

          {show && (
            <DateTimePicker
              testID="dateTimePicker"
              value={new Date()}
              mode='date'
              display="default"
              onChange={onChange}
            />
          )}
          {showfix && (
            <DateTimePicker
              testID="dateTimePicker"
              value={new Date()}
              mode='date'
              display="default"
              onChange={onChange2}
            />
          )}

          <View style={{ marginLeft: wp(1) }}>
            <Text style={{ fontSize: 14, marginLeft: wp(3), fontFamily: "Montserrat-Bold", color: '#000', marginVertical: hp(2) }}>Total Booking : 0</Text>
            <View style={{ alignItems: 'center' }}>
              <FlatList
                data={countDashboard}
                keyExtractor={(item, index) => index}
                numColumns={3}
                renderItem={({ item, index }) => (
                  <View style={{ justifyContent: 'center', marginTop: hp(1), width: wp(30), }}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('BookingServices')} style={{ height: hp(14), width: wp(28), backgroundColor: item.status === 'Completed' ? "#2ea749" : item.status === "Pending" ? "#f2ac00" : item.status === "Approved" ? '#23a2b7' : item.status === "In Progress" ? '#157dfc' : item.status === "Cancelled" ? '#da3348' : item.status === "Total" ? '#2ea749' : null, alignItems: 'center', borderRadius: 4, justifyContent: 'center' }}>
                      <Image source={require('../assets/Booking.png')} resizeMode='contain' style={{ height: hp(5), width: wp(10), }} />
                      <Text style={{ color: '#fff', fontSize: 15, fontFamily: "Montserrat-Bold", marginTop: hp(2) }}>{item.count}</Text>
                    </TouchableOpacity>
                    <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 10, color: '#000', textAlign: 'center' }}>{item.status}</Text>
                  </View>
                )}
              />
            </View>
          </View>

          <View>

          </View>

        </ScrollView>
      </SafeAreaView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#f7f5f1', },
  containerText: { fontSize: 27, color: '#000' },
});

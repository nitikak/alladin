import React, { useEffect, useState } from 'react';
import {
    Text, TextInput, View, StyleSheet, Image, useWindowDimensions, TouchableOpacity, ActivityIndicator, ScrollView, Alert
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { useDispatch, useSelector } from 'react-redux';
import { useIsFocused, useNavigation } from '@react-navigation/native';
import { CreatingProductCard } from '../Redux/Action/bookingAction';
import moment from "moment";
import CalendarStrip from 'react-native-calendar-strip';

import DateTimePicker from "@react-native-community/datetimepicker";
import { SafeAreaView } from 'react-native-safe-area-context';


const AddAddressCart = ( { route } ) => {
    const { width } = useWindowDimensions();
    const dispatch = useDispatch();
    const navigation = useNavigation();
    const [loading, setLoading] = useState( false );
    const cartList = useSelector( ( state ) => state.cartItemsReducer.cartItems );
    console.log( 'cartList====>', cartList )

    const [address, setAddress] = useState( '' );
    const [pincode, setPicode] = useState( '' );

    const [date, setDate] = useState( new Date( 1598051730000 ) );
    const [show, setShow] = useState( false );
    const [showfix, setShowFix] = useState( false );

    // const Products = useSelector((state) => state.productsReducer.detailProduct);



    const productType = route.params.type;
    const priceTotal = route.params.totalPrice;

    const onChange = ( event, selectedDate ) => {
        const currentDate = selectedDate || date;
        setShow( Platform.OS === 'ios' );
        setDate( currentDate );
    };
    const showMode = ( currentMode ) => {
        setShow( true );
        setShowFix( false );
    };
    const showDatepicker = () => {
        showMode( 'date' );
    };


    var number_id = [];
    var number_price = [];
    var number_quantity = [];
    var number_discountPrice = [];
    var number_netPrice = [];




    cartList.forEach( ( item, arr ) => {


        // product id show the data 
        var Product_Id = item.id;
        number_id.push( Product_Id.toString() );

        // location id show 
        // var Location = item.location_id;
        // number_location_id.push(Location.toString());

        //company id show
        // var Company = item.company_id;
        // number_company_id.push(Company.toString());

        // price show data
        var Price = item.price;
        number_price.push( Price.toString() );

        // quantity show 
        var Quantity = item.quantity;
        number_quantity.push( Quantity.toString() );

        // discounted price
        var Discount_price = item.discounted_price;
        number_discountPrice.push( Discount_price.toString() );

        // net price 
        var NetPrice = item.net_price;
        number_netPrice.push( NetPrice.toString() );


    } );

    const bookingFunction = () => {
        if ( address === '' ) {
            Alert.alert( 'Enter the address' );

        } else if ( pincode === '' ) {
            Alert.alert( 'Enter the picode ' );

        }
        else {
            if ( productType === 'delimp_product' ) {
                dispatch( CreatingProductCard( {
                    product_id: number_id,
                    product_company_id: '1',
                    product_location_id: '1',
                    type: 'delimp_product',
                    // product_type: route.params.type,
                    service_address: address + '' + pincode,
                    product_quantity: number_quantity,
                    actual_price: number_price,
                    price: number_netPrice,
                    discount: number_discountPrice,

                }, navigation ) );
            }

        }


    }

    return (

        <View style={styles.container}>

            <SafeAreaView>

                <View style={styles.UpperBlock}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <Image style={styles.listIcon} source={require( '../assets/left-arrow.png' )} />

                    </TouchableOpacity>
                    <Text numberOfLines={1} style={styles.headingText}>CheckOut </Text>
                </View>

                {( loading ) &&
                    <View style={{ flex: 1, justifyContent: 'center', position: 'absolute', top: '50%', left: '40%' }}>

                        <ActivityIndicator


                            size="large"
                            style={{
                                backgroundColor: "rgba(20,116,240,.8)",
                                height: 80,
                                width: 80,
                                zIndex: 999,
                                borderRadius: 15
                            }}
                            color="#ffffff"
                        />
                    </View>}

                <ScrollView>
                    <View style={styles.catBox}>

                        {/* <View style={styles.dateWrapper}>
                            <Text style={styles.dateTextWrapper}>Date</Text>

                        </View> */}

                        {/* <TouchableOpacity onPress={showDatepicker} style={{ marginTop: hp(2), backgroundColor: '#fff', marginHorizontal: wp(5), paddingVertical: hp(1.5), elevation: 2 }}>
                            <Text style={{ fontFamily: "Montserrat-Regular", fontSize: 14, paddingLeft: wp(3), color: '#c2c2c2' }}>{moment(date).format("DD-MM-YYYY ")}</Text>
                        </TouchableOpacity> */}

                        {show && (
                            <DateTimePicker
                                testID="dateTimePicker"
                                value={new Date()}
                                mode='date'
                                display="default"
                                onChange={onChange}
                            />
                        )}
                        <Text style={styles.addressAdd}>
                            Enter the Address
                        </Text>

                        <View style={{ borderWidth: 1, backgroundColor: '#fff', borderColor: '#c2c2c2', marginHorizontal: wp( 5 ), marginTop: Platform.OS === 'ios' ? hp( 1 ) : hp( 3 ) }}>
                            <TextInput
                                placeholder='Enter the address'
                                // numberOfLines={1}
                                style={{ paddingLeft: wp( 3 ), textAlignVertical: 'top', fontSize: 15, paddingVertical: Platform.OS === 'ios' ? hp( 1.5 ) : hp( 0 ) }}
                                value={address}
                                onChangeText={( text ) => setAddress( text )}
                            />
                        </View>

                        <Text style={styles.addressAdd}>
                            Enter the Pincode
                        </Text>

                        <View style={{ borderWidth: 1, backgroundColor: '#fff', borderColor: '#c2c2c2', marginHorizontal: wp( 5 ), marginVertical: Platform.OS === 'ios' ? hp( 2 ) : hp( 3 ) }}>
                            <TextInput
                                placeholder='Enter the Pincode'
                                // numberOfLines={1}
                                keyboardType='number-pad'
                                style={{ paddingLeft: wp( 3 ), textAlignVertical: 'top', fontSize: 15, paddingVertical: Platform.OS === 'ios' ? hp( 1.5 ) : hp( 0 ) }}
                                value={pincode}
                                onChangeText={( text ) => setPicode( text )}
                            />
                        </View>
                    </View>

                    <View style={{ flex: 0.2 }}>
                        <View style={styles.bookWrapper}>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={styles.bookLeftTextWrapper}>
                                    ${priceTotal}
                                </Text>
                            </View>
                            {/* {
                            order === true ? */}
                            <TouchableOpacity
                                onPress={() => bookingFunction()}
                                // onPress={() => navigation.navigate('CardDetails')}
                                style={styles.rightBookWrapper}>
                                <Text style={styles.rightBookText}>Place Order</Text>
                            </TouchableOpacity>
                            {/* : null

                        } */}


                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        </View>
    )
}

export default AddAddressCart;


const styles = StyleSheet.create( {
    container: {
        flex: 1,
        backgroundColor: '#dfdfdf'
    },

    caticon: {
        height: hp( '40%' ),
        width: wp( '100%' ),
        backgroundColor: 'transparent',
        // backgroundColor:'red',
        marginTop: hp( 2 )
    },
    headingTextProduct: { fontSize: 18, color: '#000', fontFamily: 'Montserrat-SemiBold' },
    locationWrapper: { marginHorizontal: wp( 5 ), justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', marginVertical: hp( 2 ) },
    leftLocationWrapper: { fontSize: 14, color: '#c2c2c2', fontFamily: 'Montserrat-Regular' },
    rightlocationWrapper: { fontSize: 14, color: '#000', fontFamily: 'Montserrat-Regular' },
    editImageWrapper: { height: hp( 3 ), width: wp( 6 ), marginLeft: wp( 1 ) },
    editfirstHeading: { marginHorizontal: wp( 5 ), marginVertical: hp( 2 ) },
    editTextFirstHeading: { fontSize: 14, color: '#000', fontFamily: 'Montserrat-SemiBold' },
    selectedTextWrapper: { alignItems: 'center', justifyContent: 'center', height: hp( 5 ), paddingHorizontal: wp( 2 ), flexDirection: 'row', marginLeft: wp( 5 ), borderRadius: 3 },
    selectedText: { fontSize: 12, fontFamily: 'Montserrat-SemiBold' },
    dateWrapper: { marginHorizontal: wp( 5 ), marginTop: hp( 2 ) },
    dateTextWrapper: { fontSize: 14, color: '#000', fontFamily: 'Montserrat-SemiBold' },
    timeWrapper: { alignItems: 'center', justifyContent: 'center', height: hp( 5 ), width: wp( 20 ), flexDirection: 'row', marginLeft: wp( 5 ), borderRadius: 3, marginVertical: hp( 2 ) },
    timeText: { fontSize: 12, fontFamily: 'Montserrat-SemiBold' },
    bookWrapper: { marginHorizontal: wp( 5 ), paddingBottom: hp( 1 ), flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' },
    bookLeftTextWrapper: { fontSize: 17, color: '#000', fontFamily: 'Montserrat-SemiBold' },
    rightBookWrapper: { backgroundColor: '#9066e6', width: wp( 35 ), paddingHorizontal: wp( 3 ), paddingVertical: hp( 1 ), alignItems: 'center', justifyContent: 'center', borderRadius: 5 },
    rightBookText: { fontSize: 13, color: '#fff', fontFamily: 'Montserrat-SemiBold' },
    addressAdd: { marginHorizontal: wp( 5 ), fontSize: 14, fontFamily: 'Montserrat-SemiBold', marginTop: hp( 2 ) },
    ServiceText: {
        color: '#000',
        fontSize: 14,
        paddingHorizontal: wp( 5 ),
        paddingTop: hp( 3 ),
        fontFamily: 'Montserrat-SemiBold'
    },
    searchSection: {
        flexDirection: 'row',
        width: wp( '95%' ),
        backgroundColor: '#fff',
        borderRadius: 5,
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 10,
        alignSelf: 'center',
        marginTop: 10,
        marginBottom: 10
    },
    searchIcon: {
        marginTop: 10,
        height: 25,
        width: 25,
    },
    input: {
        width: '80%',
        fontFamily: 'Montserrat-Regular',
        fontSize: 12,
        color: 'black',
    },
    PriceText: {
        color: '#000',
        fontSize: 14,
        fontFamily: 'Montserrat-SemiBold'
    },

    NameText: {
        color: '#000',
        fontSize: 12,
        fontFamily: 'Montserrat-Regular'
    },
    description: {
        color: '#c2c2c2',
        fontSize: 12,
        paddingLeft: 10,
        paddingRight: 10,
        fontFamily: 'Montserrat-Regular'
    },
    UpperBlock: {
        height: hp( '7%' ),
        backgroundColor: '#9066e6',
        alignItems: 'center',
        flexDirection: 'row'
    },
    listIcon: {
        height: 20,
        width: 20,
        marginLeft: 10,
    },
    lineWrapper: {
        backgroundColor: '#c2c2c2',
        height: hp( .1 ),
        marginTop: hp( 2 )
    },
    headingText: { width: wp( '70%' ), marginLeft: 15, fontSize: 14, paddingTop: 5, color: '#fff', fontFamily: 'Montserrat-SemiBold' },

} )


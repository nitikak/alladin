import React, { useEffect, useState } from 'react';
import {
    Text, TextInput, View, StyleSheet, Image, Pressable, KeyboardAvoidingView, SafeAreaView,
    FlatList, useWindowDimensions, TouchableOpacity, ActivityIndicator, ScrollView, Alert, Modal
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Colors from '../Layout/Colors'
import Device from './Device';
import { verifyOtp } from '../Redux/Action/SendOtp'
import { useDispatch, useSelector } from 'react-redux';
import { useIsFocused, useNavigation } from '@react-navigation/native';
import RenderHtml from 'react-native-render-html';
import WeekCalendar from '../components/WeekCalender/index.tsx';
import { CreatingServices } from '../Redux/Action/bookingAction';
import moment from "moment";
import CalendarStrip from 'react-native-calendar-strip';
import ImgToBase64 from 'react-native-image-base64';
import ImagePicker from 'react-native-image-crop-picker';

// import {ServiceDetail} from '../Redux/Action/ServiceAction';

const PropertyDetail = ( { route } ) => {
    const { width } = useWindowDimensions();
    const dispatch = useDispatch();
    const navigation = useNavigation();
    const [loading, setLoading] = useState( false );
    const { ServiceDetail } = useSelector( state => state.Services );

    const [selectRoom, setSelectRoom] = useState( '1' )
    const [cleaningdata, setCleaningData] = useState( '' )
    const [days, setDays] = useState( [
        {
            id: 1,
            title: 'Today'
        },
        {
            id: 2,
            title: 'Tomorrow'
        },
        {
            id: 3,
            title: 'This Week'
        },
        {
            id: 4,
            title: 'Last Week'
        },


    ] )
    const [date, setDate] = useState( new Date() );
    const [time, setTime] = useState( [
        {
            id: 1,
            timeShow: '9:00'
        },
        {
            id: 2,
            timeShow: '10:00'
        },
        {
            id: 3,
            timeShow: '11:00'
        },
        {
            id: 4,
            timeShow: '12:00'
        },
    ] )
    const [timeSelected, setTimeSelected] = useState( '9:00' )
    const [customMessage, setCustomMessage] = useState( 'Services Booking' )
    const [address, setAddress] = useState( '' );
    const [showimage, setShowImage] = useState( '' );


    const Products = useSelector( ( state ) => state.productsReducer.detailProduct );


    const productType = route.params.type;
    const serviceType = route.params.typeservice;

    const showId = route.params.productId;
    const [image, setImage] = useState( null );
    const [description, setDescription] = useState( '' )
    const [modalServices, setModalServices] = useState( false );

    const base64Imgae = `data:image/png;base64,${showimage}`

    const bookingFunction = () => {

        if ( address === '' ) {
            Alert.alert( 'Enter the address' );
        } else {

            dispatch( CreatingServices( {
                service_id: route.params.serviceId,
                company_id: ServiceDetail.service.company_id,
                location_id: ServiceDetail.service.location_id,
                type: route.params.typeservice,
                service_booking_date: selectRoom,
                service_address: address,
                image: base64Imgae,
                description: description,
            }, navigation ) );
        }
    }

    const takePhotoFromCamera = () => {

        // setModalServices(false)
        ImagePicker.openCamera( {
            width: 300,
            height: 400,
            cropping: true,
        } ).then( image => {
            console.log( image );
            imageUpload( image.path )
        } );
    }

    const choosePhotoFromLibrary = () => {
        // setModalServices(false)
        ImagePicker.openPicker( {
            width: 300,
            height: 400,
            cropping: true
        } ).then( image => {
            console.log( "selected Image", image )
            console.log( "selected Image", image.path )

            imageUpload( image.path )
        } );
    }

    const imageUpload = ( imagePath ) => {
        setModalServices( false )
        console.log( 'imageData==========>Before ', imagePath )

        // const imageData = new FormData();
        // imageData.append("image", {
        //   uri: imagePath.uri,
        //   name: "image.png",

        //   type: "image/jpeg"
        // })
        console.log( "form data", imagePath )
        setImage( imagePath )


    }
    ImgToBase64.getBase64String( image )
        .then( ( base64String ) => {
            setShowImage( base64String )
            console.log( showimage )
        } )
        .catch( err => doSomethingWith( err ) );





    return (

        <View style={styles.container}>

            <SafeAreaView>

                <View style={styles.UpperBlock}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <Image style={styles.listIcon} source={require( '../assets/left-arrow.png' )} />

                    </TouchableOpacity>
                    <Text numberOfLines={1} style={styles.headingText}>{productType === 'delimp_product' ? Products.name : ServiceDetail.service.name}</Text>
                </View>

                {( loading ) &&
                    <View style={{ flex: 1, justifyContent: 'center', position: 'absolute', top: '50%', left: '40%' }}>

                        <ActivityIndicator


                            style={{
                                backgroundColor: "rgba(20,116,240,.8)",
                                height: 80,
                                width: 80,
                                zIndex: 999,
                                borderRadius: 15
                            }}
                            size="small"
                            color="#ffffff"
                        />
                    </View>}

                <ScrollView style={{ marginBottom: hp( 7 ) }}>
                    <View style={styles.catBox}>
                        <Image resizeMode='contain' style={styles.caticon} source={{ uri: productType === 'delimp_product' ? Products.product_image_url : ServiceDetail.service.service_image_url }} />
                        <View style={{ marginHorizontal: wp( 5 ) }}>
                            <Text style={styles.headingTextProduct}>{productType === 'delimp_product' ? Products.name : ServiceDetail.service.name}</Text>
                        </View>

                        <View style={styles.locationWrapper}>
                            <Text style={styles.leftLocationWrapper}>Location</Text>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={styles.rightlocationWrapper}>Home,land,Town</Text>
                                <TouchableOpacity>
                                    <Image source={require( '../assets/editblack.png' )} resizeMode='contain' style={styles.editImageWrapper} />
                                </TouchableOpacity>
                            </View>
                        </View>

                        {/* <View style={styles.editfirstHeading}>
                        <Text style={styles.editTextFirstHeading}>Which hair styles you want to cut ? </Text>

                    </View> */}

                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>

                            {days.length &&
                                days.map( ( item, index ) => {
                                    return (
                                        <TouchableOpacity key={item.id} onPress={() => setSelectRoom( item.id )} style={[styles.selectedTextWrapper, { backgroundColor: item.id === selectRoom ? '#9066e6' : '#fff', }]}>
                                            <Text style={[styles.selectedText, { color: item.id === selectRoom ? '#fff' : '#000', }]}>{item.title}</Text>
                                        </TouchableOpacity>
                                    )
                                } )
                            }

                        </ScrollView>

                        {/* <View style={styles.dateWrapper}>
                            <Text style={styles.dateTextWrapper}>Date & Time</Text>

                        </View> */}
                        {/* <View style={{ marginHorizontal: wp(5), flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ fontSize: 15, color: '#000', fontFamily: 'Montserrat-Regular' }}>May 2021</Text>
                        </View>
                        <TouchableOpacity>
                            <Image source={require('../assets/timeicon.png')} resizeMode='contain' style={{ height: hp(7), width: wp(14) }} />
                        </TouchableOpacity>
                    </View> */}
                        {/* <CalendarStrip

                            dateNameStyle={{ color: '#000', fontFamily: 'Montserrat-SemiBold', fontSize: 10 }}
                            dateNumberStyle={{ color: '#000', fontFamily: 'Montserrat-SemiBold', fontSize: 14 }}
                            highlightDateNameStyle={{ color: '#fff', fontFamily: 'Montserrat-SemiBold', fontSize: 10 }} // week show
                            dayContainerStyle={{ backgroundColor: '#fff', color: '#000', height: hp(8), width: wp(10), borderRadius: 0, borderColor: '#c2c2c2', borderWidth: 1 }}
                            highlightDateNumberStyle={{ color: '#fff', fontFamily: 'Montserrat-SemiBold', fontSize: 14 }} //date show 
                            highlightDateContainerStyle={{ backgroundColor: '#9066e6', width: wp(10), borderColor: '#c2c2c2', height: hp(8), borderRadius: 0, borderWidth: 1, }}
                            // startingDate={date}
                            selectedDate={moment(date).format('YYYY-MM-DD')}
                            style={{ height: 120, paddingTop: 15, paddingBottom: 5, }}
                            onDateSelected={(newDate => setDate(newDate))}
                        /> */}
                        {/* <WeekCalendar date={date} onChange={(newDate) => setDate(newDate)} /> */}
                        {/* <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} contentContainerStyle={{ paddingBottom: hp(2) }}>

                        {time.length &&
                            time.map((item, index) => {
                                return (
                                    <TouchableOpacity onPress={() => setTimeSelected(item.timeShow)} style={[styles.timeWrapper,{backgroundColor: item.timeShow === timeSelected ? '#9066e6' : '#fff',}]}>
                                        <Text style={[styles.timeText,{color: item.timeShow === timeSelected ? '#fff' : '#000',}]}>{item.timeShow}</Text>
                                    </TouchableOpacity>
                                )
                            })
                        }

                    </ScrollView> */}

                        <View style={{ borderWidth: 1, borderColor: '#c2c2c2', marginHorizontal: wp( 5 ), marginVertical: hp( 3 ) }}>
                            <TextInput
                                placeholder='Enter the address'
                                numberOfLines={5}
                                style={{ paddingLeft: wp( 5 ), textAlignVertical: 'top', fontSize: 17, paddingVertical: hp( 2 ) }}
                                value={address}
                                onChangeText={( text ) => setAddress( text )}
                            />
                        </View>


                        <View style={{ marginHorizontal: wp( 5 ), paddingVertical: hp( .5 ), flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View style={{ width: wp( 60 ), height: hp( 10 ), borderWidth: 1, borderColor: '#c2c2c2', alignItems: 'center', justifyContent: 'center' }}>
                                <Image source={{ uri: image }} resizeMode='stretch' style={{ height: hp( 10 ), width: wp( 30 ) }} />
                            </View>
                            <TouchableOpacity style={{ width: wp( 30 ), alignItems: 'center', justifyContent: 'center' }} onPress={() => setModalServices( true )}>
                                <Image source={require( '../assets/addicon.png' )} resizeMode='contain' style={{ height: hp( 9 ), width: wp( 17 ) }} />
                            </TouchableOpacity>
                        </View>

                        <View style={{ borderWidth: 1, borderColor: '#c2c2c2', marginHorizontal: wp( 5 ), marginVertical: hp( 3 ) }}>
                            <TextInput
                                placeholder='Description'
                                numberOfLines={5}
                                style={{ paddingLeft: wp( 5 ), textAlignVertical: 'top', fontSize: 17, paddingVertical: hp( 2 ) }}
                                value={description}
                                onChangeText={( text ) => setDescription( text )}
                            />
                        </View>
                    </View>

                    <View style={{ flex: 0.2 }}>
                        <View style={styles.bookWrapper}>
                            <View style={{ flexDirection: 'row' }}>
                                {/* <Text style={styles.bookLeftTextWrapper}>{productType === 'delimp_product' ? Products.formated_price : ServiceDetail.service.formated_price}</Text> */}
                            </View>


                            <TouchableOpacity
                                onPress={() => bookingFunction()}
                                // onPress={() => navigation.navigate('CardDetails')}
                                style={styles.rightBookWrapper}>
                                <Text style={styles.rightBookText}>Book</Text>
                            </TouchableOpacity>



                        </View>
                    </View>

                    <Modal animationType="slide"
                        transparent={true}
                        visible={modalServices}
                        onRequestClose={() => {
                            setModalServices( false );
                        }}>
                        <View style={styles.modalWrapper}>
                            <View style={styles.modalCont}>
                                <View style={styles.panel}>
                                    <View style={{ alignItems: 'center' }}>
                                        <Text style={styles.panelTitle}>Upload Photo</Text>
                                        <Text style={styles.panelSubtitle}>Choose Your Profile Picture</Text>
                                    </View>
                                    <TouchableOpacity style={styles.panelButton} onPress={takePhotoFromCamera}>
                                        <Text style={styles.panelButtonTitle}>Take Photo</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.panelButton} onPress={choosePhotoFromLibrary}>
                                        <Text style={styles.panelButtonTitle}>Choose From Library</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        style={styles.panelButton}
                                        // onPress={() => this.bs.current.snapTo(1)}
                                        onPress={() => setModalServices( false )}
                                    >
                                        <Text style={styles.panelButtonTitle}>Cancel</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>

                    </Modal>
                </ScrollView>
            </SafeAreaView>
        </View>
    )
}

export default PropertyDetail;


const styles = StyleSheet.create( {
    container: {
        flex: 1,
        backgroundColor: '#dfdfdf'
    },

    caticon: {
        height: hp( '40%' ),
        width: wp( '100%' ),
        backgroundColor: 'transparent',
        // backgroundColor:'red',
        // marginTop: hp(2)
    },
    headingTextProduct: { fontSize: 18, color: '#000', fontFamily: 'Montserrat-SemiBold' },
    locationWrapper: { marginHorizontal: wp( 5 ), justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', marginVertical: hp( 2 ) },
    leftLocationWrapper: { fontSize: 14, color: '#c2c2c2', fontFamily: 'Montserrat-Regular' },
    rightlocationWrapper: { fontSize: 14, color: '#000', fontFamily: 'Montserrat-Regular' },
    editImageWrapper: { height: hp( 3 ), width: wp( 6 ), marginLeft: wp( 1 ) },
    editfirstHeading: { marginHorizontal: wp( 5 ), marginVertical: hp( 2 ) },
    editTextFirstHeading: { fontSize: 14, color: '#000', fontFamily: 'Montserrat-SemiBold' },
    selectedTextWrapper: { alignItems: 'center', justifyContent: 'center', height: hp( 5 ), width: wp( 25 ), flexDirection: 'row', marginLeft: wp( 5 ), borderRadius: 3 },
    selectedText: { fontSize: 12, fontFamily: 'Montserrat-SemiBold' },
    dateWrapper: { marginHorizontal: wp( 5 ), marginVertical: hp( 2 ) },
    dateTextWrapper: { fontSize: 14, color: '#000', fontFamily: 'Montserrat-SemiBold' },
    timeWrapper: { alignItems: 'center', justifyContent: 'center', height: hp( 5 ), width: wp( 20 ), flexDirection: 'row', marginLeft: wp( 5 ), borderRadius: 3, marginVertical: hp( 2 ) },
    timeText: { fontSize: 12, fontFamily: 'Montserrat-SemiBold' },
    bookWrapper: { marginHorizontal: wp( 5 ), paddingBottom: hp( 1 ), flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' },
    bookLeftTextWrapper: { fontSize: 17, color: '#000', fontFamily: 'Montserrat-SemiBold' },
    rightBookWrapper: { backgroundColor: '#9066e6', width: wp( 30 ), paddingHorizontal: wp( 3 ), paddingVertical: hp( 1 ), alignItems: 'center', justifyContent: 'center', borderRadius: 5 },
    rightBookText: { fontSize: 13, color: '#fff', fontFamily: 'Montserrat-SemiBold' },
    ServiceText: {
        color: '#000',
        fontSize: 14,
        paddingHorizontal: wp( 5 ),
        paddingTop: hp( 3 ),
        fontFamily: 'Montserrat-SemiBold'
    },
    searchSection: {
        flexDirection: 'row',
        width: wp( '95%' ),
        backgroundColor: '#fff',
        borderRadius: 5,
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 10,
        alignSelf: 'center',
        marginTop: 10,
        marginBottom: 10
    },
    searchIcon: {
        marginTop: 10,
        height: 25,
        width: 25,
    },
    input: {
        width: '80%',
        fontFamily: 'Montserrat-Regular',
        fontSize: 12,
        color: 'black',
    },
    PriceText: {
        color: '#000',
        fontSize: 14,
        fontFamily: 'Montserrat-SemiBold'
    },

    NameText: {
        color: '#000',
        fontSize: 12,
        fontFamily: 'Montserrat-Regular'
    },
    description: {
        color: '#c2c2c2',
        fontSize: 12,
        paddingLeft: 10,
        paddingRight: 10,
        fontFamily: 'Montserrat-Regular'
    },
    UpperBlock: {
        height: hp( '7%' ),
        backgroundColor: '#9066e6',
        alignItems: 'center',
        flexDirection: 'row'
    },
    listIcon: {
        height: 20,
        width: 20,
        marginLeft: 10,
    },
    lineWrapper: {
        backgroundColor: '#c2c2c2',
        height: hp( .1 ),
        marginTop: hp( 2 )
    },
    headingText: { width: wp( '70%' ), marginLeft: 20, fontSize: 14, paddingTop: 5, color: '#fff', fontFamily: 'Montserrat-SemiBold' },
    modalWrapper: { flex: 1, backgroundColor: '#00000040', alignItems: 'center', justifyContent: 'center' },
    modalCont: { width: wp( 90 ), backgroundColor: '#fff', justifyContent: 'center', borderRadius: 6, paddingVertical: hp( 1 ) },
    panel: {
        padding: 20,
        backgroundColor: '#FFFFFF',
        paddingTop: 20,
        // borderTopLeftRadius: 20,
        // borderTopRightRadius: 20,
        // shadowColor: '#000000',
        // shadowOffset: {width: 0, height: 0},
        // shadowRadius: 5,
        // shadowOpacity: 0.4,
    },
    header: {
        backgroundColor: '#FFFFFF',
        shadowColor: '#333333',
        shadowOffset: { width: -1, height: -3 },
        shadowRadius: 2,
        shadowOpacity: 0.4,
        // elevation: 5,
        paddingTop: 20,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
    },
    panelHeader: {
        alignItems: 'center',
    },
    panelHandle: {
        width: 40,
        height: 8,
        borderRadius: 4,
        backgroundColor: '#00000040',
        marginBottom: 10,
    },
    panelTitle: {
        fontSize: 27,
        height: 35,
    },
    panelSubtitle: {
        fontSize: 14,
        color: 'gray',
        height: 30,
        marginBottom: 10,
    },
    panelButton: {
        padding: 13,
        borderRadius: 10,
        backgroundColor: '#FF6347',
        alignItems: 'center',
        marginVertical: 7,
    },
    panelButtonTitle: {
        fontSize: 17,
        fontWeight: 'bold',
        color: 'white',
    },
} )


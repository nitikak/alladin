import React, { useEffect, useState } from 'react';
import {
    Text, TextInput, View, StyleSheet, Image, Pressable, KeyboardAvoidingView, SafeAreaView,
    useWindowDimensions, TouchableOpacity, ActivityIndicator,ScrollView
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import OTP from '../components/OtpInput';
import Login from './Login';
import Register from './Register'
import Colors from '../Layout/Colors'
import Device from './Device';
import { verifyOtp } from '../Redux/Action/SendOtp'
import { useDispatch, useSelector } from 'react-redux';
import { useIsFocused, useNavigation } from '@react-navigation/native';



const CardDetails = ({ route }) => {

    const navigation = useNavigation();

    const [loading, setLoading] = useState(false);
    const [visa, setVisa] = useState('')
    const [expiryDate, setExpiryDate]=useState('')
    const [cvc, setCvc]=useState('')



    return (

        <View style={styles.container}>


            {(loading) &&
                <View style={{ flex: 1, justifyContent: 'center', position: 'absolute', top: '50%', left: '40%' }}>

                    <ActivityIndicator
                        size="large"
                        style={{
                            backgroundColor: "rgba(20,116,240,.8)",
                            height: 80,
                            width: 80,
                            zIndex: 999,
                            borderRadius: 15
                        }}
                        size="small"
                        color="#ffffff"
                    />
                </View>}
            <View style={styles.UpperBlock}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <Image resizeMode='contain' style={styles.listIcon} source={require('../assets/left-arrow.png')} />

                </TouchableOpacity>
                <Text style={{width:wp('70%'),marginLeft:wp(3),fontSize:14,color:'#fff',  fontFamily:'Montserrat-SemiBold'}}>{'All Details'}</Text>

            </View>
            <ScrollView style={{flex: 0.8}}>
                <View >
            <View style={{marginTop:hp(6), marginHorizontal:wp(5)}}>
                <Text style={styles.payDetailHeading}>Pay & Details</Text>
          
                <View style={{marginTop:hp(3)}}>
                <Text style={styles.textBoldHeading}>Time</Text>
                <Text style={styles.timeSubHeading}>12/08/2016- 23:10</Text>
                </View>
                <View style={{marginTop:hp(3), flexDirection:'row'}}>
                <Text style={styles.timeSubHeading}>No. of Bedrooms are</Text>
                <Text  style={styles.textBoldHeading}>  {'5'}</Text>
                </View>
                <View style={{marginTop:hp(3)}}>
                <Text style={styles.timeSubHeading}>Extras included</Text>
                <Text style={styles.timeSubHeading}>Lounge Room, kitchen</Text>
                </View>
                <View style={{marginTop:hp(3)}}>
                <Text  style={styles.textBoldHeading}>Pay Via Credit Card</Text>
                </View>
                <View style={{marginTop:hp(3)}}>
                <Text style={[styles.timeSubHeading,{fontSize:12}]}>Credit card number</Text>
                </View>
                <View style={styles.creditTextInputWrapper}>
             
                <TextInput
                placeholder='*** *** **** 8745'
                value={visa}
                keyboardType='numeric'
                maxLength={16}
                onChangeText={visa => setVisa(visa)}
                style={{fontSize:20, paddingLeft:wp(5),color: '#000', fontFamily:'Montserrat-SemiBold',width:wp(70),}}
                placeholderTextColor={'#000'}
                />
                <Image source={require("../assets/visa.png")} style={{height:hp(8),width:hp(10)}}/>
                </View>
                <View style={{flexDirection:'row', marginTop:hp(3)}}>
                    <View style={{ width:wp(60),}}>
                    <Text style={{fontSize:12,color:'#000',  fontFamily:'Montserrat-SemiBold'}}>Expiry date</Text>
                    <View style={{borderWidth:1, borderColor:'#000',width:wp(55),borderRadius:5,marginTop:hp(2),borderColor: '#c2c2c2', backgroundColor:'#fff',flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                    <TextInput
                    placeholder='22/12'
                    value={expiryDate}
                    keyboardType='numeric'
                    maxLength={5}
                    onChangeText={expiryDate => setExpiryDate(expiryDate)}
                    style={{fontSize:20, color: '#000', fontFamily:'Montserrat-SemiBold',width:wp(40)}}
                    placeholderTextColor={'#000'}
                   />
                   <TouchableOpacity>
                       <Image resizeMode='contain' source={require('../assets/tick.png')} style={{height:hp(4),width:wp(10)}}/>
                   </TouchableOpacity>
                   </View>
                    </View>
                    <View style={{ width:wp(30),}}>
                        <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                        <Text style={{fontSize:12,color:'#000',  fontFamily:'Montserrat-SemiBold'}}>CVC</Text>
                            <Image source={require('../assets/question.png')} resizeMode='contain' style={{height:hp(3),width:wp(10)}}/>
                        </View>
                    <View style={{borderWidth:1, borderColor:'#000',width:wp(30),borderRadius:5,marginTop:hp(2),borderColor: '#c2c2c2', backgroundColor:'#fff',}}>
                    <TextInput
                    placeholder='134'
                    value={cvc}
                    keyboardType='numeric'
                    maxLength={3}
                    onChangeText={cvc => setCvc(cvc)}
                    style={{fontSize:20, color: '#000', fontFamily:'Montserrat-SemiBold',width:wp(30)}}
                    placeholderTextColor={'#000'}
                   /></View>
                    </View>
                </View>
          
            </View>
            </View>
          
            </ScrollView>
            <View style={{flex: 0.2, justifyContent:'flex-end'}}>
                <TouchableOpacity style={{backgroundColor:'#9066e6',paddingVertical:hp(2),alignItems:'center'}}>
                <Text style={[styles.textBoldHeading,{color: '#fff'}]}>Pay and Confirm</Text>
            
                </TouchableOpacity>
            </View>


        </View>

    )
}

export default CardDetails;


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#dfdfdf'

    },
    UpperBlock: {
        backgroundColor: '#9066e6',
        alignItems: 'center',
        flexDirection: 'row',
        paddingVertical: hp(.8)

    },
    listIcon: {
        height: hp(4),
        width: wp(5),
        marginLeft: 10,
    },
    payDetailHeading:{fontSize:20,color:'#9066e6',  fontFamily:'Montserrat-SemiBold'},
    textBoldHeading:{fontSize:14,color:'#000',  fontFamily:'Montserrat-SemiBold'},
    timeSubHeading:{fontSize:14,color:'#000',  fontFamily:'Montserrat-Regular'},
    creditTextInputWrapper:{flexDirection:'row',justifyContent:'space-between' ,borderWidth:1, borderColor: '#c2c2c2', backgroundColor:'#fff',borderRadius:5,alignItems:'center',marginTop:hp(2)}



})


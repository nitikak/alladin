import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, ActivityIndicator, ScrollView, } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { useIsFocused, useNavigation } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import { GetBookingDetail, CancelServices } from '../Redux/Action/bookingAction';
import { SafeAreaView } from 'react-native-safe-area-context';


export default function BookingProductList({ props, route }) {
    const navigation = useNavigation();

    const dispatch = useDispatch();

    const bookingSHowID = route.params.bookingId;

    console.log('booking_id', bookingSHowID)
    useEffect(() => {
        dispatch(GetBookingDetail({ booking_id: bookingSHowID }))
    }, [])
    const { loading } = useSelector(state => state.UserReducers);

    const detailBooking = useSelector((state) => state.bookingReducer.getBookingDetailData)


    return (
        <View style={styles.container}>
            <SafeAreaView>
                {(loading) &&
                    <View style={{ flex: 1, justifyContent: 'center', position: 'absolute', top: '40%', left: '40%' }}>

                        <ActivityIndicator


                            size="large"
                            style={{
                                //  backgroundColor: "rgba(144,102,230,.8)",
                                height: 80,
                                width: 80,
                                zIndex: 999,

                                borderRadius: 15

                            }}

                            color="rgba(144,102,230,.8)"
                        />
                    </View>}
                <View style={styles.headerWrapper}>
                    <View style={styles.headerRow}>
                        <TouchableOpacity onPress={() => navigation.goBack()}>
                            <Image resizeMode='contain' source={require('../assets/left-arrow.png')} style={styles.arrowImage} />
                        </TouchableOpacity>
                        <Text style={styles.headerHeading}>BookingList</Text>
                    </View>
                </View>
                {
                    detailBooking.BookingDetail && detailBooking.ServiceDetail &&

                    <ScrollView>
                        <View style={styles.centerImageWrapper}>
                            <Image source={{ uri: detailBooking.ServiceDetail.service_detail_url }} resizeMode='contain' style={styles.centerImage} />
                        </View>

                        <View style={styles.emailWrapper}>
                            <View style={styles.emailWidthWrapper}>
                                <Text style={styles.emailHeading}>Email</Text>
                                <View style={styles.emailBottomWRapper}>
                                    <Image source={require('../assets/mailbooking.png')} resizeMode='contain' style={styles.emailBottomLeftImage} />
                                    <View>
                                        <Text style={styles.emailBottomLeftText}>{detailBooking.servie_provider.company_email}</Text>

                                    </View>
                                </View>
                            </View>
                            <View style={styles.mobileWrapper}>
                                <Text style={styles.mobileHeading}>Mobile</Text>
                                <View style={styles.mobileBottomWrapper}>
                                    <Image source={require('../assets/mobilebooking.png')} resizeMode='contain' style={styles.mobileBottomLeftImage} />
                                    <Text style={styles.mobileBottomText}>{detailBooking.servie_provider.company_phone}</Text>
                                </View>
                            </View>
                        </View>
                        <View style={styles.lineWrapper} />
                        <View style={styles.bookingDateWrapper}>
                            <View style={styles.bookingWidthWrapper}>
                                <Text style={styles.bookingHeading}>Booking Date</Text>
                                <View style={styles.bookingBottomWrapper}>
                                    <Image source={require('../assets/DateBooking.png')} resizeMode='contain' style={styles.emailBottomLeftImage} />
                                    <Text style={styles.bookingBottomDate}>{detailBooking.BookingDetail.date}</Text>
                                </View>
                            </View>

                            <View style={styles.bookingBottomRightWRapper}>
                                <Text style={styles.bookingDAteHeading}>Booking Status</Text>
                                <View style={styles.bookingStatusWrapper}>
                                    <Text style={{ fontFamily: 'Montserrat-Medium', fontSize: 12, color: detailBooking.BookingDetail.status === 'canceled' ? 'red' : '#f2ac00' }}>{detailBooking.BookingDetail.status}</Text>
                                </View>
                            </View>
                            {
                                detailBooking.BookingDetail.status === 'pending' ?
                                    <View style={{ width: wp(30), }}>
                                        <TouchableOpacity onPress={() => dispatch(CancelServices(route.params.bookingId, navigation))} style={{ backgroundColor: 'red', width: wp(20), paddingVertical: hp(1), marginLeft: wp(4), justifyContent: 'center', alignItems: 'center', marginTop: hp(1), borderRadius: 6 }}>
                                            <Text style={{ color: '#fff', fontSize: 12, fontFamily: 'Montserrat-Medium' }}>Cancel</Text>
                                        </TouchableOpacity>

                                    </View> : null
                            }

                        </View>
                        <View style={styles.bookingLine} />
                        <View style={styles.itemElevationWrapper}>
                            <View style={styles.itemHeadingWrapper}>
                                <View style={styles.itemLeftHeading}>
                                    <Text style={styles.itemText}>Item</Text>
                                </View>
                                <View style={{ width: wp(25), }}>
                                    <Text style={styles.itemText}>Unit Price</Text>
                                </View>
                                <View style={{ width: wp(15), }}>
                                    <Text style={styles.itemText}>Qty</Text>
                                </View>
                                <View style={{ width: wp(20), }}>
                                    <Text style={styles.itemText}>Amount</Text>
                                </View>
                            </View>
                            <View style={styles.productWrapper}>
                                <View style={styles.serviceWrapper}>
                                    <View style={{ width: wp(30), }}>
                                        <Text style={styles.serviceName}>{detailBooking.ServiceDetail.name}</Text>
                                    </View>
                                    <View style={{ width: wp(20), }}>
                                        <Text style={styles.serviceName}>{detailBooking.ServiceDetail.formated_price}</Text>
                                    </View>
                                    <View style={styles.quantityWrapper}>
                                        {/* <Text style={styles.serviceName}>x2</Text> */}
                                    </View>
                                    <View style={styles.quantityWrapper}>
                                        <Text style={styles.serviceName}>{detailBooking.ServiceDetail.formated_price}</Text>
                                    </View>
                                </View>
                                <View style={styles.paymentLine} />
                                <View style={styles.paymentWrapper}>
                                    <View style={{ width: wp(30), }}>
                                        <Text style={styles.serviceName}>Payment Method</Text>
                                    </View>
                                    <View style={{ width: wp(20), }}>
                                        <Text style={styles.serviceName}>{detailBooking.BookingDetail.payment_gateway}</Text>
                                    </View>
                                    <View style={styles.quantityWrapper}>
                                        <Text style={styles.serviceName}>Service Total</Text>
                                    </View>

                                    <View style={styles.quantityWrapper}>
                                        <Text style={styles.amountHeading}>{detailBooking.BookingDetail.amount_to_pay}</Text>
                                    </View>
                                </View>
                                <View style={styles.paymentLine} />
                                <View style={styles.paymentWrapper}>
                                    <View style={{ width: wp(30), }}>
                                        <Text style={styles.serviceName}>Payment Status</Text>
                                    </View>
                                    <View style={{ width: wp(20), }}>
                                        <Text style={styles.serviceName}>{detailBooking.BookingDetail.status}</Text>
                                    </View>
                                    {/* <View style={styles.quantityWrapper}>
                                <Text style={styles.serviceName}>Product Total</Text>
                            </View> */}
                                    <View style={styles.quantityWrapper}>
                                        <Text style={styles.amountHeading}>{detailBooking.BookingDetail.converted_price}</Text>
                                    </View>
                                </View>
                                <View style={styles.paymentLine} />
                                <View style={styles.paymentWrapper}>
                                    {/* <View style={{ width: wp(30), }}>
                                <Text style={styles.serviceName}>Payment Method</Text>
                            </View> */}
                                    {/* <View style={{ width: wp(20), }}>
                                <Text style={{color:'#000', fontFamily:'Montserrat-Medium', fontSize:14}}>pending</Text>
                            </View> */}
                                    <View style={[styles.quantityWrapper, { width: wp(30), justifyContent: 'flex-start', alignItems: 'flex-start' }]}>
                                        <Text style={styles.serviceName}>Discount(On Service)</Text>
                                    </View>
                                    <View style={styles.quantityWrapper}>
                                        <Text style={styles.amountHeading}>{detailBooking.BookingDetail.formated_discounted_price}</Text>
                                    </View>
                                </View>
                                <View style={styles.paymentLine} />
                                <View style={styles.paymentWrapper}>
                                    <View style={{ width: wp(30), }}>
                                    </View>
                                    <View style={{ width: wp(20), }}>
                                    </View>
                                    <View style={styles.quantityWrapper}>
                                        <Text style={styles.totalText}>Total</Text>
                                    </View>
                                    <View style={styles.quantityWrapper}>
                                        <Text style={styles.totalText}>{detailBooking.BookingDetail.formated_price}</Text>
                                    </View>
                                </View>
                            </View>
                        </View>

                        <View style={styles.addressWrapper}>
                            <Text style={styles.addressHeading}>Custom Message</Text>
                            <Text style={styles.addressText}>{detailBooking.servie_provider.formatted_address}</Text>
                        </View>
                    </ScrollView>

                }

            </SafeAreaView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: '#f7f5f1', },
    headerWrapper: { backgroundColor: '#9066e6', paddingVertical: hp(2), flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' },
    headerRow: { flexDirection: 'row', alignItems: 'center' },
    arrowImage: { height: hp(3), width: wp(8), marginRight: wp(6), marginLeft: wp(2) },
    headerHeading: { fontSize: 14, color: '#fff', fontFamily: "Montserrat-Bold" },
    centerImageWrapper: { height: hp(15), marginTop: hp('5'), justifyContent: 'center', alignSelf: 'center', width: wp(28), borderRadius: 50, borderWidth: 3, borderColor: '#9066e6' },
    centerImage: { height: wp(15), width: wp(26), borderRadius: 50 },
    emailWrapper: { marginHorizontal: wp(5), paddingTop: hp(2), flexDirection: 'row' },
    emailWidthWrapper: { width: wp(50), borderRightWidth: 1, borderColor: '#c2c2c2' },
    emailHeading: { fontFamily: 'Montserrat-Medium', fontSize: 14, color: '#000' },
    emailBottomWRapper: { flexDirection: "row", alignItems: 'center', width: wp(50) },
    emailBottomLeftImage: { height: hp(4), width: wp(8) },
    emailBottomLeftText: { fontFamily: 'Montserrat-Medium', fontSize: 12, color: '#000', textAlign: 'left' },
    mobileWrapper: { width: wp(40), paddingLeft: wp(2) },
    mobileHeading: { fontFamily: 'Montserrat-Medium', fontSize: 14, color: '#000' },
    mobileBottomWrapper: { flexDirection: "row", alignItems: 'center' },
    mobileBottomLeftImage: { height: hp(4), width: wp(8) },
    mobileBottomText: { fontFamily: 'Montserrat-Medium', fontSize: 12, color: '#000' },
    lineWrapper: { marginHorizontal: wp(5), height: hp(.2), backgroundColor: '#c2c2c2', marginTop: hp(1.5) },
    bookingDateWrapper: { marginHorizontal: wp(5), paddingVertical: hp(1), flexDirection: 'row', marginTop: hp(2) },
    bookingWidthWrapper:
    {
        width: wp(30),
        borderRightWidth: 1, borderColor: '#c2c2c2'
    },
    bookingHeading: { fontFamily: 'Montserrat-Bold', fontSize: 13, color: '#000' },
    bookingBottomWrapper: { flexDirection: "row", alignItems: 'center', },
    bookingBottomDate: { fontFamily: 'Montserrat-Medium', fontSize: 12, color: '#007bff' },
    bookingBottomRightWRapper: { width: wp(30), borderRightWidth: 1, borderColor: '#c2c2c2', },
    bookingDAteHeading: { fontFamily: 'Montserrat-Bold', fontSize: 13, color: '#000', textAlign: 'center' },
    bookingStatusWrapper: { flexDirection: "row", alignItems: 'center', paddingLeft: wp(5), paddingTop: hp(1) },
    bookingLine: { marginHorizontal: wp(5), height: hp(.2), backgroundColor: '#c2c2c2', marginTop: hp(1) },
    itemElevationWrapper: { marginHorizontal: wp(5), marginTop: hp(2), paddingBottom: hp(2), elevation: 2 },
    itemHeadingWrapper: { backgroundColor: '#9066e6', paddingVertical: hp(1), flexDirection: 'row' },
    itemLeftHeading: { width: wp(30), paddingHorizontal: wp(1) },
    itemText: { color: '#fff', fontFamily: 'Montserrat-Bold', fontSize: 14 },
    productWrapper: { backgroundColor: '#fff', paddingVertical: hp(1), },
    serviceWrapper: { backgroundColor: '#fff', paddingVertical: hp(1), flexDirection: 'row', paddingHorizontal: wp(1), alignItems: 'center' },
    serviceName: { color: '#000', fontFamily: 'Montserrat-Medium', fontSize: 14 },
    quantityWrapper: { width: wp(20), justifyContent: 'center', alignItems: 'center' },
    paymentLine: { height: hp(.2), backgroundColor: '#c2c2c2', },
    paymentWrapper: { backgroundColor: '#fff', paddingVertical: hp(1), flexDirection: 'row', paddingHorizontal: wp(1), alignItems: 'center' },
    amountHeading: { color: '#000', fontFamily: 'Montserrat-Medium', fontSize: 14, },
    totalText: { color: '#000', fontFamily: 'Montserrat-Bold', fontSize: 14 },
    addressWrapper: { marginHorizontal: wp(5), marginBottom: hp(10) },
    addressHeading: { fontFamily: 'Montserrat-Bold', fontSize: 14, color: '#9066e6' },
    addressText: { fontFamily: 'Montserrat-Medium', fontSize: 14, color: '#000', paddingTop: hp(1) },



});

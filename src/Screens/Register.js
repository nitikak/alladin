import React, { useEffect, useState } from 'react';
import {
  Text, TextInput, View, StyleSheet, Image, Pressable, SafeAreaView, KeyboardAvoidingView, ImageBackground,
  ScrollView, useWindowDimensions, TouchableOpacity, Alert, ActivityIndicator
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { sentOtp } from '../Redux/Action/SendOtp'
import { connect } from 'react-redux';
import { useDispatch, useSelector } from 'react-redux';
import OTP from '../components/OtpInput';
import { getStateFromPath } from '@react-navigation/native';
import { useIsFocused, useNavigation } from '@react-navigation/native';
import RNPickerSelect from 'react-native-picker-select';

const Register = () => {

  const navigation = useNavigation();
  const [mobile, setMobile] = useState( '' );
  const [loading, setLoading] = useState( false );

  const dispatch = useDispatch();

  const GotoOtp = () => {


    console.log( '??????????????????/', mobile );
    if ( mobile == '' ) {
      Alert.alert( 'Mobile number is required' );
    }
    else if ( mobile.length < 10 || mobile.length > 10 ) {
      Alert.alert( 'Mobile number should be 10 digit' );
    }
    else {

      setLoading( true );
      setTimeout( () => {

        dispatch( sentOtp( { mobile: mobile, calling_code: state }, navigation ) )
        setLoading( false );
      }, 2000 );

      // dispatch(sentOtp({mobile: mobile,calling_code: '+91'},navigation))

    }

  }
  const [state, setState] = useState( '+964' )
  const state_list = [
    { label: '+964', value: '+964' },
    { label: '+91', value: '+91' },
    { label: '+971', value: '+971' },

  ];

  useEffect( () => {
    setMobile( '' )
  }, [] )

  return (
    <ScrollView style={styles.container}>

      {( loading ) &&
        <View style={{ flex: 1, justifyContent: 'center', position: 'absolute', top: '20%', left: '40%' }}>

          <ActivityIndicator


            style={{
              backgroundColor: "rgba(144,102,230,.8)",
              height: 80,
              width: 80,
              zIndex: 999,
              borderRadius: 15
            }}
            size="small"
            color="#ffffff"
          />
        </View>}
      <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{ paddingBottom: 130 }}>
        <View style={styles.content}>
          <View style={styles.form}>

            <Text
              style={{
                textAlign: 'center',
                color: '#000',
                marginBottom: 20,
                fontSize: 14,
                fontFamily: 'Montserrat-Regular',
                width: wp( '90%' ),
                textAlign: 'center',
                lineHeight: 22

              }}>
              Enter your mobile number to get{'\n'} OTP for verification.
            </Text>




            <View style={styles.searchSection}>

              {/* <View style={styles.searchIcon} >
                <Text style={{ color: '#000', fontSize: 12, fontFamily: 'Montserrat-Regular', }}>+964</Text>
              </View> */}
              <TouchableOpacity style={{
                // borderTopLeftRadius: 30,
                // borderBottomLeftRadius: 30,
                // backgroundColor: '#dfdfdf',
                // backgroundColor: 'red',
                // borderWidth: 1,
                borderColor: '#7c8791',
                alignItems: 'center',
                justifyContent: 'center',
                width: wp( 20 ),
                borderRightWidth: 1
                // marginTop: hp( 4 ),
                // elevation: 3,
                // paddingVertical: hp( 1.7 ),
                // marginHorizontal: wp( 5 )
              }}>
                <RNPickerSelect
                  onValueChange={( value ) => setState( value )}
                  items={state_list}
                  value={state}
                  placeholder={{}}
                >
                  <View style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    paddingHorizontal: wp( 4 )
                  }}>
                    {state_list.map(
                      ( item ) =>
                        item.value === state && (
                          <Text
                            key={item.value}
                            style={{ fontSize: 12, color: '#000', fontFamily: 'Montserrat-Regular' }}>
                            {item.label}
                          </Text>
                        )
                    )}
                    <Image source={require( '../assets/Downarrow.png' )} resizeMode='contain' style={{ height: hp( 3 ), width: wp( 8 ) }} />

                  </View>
                </RNPickerSelect>

              </TouchableOpacity>
              <View style={{
                width: wp( 60 ),
                // backgroundColor: 'green',
                justifyContent: 'center', alignItems: 'center'
              }}>
                <TextInput
                  maxLength={10}
                  onChangeText={mobile => setMobile( mobile?.replace( /[^0-9]/g, '' ) )}
                  // onChangeText={(mobile) => setMobile(mobile)}
                  keyboardType='numeric'
                  style={{
                    fontFamily: 'Montserrat-Regular',
                    fontSize: 12,
                    color: 'black',
                    width: wp( 40 ),
                  }}
                  placeholder={'Enter Mobile Number'}
                  value={mobile}
                />
              </View>
            </View>

            <TouchableOpacity style={styles.button}
              onPress={() => GotoOtp()
                // navigation.navigate('OtpScreen')
                // onPress={() =>   
                //  dispatch(sentOtp({mobile: mobile,calling_code: '+91'}))
              }
            >
              <Text style={styles.buttonText}>Request OTP</Text>
            </TouchableOpacity>



          </View>

        </View>
      </ScrollView>

    </ScrollView>
  )
}

export default Register;

const styles = StyleSheet.create( {
  container: {
    flex: 1,
    // backgroundColor: Colors.background,
    paddingBottom: 120
  },
  borderStyleHighLighted: {
    borderColor: "#03DAC6",
  },

  content: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 16,
    paddingVertical: 80,

  },

  searchSection: {
    flexDirection: 'row',
    // width:wp('75%'),
    width: wp( 80 ),
    // margin:1,
    borderWidth: 1,
    borderColor: '#7c8791',
    borderRadius: 30,
    // height: hp( 8 ),
    //paddingTop:2,
    paddingLeft: wp( 3 ),
    alignSelf: 'center',
    // justifyContent: 'center'
  },

  OtpsearchSection: {

    flexDirection: 'row',
    width: wp( '75%' ),
    margin: 15,
    borderWidth: 1,
    borderColor: '#7c8791',
    borderRadius: 30,
    paddingTop: 3,
    paddingLeft: 10,
    alignSelf: 'center'

  },

  searchIcon: {
    //marginTop:10,
    //height:25,
    // padding:5,
    width: wp( 10 ),
    // textAlign:'center',
    justifyContent: 'center',
    borderRightWidth: 1,
    borderColor: '#7c8791',
    //backgroundColor:'red'
  },

  form: {
    width: '100%',
  },

  input: {

    // width:'80%',
    // backgroundColor:'red',
    fontFamily: 'Montserrat-Regular',
    fontSize: 12,
    color: 'black',
    textAlign: 'center',

  },

  inputpassword: {

    width: '80%',
    marginBottom: 2,
    fontFamily: 'Montserrat-Regular',
    fontSize: 12,
    color: 'black',
    textAlign: 'center'

  },

  submitBtn: {
    borderRadius: 40,
    marginTop: 40,
  },

  button: {
    marginTop: 30,
    marginHorizontal: 40,
    backgroundColor: '#9066e6',
    padding: 10,
    width: wp( '40%' ),
    borderRadius: 40,
    alignSelf: 'center'

  },

  buttonText: {
    color: 'white',
    textAlign: 'center',
    fontFamily: 'Montserrat-Regular',
    //  textTransform:'uppercase'
  },
} );

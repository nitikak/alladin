import React, { useEffect, useState } from 'react';
import {
  Text, TextInput, View, StyleSheet, Image, Pressable, KeyboardAvoidingView, SafeAreaView,
  FlatList, useWindowDimensions, TouchableOpacity, ActivityIndicator
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import OTP from '../components/OtpInput';
import Login from './Login';
import Register from './Register'
import Colors from '../Layout/Colors'
import Device from './Device';
import { verifyOtp } from '../Redux/Action/SendOtp'
import { useDispatch, useSelector } from 'react-redux';
import { useIsFocused, useNavigation } from '@react-navigation/native';
import { AllServices } from '../Redux/Action/ServiceAction';


const Company = ({ route }) => {

  const dispatch = useDispatch();
  const navigation = useNavigation();
  // const[loading,setLoading] = useState(false);
  const { companies, loading } = useSelector(state => state.Services);


  const Services = (category_id, company_id) => {

    console.log(category_id, company_id);
    dispatch(AllServices(category_id, company_id, '', navigation))

  }


  return (

    <View style={styles.container}>
      <SafeAreaView>
        {/* {(loading) &&
                    <View style={{ flex: 1, justifyContent: 'center', position: 'absolute', top: '30%', left: '40%' }}>

                        <ActivityIndicator

                            
                            size="large"
                            style={{
                              //  backgroundColor: "rgba(144,102,230,.8)",
                                height: 80,
                                width: 80,
                                zIndex: 999,
                                borderRadius: 15
                            }}
                            size="small"
                            color="rgba(144,102,230,.8)"
                        />
                    </View>} */}

        <View style={styles.UpperBlock}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Image style={styles.listIcon} source={require('../assets/left-arrow.png')} />

          </TouchableOpacity>

          {companies && companies.length > 0 ?
            <Text style={{ marginLeft: 20, fontSize: 14, paddingTop: 5, color: '#fff', fontFamily: 'Montserrat-SemiBold' }}>{companies[0].catname}</Text>


            :

            <View />
          }


        </View>
        {/* {(loading) &&
                    <View style={{ flex: 1, justifyContent: 'center', position: 'absolute', top: '50%', left: '40%' }}>

                        <ActivityIndicator

                            
                            size="large"
                            style={{
                                backgroundColor: "rgba(20,116,240,.8)",
                                height: 80,
                                width: 80,
                                zIndex: 999,
                                borderRadius: 15
                            }}
                            size="small"
                            color="#ffffff"
                        />
                    </View>}  */}

        {/* <Image  style={styles.logo} source={require('../assets/left-arrow.png')}/> */}



        <Text
          style={{ padding: 10, fontSize: 14, paddingTop: 5, color: '#000', fontFamily: 'Montserrat-SemiBold' }}
        >Select service provider || {companies && companies.length > 0 ? companies[0].catname : null}  </Text>
        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
          {companies && companies.length > 0 ?



            <FlatList

              columnWrapperStyle={{ justifyContent: 'space-between', }}
              data={companies}
              keyExtractor={(item, index) => index}
              horizontal={false}
              numColumns={2}
              renderItem={({ item, index }) => (

                <TouchableOpacity style={styles.catBox}

                  onPress={() => Services(item.category_id, item.company_id)}

                >
                  <Image style={styles.caticon} source={{ uri: item.company_logo }} />
                  <View style={{ alignItems: 'center' }}>
                    <Text style={styles.ServiceText}>{item.company_name} | {item.address} </Text>
                  </View>
                  {/* <Text style={styles.AddressText}>{item.address}</Text> */}
                </TouchableOpacity>

              )}

            />

            :

            <Text style={{ textAlign: 'center', marginTop: 30 }}>No data available</Text>

          }


        </View>
      </SafeAreaView>
    </View>
  )
}

export default Company;


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#dfdfdf'

  },
  catBox: {
    // backgroundColor:'yellow',
    // marginBottom: 20
    // alignItems:'center'
    width: wp(50),
    marginLeft: wp(2.5),
    alignItems: 'flex-start'

  },
  caticon: {
    // resizeMode:'center',
    // height: 50,
    borderRadius: 5,
    height: hp(20),
    width: wp(40),
    // padding: 60,
    // width: 90,
    // padding: 90,
    // alignSelf: 'center',
    // backgroundColor: '#fff',
  },

  ServiceText: {
    color: '#000',
    textAlign: 'center',
    fontSize: 12,
    //paddingLeft:10,
    paddingTop: 10,
    fontFamily: 'Montserrat-Regular'
  },

  AddressText: {
    color: '#000',
    textAlign: 'center',
    fontSize: 12,
    //  paddingLeft:10,
    fontFamily: 'Montserrat-Regular'
  },

  button: {
    marginTop: 30,
    marginHorizontal: 40,
    backgroundColor: '#9066e6',
    padding: 10,
    width: wp('40%'),
    borderRadius: 10,
    alignSelf: 'center'

  },
  buttonText: {
    color: 'white',
    textAlign: 'center',
    fontFamily: 'Montserrat-Regular',
    //  textTransform:'uppercase'
  },
  UpperBlock: {
    height: hp('7%'),
    backgroundColor: '#9066e6',
    //   justifyContent:'center',
    alignItems: 'center',
    flexDirection: 'row'

  },
  OTPContainer: {
    width: wp('90%'),
    marginTop: 20,
    alignSelf: 'center'
  },

  listIcon: {
    height: 20,
    width: 20,
    marginLeft: 10
  },
  HomeText: {
    color: '#9066e6',
    fontSize: 24,
    fontFamily: 'Montserrat-Bold'
    // alignSelf:'center'
  },
  content: {
    //  padding: 10,
    marginTop: 20,
  },
  tabViewContainer: {
    //  marginVertical: 50,
    height: Device.height * 0.20,
  },
  maxHeight: { height: '100%' },
  tabBar: {
    height: 50,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  tabItem: {
    width: '50%',
    height: 50,
    backgroundColor: Colors.white,
    justifyContent: 'center',
    alignItems: 'center',
  },
  tabTitle: {
    fontSize: 15,
    color: Colors.black,
  },


})


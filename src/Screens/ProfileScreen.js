import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, Platform, BackHandler, Modal, ScrollView, TextInput } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useIsFocused, useNavigation } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import { ProfileData, UpdateProfileData } from '../Redux/Action/userAction';
import ImagePicker from 'react-native-image-crop-picker';
import { RNS3 } from 'react-native-s3-upload';
import { launchCamera } from 'react-native-image-picker';

import ImgToBase64 from 'react-native-image-base64';

export default function ProfileScreen ( props ) {

  const navigation = useNavigation()
  const dispatch = useDispatch();

  const fetchProfile = useSelector( ( state ) => state.UserReducers.profile );
  const [error, seterror] = useState( "" );
  const [error1, seterror1] = useState( "" );
  const [error2, seterror2] = useState( "" );
  const [image, setImage] = useState( null );
  const [showimage, setShowImage] = useState( '' );

  // const [image, setImage] = useState('https://api.adorable.io/avatars/80/abott@adorable.png');
  const [modalServices, setModalServices] = useState( false );


  let [inbuiltstate, setInbuiltstate] = useState( {

    mobNo: '',
    mobNoError: '',
    mobNoStatus: false,

    name: '',
    nameError: '',
    nameStatus: false,

    password: '',
    passwordError: '',
    passwordStatus: false,

  } )
  const [signin, setSignIn] = useState( {

    mobileNumber: fetchProfile?.formatted_mobile ? fetchProfile?.formatted_mobile : '',
    password: '',
    name: fetchProfile?.name ? fetchProfile?.name : '',
    email: fetchProfile?.email ? fetchProfile?.email : '',

  } );


  const base64Imgae = `data:image/png;base64,${showimage}`
  const updateProfileFunction = () => {

    dispatch( UpdateProfileData( {
      name: signin.name, email: signin.email, id: fetchProfile.id,
      image: base64Imgae, password: signin.password, navigation
    } ) )

  }

  const takePhotoFromCamera = () => {
    ImagePicker.openCamera( {
      width: 300,
      height: 400,
      cropping: true,
    } ).then( image => {
      console.log( image );
      imageUpload( image.path )
    } );
  }

  const choosePhotoFromLibrary = () => {
    ImagePicker.openPicker( {
      width: 300,
      height: 400,
      cropping: true
    } ).then( image => {
      console.log( "selected Image", image )
      console.log( "selected Image", image.path )

      imageUpload( image.path )
    } );
  }

  const imageUpload = ( imagePath ) => {
    setModalServices( false )
    console.log( 'imageData==========>Before ', imagePath )

    // const imageData = new FormData();
    // imageData.append("image", {
    //   uri: imagePath.uri,
    //   name: "image.png",

    //   type: "image/jpeg"
    // })
    console.log( "form data", imagePath )
    setImage( imagePath )
    // setShowImage(imagePath)

  }


  ImgToBase64.getBase64String( image )
    .then( ( base64String ) => {
      setShowImage( base64String )
      console.log( showimage )
    } )
    .catch( err => doSomethingWith( err ) );



  // const takePhotoFromCamera = () => {
  //   ImagePicker.openCamera({
  //     compressImageMaxWidth: 300,
  //     compressImageMaxHeight: 300,
  //     cropping: true,
  //     compressImageQuality: 0.7
  //   }).then(image => {
  //     // console.log('image=============>',image);
  //     setImage(image.path);
  //     imageUpload(image.path)

  //     this.bs.current.snapTo(1);
  //   });
  // }
  // const choosePhotoFromLibrary = () => {
  //   ImagePicker.openPicker({
  //     width: 300,
  //     height: 300,
  //     cropping: true,
  //     compressImageQuality: 0.7
  //   }).then(image => {
  //     console.log('========================>111',image);

  //     console.log('========================>',image.path);
  //     setImage(image.path);
  //     imageUpload(image.path)

  //     // this.bs.current.snapTo(1);
  //   });
  // }

  // const imageUpload = (imagePath) => {
  //   const imageData = new FormData()
  //   imageData.append('image',{
  //     uri: imagePath,
  //     name: 'image.png',
  //     fileName: 'image',
  //     type: 'image/png'
  //   })
  //   console.log('form data', imageData)
  //   // dispatch(UpdateProfileData({
  //   //   name: signin.name, email: signin.email, id: fetchProfile.id,
  //   //   image: imageData, navigation
  //   // }))
  // }


  return (
    <View style={styles.container}>
      <SafeAreaView>
        <View style={styles.headerWrapper}>
          <View style={styles.headerRow}>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <Image resizeMode='contain' source={require( '../assets/left-arrow.png' )} style={styles.headerImage} />
            </TouchableOpacity>
            <Text style={styles.headerHeading}>Profile</Text>
          </View>
        </View>
        <ScrollView contentContainerStyle={{ paddingBottom: hp( 3 ) }}>
          <View style={styles.centerImageWrapper}>
            {
              console.log( 'fetchProfile', fetchProfile )
            }
            {
              fetchProfile.user_image_url === '' ?
                <Image source={require( '../assets/userProfile.png' )} resizeMode='contain' style={[styles.centerImage, { height: hp( 18 ), width: wp( 28 ) }]} />
                : <Image source={{ uri: fetchProfile.user_image_url }} resizeMode='contain' style={styles.centerImage} />

            }

            <TouchableOpacity style={styles.cameraButtonWrapper} onPress={() => setModalServices( true )}>
              <Image source={require( '../assets/camera.png' )} resizeMode='contain' style={styles.cameraImage} />
            </TouchableOpacity>
          </View>
          <View style={styles.inputHeadingWrapper}>
            <Image source={require( '../assets/userProfile.png' )} resizeMode='contain' style={styles.headingImage} />
            <Text style={styles.headingTextInput}> Name</Text>

          </View>
          <View style={styles.inputWrapper}>
            <TextInput
              placeholder={'Enter your name  '}
              placeholderTextColor={'#c2c2c2'}
              value={signin.name}
              onChangeText={( text ) => { setSignIn( { ...signin, name: text } ); seterror1( '' ) }}

              // value={inbuiltstate.name}
              // onChangeText={(text) => handlevalidate(text, 'name')}
              errortext={inbuiltstate.nameError}
              style={styles.input}
            />
          </View>
          {error1 !== "" ? <Text style={styles.errorHeading}>{error1}</Text> : null}

          <View style={styles.inputHeadingWrapper}>
            <Image source={require( '../assets/mail.png' )} resizeMode='contain' style={styles.headingImage} />
            <Text style={styles.headingTextInput}> E-mail</Text>

          </View>
          <View style={styles.inputWrapper}>
            <TextInput
              placeholder={'Enter your Email  '}
              placeholderTextColor={'#c2c2c2'}
              value={signin.email}
              onChangeText={( text ) => { setSignIn( { ...signin, email: text } ); seterror1( '' ) }}

              // value={inbuiltstate.name}
              // onChangeText={(text) => handlevalidate(text, 'name')}
              errortext={inbuiltstate.nameError}
              style={styles.input}
            />
          </View>
          {error1 !== "" ? <Text style={styles.errorHeading}>{error1}</Text> : null}


          <View style={styles.inputHeadingWrapper}>
            <Image source={require( '../assets/lock.png' )} resizeMode='contain' style={styles.headingImage} />
            <Text style={styles.headingTextInput}> Password</Text>

          </View>
          <View style={styles.inputWrapper}>
            <TextInput
              placeholder={'*********'}
              placeholderTextColor={'#c2c2c2'}
              secureTextEntry={true}
              value={signin.password}
              onChangeText={( text ) => { setSignIn( { ...signin, password: text } ); seterror2( '' ) }}

              // value={inbuiltstate.password}
              // onChangeText={(text) => handlevalidate(text, 'password')}
              errortext={inbuiltstate.passwordError}
              style={{ width: wp( 80 ), marginLeft: wp( 4 ), paddingVertical: Platform.OS === 'ios' ? hp( 2 ) : hp( 2 ) }}
            />
          </View>
          {error2 !== "" ? <Text style={styles.errorHeading}>{error2}</Text> : null}

          <View style={styles.inputHeadingWrapper}>
            <Image source={require( '../assets/mobile.png' )} resizeMode='contain' style={styles.mobileImage} />
            <Text style={styles.headingTextInput}> Mobile Number</Text>

          </View>
          <View style={styles.inputWrapper}>
            <TextInput
              placeholder={'Add Your number (Iraq only) '}
              placeholderTextColor={'#c2c2c2'}
              keyboardType='numeric'
              maxLength={15}
              value={signin.mobileNumber}
              onChangeText={( text ) => { setSignIn( { ...signin, mobileNumber: text } ); seterror( '' ) }}
              editable={false}
              // value={inbuiltstate.mobNo}
              // onChangeText={(text) => handlevalidate(text, 'mobNo')}
              errortext={inbuiltstate.mobNoError}
              style={{ width: wp( 80 ), marginLeft: wp( 3 ), color: '#000', paddingVertical: Platform.OS === 'ios' ? hp( 2 ) : hp( 2 ) }}
            />
          </View>
          {error !== "" ? <Text style={styles.errorHeading}>{error}</Text> : null}



          <TouchableOpacity onPress={() => updateProfileFunction()}
            style={styles.updateWrapper}>
            <Text style={styles.headerHeading}>Update</Text>
          </TouchableOpacity>


          <Modal animationType="slide"
            transparent={true}
            visible={modalServices}
            onRequestClose={() => {
              setModalServices( false );
            }}>
            <View style={styles.modalWrapper}>
              <View style={styles.modalCont}>
                <View style={styles.panel}>
                  <View style={{ alignItems: 'center' }}>
                    <Text style={styles.panelTitle}>Upload Photo</Text>
                    <Text style={styles.panelSubtitle}>Choose Your Profile Picture</Text>
                  </View>
                  <TouchableOpacity style={styles.panelButton} onPress={takePhotoFromCamera}>
                    <Text style={styles.panelButtonTitle}>Take Photo</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={styles.panelButton} onPress={choosePhotoFromLibrary}>
                    <Text style={styles.panelButtonTitle}>Choose From Library</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={styles.panelButton}
                    // onPress={() => this.bs.current.snapTo(1)}
                    onPress={() => setModalServices( false )}
                  >
                    <Text style={styles.panelButtonTitle}>Cancel</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>

          </Modal>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
}

const styles = StyleSheet.create( {
  container: { flex: 1, backgroundColor: '#f7f5f1', },
  headerWrapper: { backgroundColor: '#9066e6', paddingVertical: hp( 2 ), flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' },
  headerRow: { flexDirection: 'row', alignItems: 'center' },
  headerImage: { height: hp( 3 ), width: wp( 8 ), marginRight: wp( 6 ), marginLeft: wp( 2 ) },
  headerHeading: { fontSize: 14, color: '#fff', fontFamily: "Montserrat-Bold" },
  centerImageWrapper: { height: hp( 15 ), position: 'relative', marginTop: hp( '5' ), justifyContent: 'center', alignSelf: 'center', width: wp( 28 ), borderRadius: 50, borderWidth: 3, borderColor: '#9066e6' },
  centerImage: { height: hp( 10 ), width: wp( 27 ) },
  cameraButtonWrapper: { backgroundColor: '#9066e6', position: 'absolute', right: -7, top: 0, height: hp( 5 ), width: wp( 10 ), borderRadius: 50, borderWidth: 2, borderColor: '#fff', alignContent: 'center', justifyContent: 'center' },
  cameraImage: { height: hp( 4 ), width: wp( 8 ) },
  inputHeadingWrapper: { marginTop: hp( 3 ), marginHorizontal: wp( 5 ), flexDirection: 'row', alignItems: 'center' },
  headingImage: { height: hp( 3 ), width: wp( 8 ) },
  headingTextInput: { fontSize: 14, fontFamily: 'Montserrat-Bold', color: '#000' },
  inputWrapper: { marginHorizontal: wp( 6 ), flexDirection: 'row', alignItems: 'center', elevation: 2, width: wp( 90 ), marginTop: hp( 1.5 ), backgroundColor: '#fff' },
  input: { width: wp( 80 ), marginLeft: wp( 4 ), fontFamily: 'Montserrat-Medium', paddingVertical: Platform.OS === 'ios' ? hp( 2 ) : hp( 2 ) },
  errorHeading: { marginLeft: wp( 6 ), color: "red", textAlign: 'left', fontFamily: 'Montserrat-Medium' },
  mobileImage: { height: hp( 3.5 ), width: wp( 7.5 ) },
  updateWrapper: { marginHorizontal: wp( 5 ), marginTop: hp( 3 ), paddingVertical: hp( 2 ), backgroundColor: '#9066e6', justifyContent: 'center', alignItems: 'center', marginBottom: hp( 7 ) },
  modalWrapper: { flex: 1, backgroundColor: '#00000040', alignItems: 'center', justifyContent: 'center' },
  modalCont: { width: wp( 90 ), backgroundColor: '#fff', justifyContent: 'center', borderRadius: 6, paddingVertical: hp( 1 ) },
  panel: {
    padding: 20,
    backgroundColor: '#FFFFFF',
    paddingTop: 20,
    // borderTopLeftRadius: 20,
    // borderTopRightRadius: 20,
    // shadowColor: '#000000',
    // shadowOffset: {width: 0, height: 0},
    // shadowRadius: 5,
    // shadowOpacity: 0.4,
  },
  header: {
    backgroundColor: '#FFFFFF',
    shadowColor: '#333333',
    shadowOffset: { width: -1, height: -3 },
    shadowRadius: 2,
    shadowOpacity: 0.4,
    // elevation: 5,
    paddingTop: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  panelHeader: {
    alignItems: 'center',
  },
  panelHandle: {
    width: 40,
    height: 8,
    borderRadius: 4,
    backgroundColor: '#00000040',
    marginBottom: 10,
  },
  panelTitle: {
    fontSize: 27,
    height: 35,
  },
  panelSubtitle: {
    fontSize: 14,
    color: 'gray',
    height: 30,
    marginBottom: 10,
  },
  panelButton: {
    padding: 13,
    borderRadius: 10,
    backgroundColor: '#FF6347',
    alignItems: 'center',
    marginVertical: 7,
  },
  panelButtonTitle: {
    fontSize: 17,
    fontWeight: 'bold',
    color: 'white',
  },
} );

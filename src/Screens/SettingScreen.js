import React, { useEffect, useState } from 'react';
import {
    Text, TextInput, View, StyleSheet, Image, Pressable, KeyboardAvoidingView, SafeAreaView,
    useWindowDimensions, TouchableOpacity, ScrollView
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { createpassword } from '../Redux/Action/SendOtp'
import { useDispatch, useSelector } from 'react-redux';
import { useIsFocused, useNavigation } from '@react-navigation/native';
import AsyncStorage from "@react-native-community/async-storage";

import { ProfileData, RemoveToken } from '../Redux/Action/userAction';

const SettingScreen = ({ route }) => {

    const dispatch = useDispatch();
    const navigation = useNavigation();

    const Logout = () => {


        dispatch(RemoveToken('null'))

        AsyncStorage.removeItem('login');
        navigation.navigate('Auth');
    }

    const fetchProfile = useSelector((state) => state.UserReducers.profile);

    console.log('fetchProfile', fetchProfile)

    useEffect(() => {
        dispatch(ProfileData())
    }, [])
    return (

        <View style={styles.container}>
            <SafeAreaView>


                <View style={styles.HeaderContainer}>
                    {/* <Text> {route.params.OTP}</Text> */}

                    <Text style={styles.HeaderText}>Settings</Text>
                    <View style={styles.BellContainer}>
                        <Image style={styles.bellIcon} source={require('../assets/bell.png')} />
                    </View>

                </View>
                <View style={styles.profileContainer}>


                    {/* <View style={styles.centerImageWrapper}>
        <Image source={{uri: fetchProfile.user_image_url}} resizeMode='contain' style={styles.centerImage} />

          {/* <Image source={require('../assets/userProfile.png')} resizeMode='contain' style={styles.centerImage} /> */}
                    {/* </View> */}
                    <View style={styles.profileContainer1}>
                        <Image style={styles.searchIcon}
                            //  source={{uri: fetchProfile.image}}
                            source={require('../assets/78.png')}
                        />
                    </View>

                    <View style={styles.profileContainer2}>
                        <Text style={{ fontFamily: 'Montserrat-SemiBold', fontSize: 16 }}>{fetchProfile.name}</Text>
                        <Text style={{ color: '#c2c2c2', fontFamily: 'Montserrat-Regular', paddingTop: 5 }}>Completed Requests 0</Text>
                    </View>

                    <TouchableOpacity onPress={() => navigation.navigate('ProfileScreen')} style={styles.profileContainer3}>
                        <Image style={styles.editIcon} source={require('../assets/pencile2.png')} />
                    </TouchableOpacity>
                </View>
                <ScrollView>
                    <View style={styles.ListContainer}>
                        <View style={styles.Listheader}>
                            <Image style={styles.listIcon} source={require('../assets/ourprice.png')} />
                            <Text style={{ color: '#c2c2c2', fontFamily: 'Montserrat-Bold', paddingLeft: 10 }}>Our Prices</Text>
                        </View>

                        <View style={styles.Listheader}>
                            <Image style={styles.listIcon} source={require('../assets/play.png')} />
                            <Text style={{ color: '#c2c2c2', fontFamily: 'Montserrat-Bold', paddingLeft: 10 }}>Change language</Text>
                        </View>

                        <View style={styles.Listheader}>
                            <Image style={styles.listIcon} source={require('../assets/location.png')} />
                            <Text style={{ color: '#c2c2c2', fontFamily: 'Montserrat-Bold', paddingLeft: 10 }}>Add address</Text>
                        </View>

                        <View style={styles.Listheader}>
                            <Image style={styles.listIcon} source={require('../assets/myshopping.png')} />
                            <Text style={{ color: '#c2c2c2', fontFamily: 'Montserrat-Bold', paddingLeft: 10 }}>My Shopping Order</Text>
                        </View>

                        <View style={styles.Listheader}>
                            <Image style={styles.listIcon} source={require('../assets/mail.png')} />
                            <Text style={{ color: '#c2c2c2', fontFamily: 'Montserrat-Bold', paddingLeft: 10 }}>Email us</Text>
                        </View>


                        <View style={styles.Listheader}>
                            <Image style={styles.listIcon} source={require('../assets/about.png')} />
                            <Text style={{ color: '#c2c2c2', fontFamily: 'Montserrat-Bold', paddingLeft: 10 }}>About Aladdin services</Text>
                        </View>

                        <View style={styles.Listheader}>
                            <Image style={styles.listIcon} source={require('../assets/rate.png')} />
                            <Text style={{ color: '#c2c2c2', fontFamily: 'Montserrat-Bold', paddingLeft: 10 }}>Rate Aladdin</Text>
                        </View>
                        <TouchableOpacity style={styles.Listheader} onPress={() => navigation.navigate('DashboardHome')} >
                            <Image style={styles.listIcon} source={require('../assets/rate.png')} />
                            <Text style={{ color: '#c2c2c2', fontFamily: 'Montserrat-Bold', paddingLeft: 10 }}>Dashboard</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.Listheader} onPress={() => navigation.navigate('BookingList')} >
                            <Image style={styles.listIcon} source={require('../assets/rate.png')} />
                            <Text style={{ color: '#c2c2c2', fontFamily: 'Montserrat-Bold', paddingLeft: 10 }}> Services Booking List</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.Listheader} onPress={() => navigation.navigate('AddCart')} >
                            <Image style={styles.listIcon} source={require('../assets/rate.png')} />
                            <Text style={{ color: '#c2c2c2', fontFamily: 'Montserrat-Bold', paddingLeft: 10 }}>Add to cart</Text>
                        </TouchableOpacity>


                        <View style={styles.Listheader}>
                            <Image style={styles.listIcon} source={require('../assets/play.png')} />
                            <TouchableOpacity onPress={() => Logout()}>
                                <Text style={{ color: '#c2c2c2', fontFamily: 'Montserrat-Bold', paddingLeft: 10 }}>Logout</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        </View>

    )
}

export default SettingScreen;


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'

    },
    centerImageWrapper: { height: hp(15), position: 'relative', marginTop: hp('5'), justifyContent: 'center', alignSelf: 'center', width: wp(28), borderRadius: 50, borderWidth: 3, borderColor: '#9066e6' },
    centerImage: { height: hp(10.5), width: wp(28), },

    HeaderContainer: {
        flexDirection: 'row',
        margin: 20,
        // backgroundColor:'yellow'
    },

    HeaderText: {
        fontFamily: 'Montserrat-Bold',
        fontSize: 20,
        width: wp('50%')

    },
    BellContainer: {
        width: wp('48%'),
        //  backgroundColor:'red',
        alignItems: 'flex-end',


    },

    profileContainer: {
        height: hp('10%'),
        backgroundColor: '#f7f5f1',
        width: wp('92%'),
        alignSelf: 'center',
        borderRadius: 10,
        flexDirection: 'row'
    },
    profileContainer1: {
        width: wp('20%'),
        //   backgroundColor:'red',
        justifyContent: 'center',
        alignItems: 'center'
    },
    profileContainer2: {
        width: wp('60%'),
        //  backgroundColor:'yellow',
        justifyContent: 'center',
        padding: 10
    },
    profileContainer3: {
        width: wp('10%'),

    },

    editIcon: {
        height: 40,
        width: 40,
        marginTop: 5
    },
    listIcon: {
        height: 20,
        width: 20,
    },
    bellIcon: {
        height: 30,
        width: 30,
        marginRight: 20
    },


    searchIcon: {

        height: 50,
        width: 50,

    },

    ListContainer: {

        margin: 20, marginTop: 40
    },
    Listheader: {
        flexDirection: 'row',
        marginBottom: 20
    }

})


import React, { useState } from 'react';
import {
  Text, TextInput, View, StyleSheet, Image, Pressable, KeyboardAvoidingView, SafeAreaView,
  useWindowDimensions, TouchableOpacity, Alert
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import OTP from '../components/OtpInput';
import Login from './Login';
import Register from './Register'
import Colors from '../Layout/Colors'
import Device from './Device';
import { createForgetpassword, createpassword } from '../Redux/Action/SendOtp'
import { useDispatch, useSelector } from 'react-redux';
import { useIsFocused, useNavigation } from '@react-navigation/native';



const CreatePassword = ({ route }) => {

  console.log('routee===========>', route)

  const dispatch = useDispatch();
  const navigation = useNavigation();
  const [password, setPassword] = useState('');
  const [showpassword, setShowpassword] = useState(true)
  const [confirmPassword, setConfirmPassword] = useState('');


  const CreatePassword = (password, confirmPassword) => {

    if(route.params.type === 'signUp'){
      if (password === confirmPassword) {
        const id = route.params.Id;
        const token = route.params.token;
  
        dispatch(createpassword({ password: password, device_type: 'android', device_code: "12345", mobile_otp_id: id, temp_token: token }, navigation))
  
      }
      else {
        Alert.alert('Please correct password enter')
      }
    
    }else{
      if(password === confirmPassword){
        const id = route.params.Id;

        dispatch(createForgetpassword({ password: password, id: id }, navigation))
    
    }else{
        Alert.alert('Please correct password enter')
    }
    }

  

  }


  return (
    <View style={styles.container}>
      <View style={styles.UpperBlock}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Image style={styles.listIcon} source={require('../assets/left-arrow.png')} />

        </TouchableOpacity>
      </View>

      <View style={styles.OTPContainer}>
        {/* <Text> {route.params.OTP}</Text> */}
        {/* <Text> {route.params.Id}</Text> */}
        <Text style={styles.HomeText}>Create {'\n'}Password</Text>
        <Text style={{ paddingTop: 10, paddingBottom: 10 }}>Your password must include 8 digits</Text>
      </View>
      <View style={styles.OTPContainer}>
        <Text style={{ paddingBottom: 10 }}>Password</Text>
        <View style={styles.searchSection}>


          <TextInput
            secureTextEntry={showpassword}
            value={password}

            onChangeText={password => setPassword(password)}
            style={styles.inputpassword}
            placeholder={'Password'}
          />
          <TouchableOpacity onPress={() => setShowpassword(!showpassword)} style={{ alignItems: 'center', justifyContent: 'center' }}>

            <Image style={styles.searchIcon} source={showpassword ? require('../assets/HidePassword.png') : require('../assets/show_Password_copy.png')} />

          </TouchableOpacity>
          {/* <Image style={styles.searchIcon} source={require('../assets/HidePassword.png')} /> */}

        </View>




      </View>
      <View style={styles.OTPContainer}>
        <Text style={{ paddingBottom: 10 }}>Confirm Password</Text>
        <View style={styles.searchSection}>


          <TextInput
            secureTextEntry={true}
            value={confirmPassword}
            onChangeText={confirmPassword => setConfirmPassword(confirmPassword)}
            style={styles.inputpassword}
            placeholder={'Confirm Password'}
          />


        </View>

        <Text style={{ paddingTop: 10 }}>Both Password must be same</Text>


      </View>
      <TouchableOpacity style={styles.button} onPress={() => CreatePassword(password, confirmPassword)}>
        <Text style={styles.buttonText}>Create Password</Text>

      </TouchableOpacity>


    </View>
  )
}

export default CreatePassword;


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#dfdfdf'

  },
  searchSection: {
    flexDirection: 'row',
    width: wp('90%'),
    margin: 1,
    //borderWidth:1,
    // borderColor: '#7c8791',
    borderRadius: 10,
    //paddingTop:2,
    paddingLeft: 10,
    alignSelf: 'center',
    backgroundColor: '#fff'
  },
  searchIcon: {

    // marginTop: 15,
    height: 20,
    padding: 5,
    width: 20,

    // justifyContent: 'center',
    // borderRightWidth: 1,
    // borderColor: '#7c8791',
    //backgroundColor:'red'
  },
  inputpassword: {
    width: '90%',
    marginBottom: 2,
    fontFamily: 'Montserrat-Regular',
    fontSize: 14,
    color: 'black',
    textAlign: 'left'
  },

  input: {

    width: '80%',
    // backgroundColor:'red',
    fontFamily: 'Montserrat-Regular',
    fontSize: 12,
    color: 'black',
    textAlign: 'center',

  },
  button: {
    marginTop: 30,
    marginHorizontal: 40,
    backgroundColor: '#9066e6',
    padding: 10,
    width: wp('80%'),
    borderRadius: 10,
    alignSelf: 'center'

  },

  buttonText: {
    color: 'white',
    textAlign: 'center',
    fontFamily: 'Montserrat-Regular',
    //  textTransform:'uppercase'
  },
  UpperBlock: {
    height: hp('7%'),
    backgroundColor: '#9066e6',
    justifyContent: 'center'
  },
  listIcon: {
    height: 20,
    width: 20,
    marginLeft: 10,
  },
  OTPContainer: {
    width: wp('90%'),
    marginTop: 20,
    alignSelf: 'center'
  },

  logo: {
    // resizeMode:'contain',
    alignSelf: 'center',
    height: hp('30%'),
    width: wp('100%'),

  },
  HomeText: {
    color: '#9066e6',
    fontSize: 24,
    fontFamily: 'Montserrat-Bold'
    // alignSelf:'center'
  },
  content: {
    //  padding: 10,
    marginTop: 20,
  },
  tabViewContainer: {
    //  marginVertical: 50,
    height: Device.height * 0.20,
  },
  maxHeight: { height: '100%' },
  tabBar: {
    height: 50,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  tabItem: {
    width: '50%',
    height: 50,
    backgroundColor: Colors.white,
    justifyContent: 'center',
    alignItems: 'center',
  },
  tabTitle: {
    fontSize: 15,
    color: Colors.black,
  },


})


import React, { useEffect, useState } from 'react';
import {
  Text, TextInput, View, StyleSheet, Image, Pressable, KeyboardAvoidingView, SafeAreaView,
  FlatList, useWindowDimensions, TouchableOpacity, ActivityIndicator, Platform
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import OTP from '../components/OtpInput';
import Login from './Login';
import Register from './Register'
import Colors from '../Layout/Colors'
import Device from './Device';
import { useDispatch, useSelector } from 'react-redux';
import { useIsFocused, useNavigation } from '@react-navigation/native';
import { AllServices, ServiceDetail } from '../Redux/Action/ServiceAction';

const ServicesScreen = ({ route }) => {

  const dispatch = useDispatch();
  const navigation = useNavigation();
  const [loading, setLoading] = useState(false);
  const { ALLSERVICES } = useSelector(state => state.Services);

  const [filterData, setFilterData] = useState([]);
  const [withoutfilter, setWithoutFilter] = useState('')
  const ServiceDetails = (id, slug, item) => {

    dispatch(ServiceDetail(id, slug));

    navigation.navigate('ServiceDetail', {
      itemId: id,
    });
  }

  const SearchFilterFunction = (text) => {
    const searchWord = text;
    const newFilter = ALLSERVICES.filter((value) => {
      const itemData = value.name ? value.name.toUpperCase() : ''.toUpperCase();
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });

    console.log('search word================>', searchWord, newFilter)
    setWithoutFilter(searchWord)
    if (searchWord === '') {
      setFilterData([]);
    } else {
      setFilterData(newFilter)
    }
  }
  return (

    <View style={styles.container}>
      <SafeAreaView>


        <View style={styles.UpperBlock}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Image style={styles.listIcon} source={require('../assets/left-arrow.png')} />

          </TouchableOpacity>

          {ALLSERVICES.length > 0 ?
            <Text numberOfLines={1} style={{ width: wp('70%'), marginLeft: 20, fontSize: 14, paddingTop: 5, color: '#fff', fontFamily: 'Montserrat-SemiBold' }}>{ALLSERVICES[0].category.name}</Text>


            :

            <View />
          }

          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Image style={styles.filterIcon} source={require('../assets/whitefilter.png')} />

          </TouchableOpacity>

        </View>

        {(loading) &&
          <View style={{ flex: 1, justifyContent: 'center', position: 'absolute', top: '50%', left: '40%' }}>

            <ActivityIndicator


              size="large"
              style={{
                backgroundColor: "rgba(20,116,240,.8)",
                height: 80,
                width: 80,
                zIndex: 999,
                borderRadius: 15
              }}
              size="small"
              color="#ffffff"
            />
          </View>}

        {/* <Image  style={styles.logo} source={require('../assets/left-arrow.png')}/> */}

        <View style={styles.searchSection}>
          <Image style={styles.searchIcon} source={require('../assets/search.png')} />

          <TextInput
            style={styles.input}
            underlineColorAndroid="rgba(0,0,0,0)"
            placeholder={'Search'}
            keyboardType="default"
            returnKeyType="done"
            autoCapitalize="none"
            value={filterData}
            onChangeText={(text) => SearchFilterFunction(text)}
          />
        </View>

        <View style={{ padding: 10, marginBottom: hp('20%') }}>

          {/* <Text>{ALL_SERVICES.length}kllk</Text> */}

          {/* {ALL_SERVICES.length > 0 ? */}

          {
            console.log('setjsjjjs', withoutfilter != filterData)
          }
          {
            filterData.length > 0 ?

              <FlatList

                data={filterData}
                keyExtractor={(item, index) => index}
                horizontal={false}
                renderItem={({ item, index }) => (
                  <TouchableOpacity style={{ paddingBottom: hp(2) }}
                    onPress={() => ServiceDetails(item.id, item.category.slug, item)}>
                    <Image style={styles.caticon} source={{ uri: item.service_image_url }} />
                    <Text style={styles.ServiceText}>{item.name}</Text>
                    <View style={{ flexDirection: 'row' }}>
                      <Text style={styles.PriceText}>{item.formated_price}</Text>
                      <Text style={styles.discountText}>{item.formated_discounted_price}</Text>
                    </View>
                  </TouchableOpacity>
                )}

              /> : withoutfilter != filterData ? <View style={{ justifyContent: 'center', alignItems: 'center' }}><Text style={{ fontSize: 16 }}> No data found</Text></View> :
                <FlatList

                  data={ALLSERVICES}
                  keyExtractor={(item, index) => index}
                  horizontal={false}
                  renderItem={({ item, index }) => (
                    <TouchableOpacity style={{ paddingBottom: hp(2) }}
                      onPress={() => ServiceDetails(item.id, item.category.slug, item)}>
                      <Image style={styles.caticon} source={{ uri: item.service_image_url }} />
                      <Text style={styles.ServiceText}>{item.name}</Text>
                      <View style={{ flexDirection: 'row' }}>
                        <Text style={styles.PriceText}>{item.formated_price}</Text>
                        <Text style={styles.discountText}>{item.formated_discounted_price}</Text>
                      </View>
                    </TouchableOpacity>
                  )}
                />
          }

          {/* :
                 
                 <Text style={{textAlign:'center',marginTop:30}}>No data available</Text>
                 
                 } */}


        </View>
      </SafeAreaView>
    </View>
  )
}

export default ServicesScreen;


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#dfdfdf'

  },

  caticon: {

    height: 50,
    borderRadius: 25,
    width: wp('90%'),
    padding: 90,
    alignSelf: 'center',
    backgroundColor: '#fff'
  },

  ServiceText: {
    color: '#000',
    textAlign: 'left',
    fontSize: 14,
    paddingLeft: 20,
    paddingTop: 15,
    fontFamily: 'Montserrat-SemiBold'
  },
  searchSection: {
    flexDirection: 'row',
    width: wp('90%'),
    backgroundColor: '#fff',
    //borderWidth:1,
    //  borderColor: '#7c8791',
    borderRadius: 5,
    // paddingTop:5,
    alignItems: 'center',
    // paddingBottom:5,
    paddingLeft: 10,
    alignSelf: 'center',
    marginTop: 10,
    marginBottom: 10
  },
  searchIcon: {

    // marginTop:10,
    height: 28,
    width: 28,
  },


  input: {
    width: '80%',
    fontFamily: 'Montserrat-Regular',
    fontSize: 13,
    color: 'black',
    paddingVertical: Platform.OS == 'ios' ? hp(1.5) : hp(0)


  },
  PriceText: {
    color: '#9066e6',
    textAlign: 'left',
    fontSize: 14,
    paddingLeft: 20,

    fontFamily: 'Montserrat-SemiBold'
  },
  discountText: {
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid',
    color: '#000',
    textAlign: 'left',
    fontSize: 14,
    paddingLeft: 20,

    fontFamily: 'Montserrat-Regular'
  },
  button: {
    marginTop: 30,
    marginHorizontal: 40,
    backgroundColor: '#9066e6',
    padding: 10,
    width: wp('40%'),
    borderRadius: 10,
    alignSelf: 'center'

  },
  buttonText: {
    color: 'white',
    textAlign: 'center',
    fontFamily: 'Montserrat-Regular',
    //  textTransform:'uppercase'
  },
  UpperBlock: {
    height: hp('7%'),
    //  width:wp('95%'),

    backgroundColor: '#9066e6',
    //   justifyContent:'center',
    alignItems: 'center',
    //  alignSelf:'center',
    flexDirection: 'row'

  },
  OTPContainer: {
    width: wp('90%'),
    marginTop: 20,
    alignSelf: 'center'
  },

  listIcon: {
    height: 20,
    width: 20,
    marginLeft: 10,
    // width:wp('10%')
  },
  filterIcon: {
    height: 20,
    width: 20,
    marginLeft: 15,
    //  alignSelf:'flex-end',
    width: wp('10%')

  },

  HomeText: {
    color: '#9066e6',
    fontSize: 24,
    fontFamily: 'Montserrat-Bold'
    // alignSelf:'center'
  },
  content: {
    //  padding: 10,
    marginTop: 20,
  },
  tabViewContainer: {
    //  marginVertical: 50,
    height: Device.height * 0.20,
  },
  maxHeight: { height: '100%' },
  tabBar: {
    height: 50,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  tabItem: {
    width: '50%',
    height: 50,
    backgroundColor: Colors.white,
    justifyContent: 'center',
    alignItems: 'center',
  },
  tabTitle: {
    fontSize: 15,
    color: Colors.black,
  },


})


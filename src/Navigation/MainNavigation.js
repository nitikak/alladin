import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Home from '../Screens/Home';
import Register from '../Screens/Register';
import Header from '../components/Header';
import SettingScreen from '../Screens/SettingScreen'
import OrdersScreen from '../Screens/OrdersScreen';
import ShopScreen from '../Screens/ShopScreen';
import MainHome from '../Screens/MainHome'
import OtpScreen from '../Screens/OtpScreen';
import ForgotPassword from '../Screens/ForgotPassword';
import ForgotPasswordOTP from '../Screens/ForgotPasswordOTP';
import AuthCheck from '../Screens/AuthCheck';
import CreatePassword from '../Screens/CreatePassword';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {
  Text, TextInput, View, StyleSheet, Image, Pressable,
  TouchableOpacity,
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { enableScreens } from 'react-native-screens';
import { Provider } from 'react-redux';
import store from '../Redux/Store';
import Company from '../Screens/Company';
import ServicesScreen from '../Screens/ServicesScreen'
import ServiceDetailScreen from '../Screens/ServiceDetailScreen';
import Services from '../components/Services';
import CardDetails from '../Screens/CardDetail';
import PropertyDetail from '../Screens/PropertyDetail';
import ForgetCreatePassword from '../Screens/ForgetCreatePassword';
import Category from '../components/Categories';
import ParticularCategories from '../components/ParticularCategories';
import CategoryDetailScreen from '../Screens/CategoryDetailScreen';
import Dashboard from '../Screens/Dashboard';
import BookingList from '../Screens/BookingList';
import BookingProductList from '../Screens/BokkingProductScreen';
import ProfileScreen from '../Screens/ProfileScreen';
import AddToCart from '../Screens/AddToCart';
import AddAddressCart from '../Screens/AddAddressCart';
import ProductListDetail from '../components/ProductListDetail';

enableScreens();


const SignStack = createStackNavigator();

function SignInScreen() {

  return (

    <SignStack.Navigator initialRouteName='AuthCheck'>

      <Stack.Screen
        name="Home"
        component={Home}
        options={{
          title: 'Login',
          headerShown: false,
          headerStyle: {
            backgroundColor: '#e85b3d',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      />

      <Stack.Screen
        name="AuthCheck"
        component={AuthCheck}
        options={{
          title: 'AuthCheck',
          headerShown: false,
          headerStyle: {
            backgroundColor: '#e85b3d',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      />

      <Stack.Screen
        name="OtpScreen"
        component={OtpScreen}
        options={{
          title: 'OtpScreen',
          headerShown: false,
          headerStyle: {
            backgroundColor: '#e85b3d',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      />


      <Stack.Screen
        name="CreatePassword"
        component={CreatePassword}
        options={{
          title: 'CreatePassword',
          headerShown: false,
          headerStyle: {
            backgroundColor: '#e85b3d',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      />
      <Stack.Screen
        name="ForgetCreatePassword"
        component={ForgetCreatePassword}
        options={{
          title: 'ForgetCreatePassword',
          headerShown: false,
          headerStyle: {
            backgroundColor: '#e85b3d',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      />
      <Stack.Screen
        name="ForgotPassword"
        component={ForgotPassword}
        options={{
          title: 'ForgotPassword',
          headerShown: false,
          headerStyle: {
            backgroundColor: '#e85b3d',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      />

      <Stack.Screen
        name="ForgotPasswordOTP"
        component={ForgotPasswordOTP}
        options={{
          title: 'ForgotPasswordOTP',
          headerShown: false,
          headerStyle: {
            backgroundColor: '#e85b3d',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      />
    </SignStack.Navigator>
  )
}

const HomeStack = createStackNavigator();

const SignInScreen1 = () => {

  return (
    // <NavigationContainer> 
    <HomeStack.Navigator initialRouteName='MainHome'
      screenOptions={{
        headerShown: false
      }}
    >

      <Stack.Screen
        name="MainHome"
        component={MainHome}
        options={{
          title: 'Bookmark',
          headerStyle: {
            backgroundColor: '#e85b3d',

          },

          headerShown: false,
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      />
      <Stack.Screen
        name="Category"
        component={Category}
        options={{
          title: 'Bookmark',
          headerStyle: {
            backgroundColor: '#e85b3d',

          },

          headerShown: false,
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      />
      <Stack.Screen
        name="ParticularCategories"
        component={ParticularCategories}
        options={{
          title: 'ParticularCategories',
          headerShown: false,
          headerStyle: {
            backgroundColor: '#e85b3d',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      />
      <Stack.Screen
        name="PropertyDetail"
        component={PropertyDetail}
        options={{
          title: 'PropertyDetail',
          headerShown: false,
          headerStyle: {
            backgroundColor: '#e85b3d',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      />
      <Stack.Screen
        name="AddAddressCart"
        component={AddAddressCart}
        options={{
          title: 'AddAddressCart',
          headerShown: false,
          headerStyle: {
            backgroundColor: '#e85b3d',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      />



      <Stack.Screen
        name="CardDetails"
        component={CardDetails}
        options={{
          title: 'CardDetails',
          headerShown: false,
          headerStyle: {
            backgroundColor: '#e85b3d',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      />
      <Stack.Screen
        name="CategoryDetailScreen"
        component={CategoryDetailScreen}
        options={{
          title: 'CategoryDetailScreen',
          headerShown: false,
          headerStyle: {
            backgroundColor: '#e85b3d',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      />
      <Stack.Screen
        name="ProductListDetail"
        component={ProductListDetail}
        options={{
          title: 'ProductListDetail',
          headerShown: false,
          headerStyle: {
            backgroundColor: '#e85b3d',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      />
    </HomeStack.Navigator>
  )
}

const SettingStack = createStackNavigator();

const SignInScreen5 = () => {

  return (
    // <NavigationContainer> 
    <SettingStack.Navigator initialRouteName='SettingScreen'
      screenOptions={{
        headerShown: false
      }}
    >

      <Stack.Screen
        name="SettingScreen"
        component={SettingScreen}
        options={{
          title: 'Bookmark',
          headerStyle: {
            backgroundColor: '#e85b3d',

          },

          headerShown: false,
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      />
      <Stack.Screen
        name="DashboardHome"
        component={Dashboard}
        options={{
          title: 'DashboardHome',
          headerShown: false,
          headerStyle: {
            backgroundColor: '#e85b3d',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      />

      <Stack.Screen
        name="BookingList"
        component={BookingList}
        options={{
          title: 'BookingList',
          headerShown: false,
          headerStyle: {
            backgroundColor: '#e85b3d',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      />
      <Stack.Screen
        name="BookingProductList"
        component={BookingProductList}
        options={{
          title: 'BookingProductList',
          headerShown: false,
          headerStyle: {
            backgroundColor: '#e85b3d',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      />
      <Stack.Screen
        name="ProductListDetail"
        component={ProductListDetail}
        options={{
          title: 'ProductListDetail',
          headerShown: false,
          headerStyle: {
            backgroundColor: '#e85b3d',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      />



    </SettingStack.Navigator>
  )
}


const CompanyStack = createStackNavigator();

const SignInScreen3 = () => {

  return (
    // <NavigationContainer>
    <CompanyStack.Navigator
      screenOptions={{
        headerShown: false
      }}
    >



      <Stack.Screen
        name="Company"
        component={Company}
        options={{
          title: 'Company',
          headerShown: false,
          headerStyle: {
            backgroundColor: '#e85b3d',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      />


      <Stack.Screen
        name="ServicesScreen"
        component={ServicesScreen}
        options={{
          title: 'ServicesScreen',
          headerShown: false,
          headerStyle: {
            backgroundColor: '#e85b3d',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      />

      <Stack.Screen
        name="ServiceDetail"
        component={ServiceDetailScreen}
        options={{
          title: 'ServiceDetail',
          headerShown: false,
          headerStyle: {
            backgroundColor: '#e85b3d',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      />

      <Stack.Screen
        name="ParticularCategories"
        component={ParticularCategories}
        options={{
          title: 'ParticularCategories',
          headerShown: false,
          headerStyle: {
            backgroundColor: '#e85b3d',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      />
      <Stack.Screen
        name="CategoryDetailScreen"
        component={CategoryDetailScreen}
        options={{
          title: 'CategoryDetailScreen',
          headerShown: false,
          headerStyle: {
            backgroundColor: '#e85b3d',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      />
      <Stack.Screen
        name="DashboardHome"
        component={Dashboard}
        options={{
          title: 'DashboardHome',
          headerShown: false,
          headerStyle: {
            backgroundColor: '#e85b3d',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      />

      <Stack.Screen name='AddCart' component={AddToCart} />








    </CompanyStack.Navigator>
  )
}




function MyTabBar({ state, descriptors, navigation }) {

  return (
    <View
      style={{

        flexDirection: 'row',
        // position:'absolute',
        bottom: 0,

        borderTopColor: '#E5E5E5',
        borderTopWidth: 1,
        width: wp('100%'),
        backgroundColor: '#9066e6',
        height: 50

      }}>
      {state.routes.map((route, index) => {
        const { options } = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
              ? options.title
              : route.name;


        const isFocused = state.index === index;
        let showlabel = "";
        let iconNm = "";
        if (label == "Dashboard") {
          //  showlabel = "Dashboard";
          iconNm = require('../assets/18.png');


          // {isFocused ?
          //   iconNm = require('../../Assets/bluedumble.png')
          //   :
          //   iconNm = require('../../Assets/dumble.png')
          // }

        }




        // if (label == "Bookmark") {
        //    // showlabel = "Settings";
        //     iconNm = require('../../Assets/homeb.png');


        //     {isFocused ?

        //       iconNm = require('../../Assets/homeb.png')

        //       :
        //       iconNm = require('../../Assets/homeicon.png')
        //     }
        // }

        if (label == "Home") {
          //  showlabel = "Home";
          iconNm = require('../assets/19.png');

          // {isFocused ?
          //   iconNm = require('../../Assets/Profile=Selected.png')
          //   :
          //   iconNm = require('../../Assets/login.png')
          // }
        }

        if (label == "bar") {
          //  showlabel = "Home";
          iconNm = require('../assets/bar.png');

          // {isFocused ?
          //   iconNm = require('../../Assets/Profile=Selected.png')
          //   :
          //   iconNm = require('../../Assets/login.png')
          // }
        }

        if (label == "Settings") {
          //  showlabel = "Home";
          iconNm = require('../assets/21.png');

          // {isFocused ?
          //   iconNm = require('../../Assets/Profile=Selected.png')
          //   :
          //   iconNm = require('../../Assets/login.png')
          // }
        }

        // if (label == "bell") {
        //     showlabel = "Notification";
        //     iconNm= label;
        // }

        // if (label == "envelope") {
        //     showlabel = "Contact us";
        //     iconNm = label
        // }


        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (

          <TouchableOpacity
            accessibilityRole="button"
            accessibilityStates={isFocused ? ['selected'] : []}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={{ flex: 1, justifyContent: 'center', alignItems: 'center', padding: 8 }}
            key={route.key}
          >
            {/* <Icon size={24} name={iconNm} color={isFocused ? '#FFFFFF' : '#d3d3d3'} />  */}
            <View style={{ flexDirection: 'row', }}>

              <Image source={iconNm} style={{ marginRight: 40, marginLeft: 40, resizeMode: 'contain', width: 30, height: 30 }} />


              {/* <Text style={{ alignSelf: 'center', color: isFocused ? '#000' : '#0008', fontSize: 13,fontWeight:'bold' }}>
                      {showlabel}
                  </Text> */}
            </View>
          </TouchableOpacity>

        );
      })}
    </View>
  );
}


const Tab = createBottomTabNavigator();

function TabNavigator() {
  return (
    <Tab.Navigator tabBar={props => <MyTabBar {...props} />}>
      <Tab.Screen name="Dashboard"
        options={{

          headerShown: false,


        }}
        component={SignInScreen1} />



      <Tab.Screen name="Home"
        options={{

          headerShown: false,


        }}

        component={ShopScreen} />


      <Tab.Screen name="bar"
        options={{

          headerShown: false,


        }}

        component={OrdersScreen} />


      <Tab.Screen name="Settings"
        options={{

          headerShown: false,


        }}

        component={SignInScreen5} />

    </Tab.Navigator>
  );
}




function SignInScreen2() {
  return (
    // <NavigationContainer>

    <Stack.Navigator >
      <Stack.Screen
        name="Home"
        component={Home}
        options={{

          title: 'Home',
          headerShown: false,
          headerStyle: {
            backgroundColor: '#e85b3d',
          },

          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      />



      <Stack.Screen
        name="AuthCheck"
        component={AuthCheck}
        options={{
          title: 'AuthCheck',
          headerShown: false,
          headerStyle: {
            backgroundColor: '#e85b3d',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      />



    </Stack.Navigator>
  )


}



const Stack = createNativeStackNavigator();

function MainNavigation() {
  return (
    <Provider store={store}>

      <NavigationContainer>

        <Stack.Navigator
          screenOptions={{
            headerShown: false
          }}
        >
          <Stack.Screen name="Auth" component={SignInScreen} />

          <Stack.Screen name="Auth1" component={SignInScreen3} />


          <Stack.Screen
            name="ProfileScreen"
            component={ProfileScreen}
            options={{
              title: 'ProfileScreen',
              headerShown: false,
              headerStyle: {
                backgroundColor: '#e85b3d',
              },
              headerTintColor: '#fff',
              headerTitleAlign: 'center',
              headerTitleStyle: {
                fontWeight: 'bold',
              },
            }}
          />
          <Stack.Screen name='AddCart' component={AddToCart} />
          <Stack.Screen name="Homes" component={TabNavigator}

            options={{
              title: 'Tabs',
              headerShown: false,
              headerStyle: {
                backgroundColor: '#e85b3d',
              },
              headerTintColor: '#fff',
              headerTitleAlign: 'center',
              headerTitleStyle: {
                fontWeight: 'bold',
              },
            }}
          />



          {/* <Stack.Screen name="AuthCheck" component={AuthCheck} />  */}

        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}

export default MainNavigation;
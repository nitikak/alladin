import React, { useEffect, useState } from 'react';
import {
    Text, TextInput, View, StyleSheet, Image, FlatList, Pressable, SafeAreaView, KeyboardAvoidingView, ImageBackground,
    ScrollView, useWindowDimensions, TouchableOpacity
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { useDispatch, useSelector } from 'react-redux';
import { AllParticularProduct, AllProduct } from '../../Redux/Action/productAction';
import Device from '../../Screens/Device';

import Colors from '../../Layout/Colors';
import { useNavigation } from '@react-navigation/native';

function ParticularCategories({ route }) {

    const dispatch = useDispatch();
    const navigation = useNavigation();


    const products = useSelector((state) => state.productsReducer.productData);


    useEffect(() => {
        const idProduct = route.params.id
        dispatch(AllParticularProduct(idProduct), navigation)
    }, [])
    return (

        <View style={styles.container}>
            <SafeAreaView>
                <View style={{ padding: 0, }}>
                    <View style={styles.UpperBlock}>
                        <TouchableOpacity onPress={() => navigation.goBack()}>
                            <Image style={styles.listIcon} source={require('../../assets/left-arrow.png')} />

                        </TouchableOpacity>

                        {
                            products && products.length > 0 ?
                                <Text style={{ marginLeft: 20, fontSize: 14, paddingTop: 5, color: '#fff', fontFamily: 'Montserrat-SemiBold' }}>{products[0].name}</Text>
                                : <View />
                        }
                        {/* <Text style={{marginLeft:wp(40),color:'#fff',fontSize:14}}>{products.length}</Text> */}

                    </View>
                    <View style={{ padding: 10, marginBottom: hp('20%') }}>

                        {
                            products.length > 0 ? <View>
                                <FlatList
                                    key={'_'}
                                    keyExtractor={item => "_" + item.id}
                                    data={products}
                                    numColumns={2}
                                    horizontal={false}
                                    renderItem={({ item, index }) => (
                                        <TouchableOpacity
                                            onPress={() => navigation.navigate('CategoryDetailScreen', {
                                                particularId: item.id,
                                                categoryId: item.product_category_id

                                            })}
                                            style={{ marginTop: hp(2), marginRight: wp('6%'), alignItems: 'center', justifyContent: 'center' }}>
                                            <Image resizeMode='contain' source={{ uri: item.product_image_url }} style={{ height: hp('25%'), width: wp('45%'), }} />
                                            <Text style={{ color: '#000', fontSize: 12, fontFamily: 'Montserrat-Bold' }}>{item.name}</Text>

                                            {/* <TouchableOpacity 
                                     onPress={() => addItemToCart(item)}
                                    style={{width:wp(40), backgroundColor:'red', paddingVertical:hp(2)}}>
                                        <Text> add to cat</Text>
                                    </TouchableOpacity> */}
                                        </TouchableOpacity>
                                    )}

                                />
                            </View> : <View>
                                    <Text style={{ textAlign: 'center', marginTop: 30 }}>No data available</Text>

                                </View>

                        }

                    </View>
                </View>
            </SafeAreaView>
        </View>
    )

}

export default ParticularCategories;

const styles = StyleSheet.create({

    container: {
        // flex:1
    },
    UpperBlock: {
        height: hp('7%'),
        backgroundColor: '#9066e6',
        //   justifyContent:'center',
        alignItems: 'center',
        flexDirection: 'row'

    },

    listIcon: {
        height: 20,
        width: 20,
        marginLeft: 10,
        // width:wp('10%')
    },
})
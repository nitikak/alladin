import React, {useState} from 'react';
import { Text, TextInput, View,StyleSheet,Image, Pressable,SafeAreaView,KeyboardAvoidingView,ImageBackground,
ScrollView,  useWindowDimensions,TouchableOpacity } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { useDispatch, useSelector } from 'react-redux';
import { AllProduct } from '../../Redux/Action/productAction';


function Header({navigation,term}){
    console.log('navigation', navigation)
    const [text, setText] = useState('');
    const [searchAsset, setSearchAsset] = useState('');
    const dispatch = useDispatch();


      const searchProduct = (text) => {
        setSearchAsset(text);
    
        if (text.length > 0) {
            console.log('abbbb', text)
            dispatch(AllProduct(text))
        } else {
            console.log('================>', text)

            dispatch(AllProduct())
        }
      };
    
    return(

        <View style={styles.container}>

           <View style={styles.headerContainer1}>
               
           <View style={styles.headerContainer}>
               {/* <View style={styles.drawerContainer}>
                  <Image  style={styles.burgerlogo} source={require('../../assets/hemburger.png')}/>
               </View> */}
               <View  style={styles.LogoContainer}>
                     <Image  style={styles.logo} source={require('../../assets/Logo.png')}/>
               </View>
               <View  style={styles.CartContainer}>
                    <Image  style={styles.cartlogo} source={require('../../assets/cart.png')}/>
                    <Image  style={styles.belllogo} source={require('../../assets/23.png')}/>
               </View>
           </View>


           <View style={styles.searchSection}> 
                    <Image style={styles.searchIcon} source={require('../../assets/search.png')}/>
                {/* <TextInput
                 // onChangeText={text => setUserName(text)}
                  style={styles.input}
                  placeholder={'Find Your Product'}
                /> */}
                 {/* <TextInput 
         style={styles.input}
        //   onChangeText={(text) => this.searchData(text)}
        //  value={this.state.searchText}
        onChangeText={text => SearchFilterFunction(text)}
        // onChangeText={text => setText(text)}
        value={text}
         underlineColorAndroid='transparent'
         placeholder={'Find Your Product'} /> */}
               <TextInput
            style={styles.input}
            underlineColorAndroid="rgba(0,0,0,0)"
            placeholder={'Find Your Product'}
            keyboardType="default"
            returnKeyType="done"
            autoCapitalize="none"
            value={searchAsset}
            onChangeText={(searchAsset) => searchProduct(searchAsset)}
          />

              </View>
              </View>
        </View>
    )

}

export default Header;

const styles = StyleSheet.create({

container:{
   // flex:1
},
headerContainer1:{
    backgroundColor:'#9066e6',
    height:hp('20%'),
   
},

headerContainer:{
    width:wp('95%'),
    height:hp('9%'),
    flexDirection:'row',
   alignSelf:'center',
    paddingTop:10,
   // backgroundColor:'yellow'

},
searchSection:{
    flexDirection: 'row',
    width:wp('90%'),
    backgroundColor:'#fff',
    borderWidth:1,
    borderColor: '#7c8791',
    borderRadius:5,
    paddingTop:3,
    paddingLeft:10,
    alignSelf:'center'
},
drawerContainer:{
    width:wp('15%'),
    justifyContent:'center',
    height:50,
   // backgroundColor:'red'
   
},
burgerlogo:{
    height:30,
    width:30
},
logo:{
  
    height:hp('7%'),
    width:wp('40%'),
    resizeMode:'contain',
  
  
    
        
},
cartlogo:{
    height:30,
    width:30,
    alignSelf:'center'
},
belllogo:{
    height:20,
    width:20,
    alignSelf:'center',
    marginTop:10
   
},
LogoContainer:{
    width:wp('75%'),
   // alignItems:'flex-start',
  // backgroundColor:'red',
  justifyContent:'center'
   
},
CartContainer:{
    width:wp('20%'),
    height:50,
  //  backgroundColor:'yellow',
    flexDirection:'row',
    justifyContent:'center',
   
},
searchIcon:{    

    marginTop:10,
    height:25,
    width:25,
 },


input: {
    width:'80%',
  fontFamily:'Montserrat-Regular',
  fontSize: 12,
  color: 'black',
 

},

})
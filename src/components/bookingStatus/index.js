import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, BackHandler, FlatList, Button, ScrollView, } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { SafeAreaView } from 'react-native-safe-area-context';
import DatePicker from 'react-native-date-picker'
import DateTimePicker from "@react-native-community/datetimepicker";
import moment from "moment";
import {useIsFocused, useNavigation} from '@react-navigation/native';


export default function BookingStatus(props) {
    // const navigation = useNavigation();
    const { notificationList, leftdayscolor, backgroundColor, negotation_image, navigation, screenName, comeFrom, titleColor, titleLeftColor, dateColor } = props;

  const [bookings, setBookings] = useState([
    {
      id: 1,
      color: '#2ea749',
      image: require('../../assets/Booking.png'),
      title: 'Completed Booking',
      number: '100',
    },
    {
      id: 2,
      color: '#f2ac00',
      image: require('../../assets/Booking.png'),
      title: 'Pending Booking',
      number: '50',
    },
    {
      id: 3,
      color: '#23a2b7',
      image: require('../../assets/Booking.png'),
      title: 'Approved Booking',
      number: '5',
    },
    {
      id: 4,
      color: '#157dfc',
      image: require('../../assets/Booking.png'),
      title: 'In Progress Booking',
      number: '140',
    },
    {
      id: 5,
      color: '#da3348',
      image: require('../../assets/Booking.png'),
      title: 'Cancelled Booking',
      number: '21',
    },
    
    
  ]);


  return (
    <View style={styles.container}>
      <View style={{ backgroundColor: '#9066e6', paddingVertical: hp(2), flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Image resizeMode='contain' source={require('../../assets/left-arrow.png')} style={{ height: hp(3), width: wp(8), marginRight: wp(6), marginLeft: wp(2) }} />
          </TouchableOpacity>
          <Text style={{ fontSize: 14, color: '#fff', fontFamily: "Montserrat-Bold" }}>DashBoard</Text>
        </View>
      </View>
      <ScrollView contentContainerStyle={{paddingBottom:hp(2)}}>

      <View style={{ marginLeft: wp(1) }}>
        <Text style={{ fontSize: 14, marginLeft: wp(3), fontFamily: "Montserrat-Bold", color: '#000', marginVertical: hp(2) }}>Total Booking : 0</Text>
        <FlatList
          data={bookings}
          keyExtractor={(item, index) => index}
          // horizontal={false}
          numColumns={3}
          renderItem={({ item, index }) => (
            <View style={{ justifyContent: 'center', marginLeft: wp(2), marginTop: hp(1) }}>
              <TouchableOpacity onPress={() => props.navigation.navigate('BookingServices')} style={{ height: hp(16), width: wp(30), backgroundColor: item.color, alignItems: 'center', borderRadius: 4, justifyContent: 'center' }}>
                <Image source={item.image} resizeMode='contain' style={{ height: hp(5), width: wp(10), }} />
                <Text> {item.title}</Text>
                <Text style={{ color: '#fff', fontSize: 15, fontFamily: "Montserrat-Bold", marginTop: hp(2) }}>{item.number}</Text>
              </TouchableOpacity>
              <Text style={{ fontFamily: "Montserrat-Bold", fontSize: 10, color: '#000', textAlign: 'center' }}>{item.title}</Text>
            </View>
          )}
        />
      </View>

        <View>
         
      </View>

      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#f7f5f1',},
  containerText: { fontSize: 27, color: '#000' },
});

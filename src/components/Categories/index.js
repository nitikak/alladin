import React, { useEffect, useState } from 'react';
import {
  Text, TextInput, View, StyleSheet, Image, FlatList, Pressable, SafeAreaView, KeyboardAvoidingView, ImageBackground,
  ScrollView, useWindowDimensions, TouchableOpacity
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { useDispatch, useSelector } from 'react-redux';
import { AllProduct } from '../../Redux/Action/productAction';
import Device from '../../Screens/Device';

import Colors from '../../Layout/Colors';
import { useNavigation } from '@react-navigation/native';

function Category() {

  const dispatch = useDispatch();
  const navigation = useNavigation();

  const data = useSelector((state) => state.productsReducer.getCartData);
  console.log('data==================>', data)

  // const ServiceDetails = (id,slug) => {

  //     dispatch(AllProduct(id,slug,'',navigation));

  //   }


  useEffect(() => {
    dispatch(AllProduct())
  }, [])

  return (

    <View style={styles.container}>
      <SafeAreaView>
        <View style={{ padding: 0, }}>
          <View style={styles.UpperBlock}>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <Image style={styles.listIcon} source={require('../../assets/left-arrow.png')} />

            </TouchableOpacity>
            <Text style={{ marginLeft: 20, fontSize: 14, paddingTop: 5, color: '#fff', fontFamily: 'Montserrat-SemiBold' }}>All Products</Text>


          </View>
          <View style={{ padding: 10, marginBottom: hp('20%') }}>


            <FlatList
              key={'_'}
              keyExtractor={item => "_" + item.id}
              data={data}
              numColumns={2}
              horizontal={false}
              renderItem={({ item, index }) => (
                <TouchableOpacity onPress={() => navigation.navigate('ParticularCategories', {
                  id: item.id,

                })}
                  style={{ marginTop: hp(2), marginRight: wp('6%'), alignItems: 'center', justifyContent: 'center' }}>
                  <Image resizeMode='contain' source={{ uri: item.category_image_url }} style={{ height: hp('23%'), width: wp('45%'), }} />
                  <Text style={{ color: '#000', fontSize: 12, fontFamily: 'Montserrat-Bold' }}>{item.name}</Text>
                </TouchableOpacity>
              )}

            />

            {/* :
 
 <Text style={{textAlign:'center',marginTop:30}}>No data available</Text>
 
 } */}


          </View>
        </View>
      </SafeAreaView>

    </View>
  )

}

export default Category;

const styles = StyleSheet.create({

  container: {
    // flex:1
  },
  UpperBlock: {
    height: hp('7%'),
    backgroundColor: '#9066e6',
    //   justifyContent:'center',
    alignItems: 'center',
    flexDirection: 'row'

  },
  listIcon: {
    height: 20,
    width: 20,
    marginLeft: 10
  },
  catBox: {
    height: 80,
    width: 80,
    backgroundColor: 'black',
    justifyContent: 'center',
    borderRadius: 5,
    // marginRight: 5
  },
  caticon: {

    height: 50,
    borderRadius: 25,
    width: wp('90%'),
    padding: 90,
    alignSelf: 'center',
    backgroundColor: '#fff'
  },

  ServiceText: {
    color: '#000',
    textAlign: 'left',
    fontSize: 14,
    paddingLeft: 20,
    paddingTop: 15,
    fontFamily: 'Montserrat-SemiBold'
  },
  searchSection: {
    flexDirection: 'row',
    width: wp('90%'),
    backgroundColor: '#fff',
    //borderWidth:1,
    //  borderColor: '#7c8791',
    borderRadius: 5,
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 10,
    alignSelf: 'center',
    marginTop: 10,
    marginBottom: 10
  },
  searchIcon: {

    marginTop: 10,
    height: 25,
    width: 25,
  },


  input: {
    width: '80%',
    fontFamily: 'Montserrat-Regular',
    fontSize: 12,
    color: 'black',


  },
  PriceText: {
    color: '#9066e6',
    textAlign: 'left',
    fontSize: 14,
    paddingLeft: 20,

    fontFamily: 'Montserrat-SemiBold'
  },
  discountText: {
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid',
    color: '#000',
    textAlign: 'left',
    fontSize: 14,
    paddingLeft: 20,

    fontFamily: 'Montserrat-Regular'
  },
  button: {
    marginTop: 30,
    marginHorizontal: 40,
    backgroundColor: '#9066e6',
    padding: 10,
    width: wp('40%'),
    borderRadius: 10,
    alignSelf: 'center'

  },
  buttonText: {
    color: 'white',
    textAlign: 'center',
    fontFamily: 'Montserrat-Regular',
    //  textTransform:'uppercase'
  },
  UpperBlock: {
    height: hp('7%'),
    //  width:wp('95%'),

    backgroundColor: '#9066e6',
    //   justifyContent:'center',
    alignItems: 'center',
    //  alignSelf:'center',
    flexDirection: 'row'

  },
  OTPContainer: {
    width: wp('90%'),
    marginTop: 20,
    alignSelf: 'center'
  },

  listIcon: {
    height: 20,
    width: 20,
    marginLeft: 10,
    // width:wp('10%')
  },
  filterIcon: {
    height: 20,
    width: 20,
    marginLeft: 15,
    //  alignSelf:'flex-end',
    width: wp('10%')

  },

  HomeText: {
    color: '#9066e6',
    fontSize: 24,
    fontFamily: 'Montserrat-Bold'
    // alignSelf:'center'
  },
  content: {
    //  padding: 10,
    marginTop: 20,
  },
  tabViewContainer: {
    //  marginVertical: 50,
    height: Device.height * 0.20,
  },
  maxHeight: { height: '100%' },
  tabBar: {
    height: 50,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  tabItem: {
    width: '50%',
    height: 50,
    backgroundColor: Colors.white,
    justifyContent: 'center',
    alignItems: 'center',
  },
  tabTitle: {
    fontSize: 15,
    color: Colors.black,
  },



})
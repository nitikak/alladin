import React, {useEffect, useState} from 'react';
import { Text, TextInput, View,StyleSheet,Image, Pressable,ActivityIndicator,SafeAreaView,KeyboardAvoidingView,ImageBackground,
ScrollView,  useWindowDimensions,TouchableOpacity,Keyboard } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import OTPInputView from '@twotalltotems/react-native-otp-input'
import RNOtpVerify from 'react-native-otp-verify';
import {verifyOtp} from '../../Redux/Action/SendOtp'
import { useDispatch,useSelector } from 'react-redux';


const  OTP = ({navigation}) => {

    const [otp,setOtp] = useState('');
    const [loading,setLoading] = useState(false);

  //   useEffect(()=>{
 
  //  // setLoading(true);

  //       RNOtpVerify.getHash()
  //       .then(console.log)
  //       .catch(console.log);
     
  //       RNOtpVerify.getOtp()
        
  //       .then(p => RNOtpVerify.addListener(otpHandler))
  //       .catch(p => console.log('>>>>>>>>>',p));
      
       
  //        return () => {
        
  //           RNOtpVerify.removeListener();

  //        }
     
  //   })

//      const otpHandler = (message) => {
    
//         //console.log('message:',message)
//         const otp = /(\d{4})/g.exec(message)[1];
//         console.log('OTp:',otp);
//         setOtp(otp);
//         console.log('OTp:',otp);
//         RNOtpVerify.removeListener();
//         Keyboard.dismiss();
       
// }


    return(

        <View style={styles.otpsearchSection}> 
         {(loading) &&
                    <View style={{ flex: 1, justifyContent: 'center', position: 'absolute', top: '50%', left: '40%' }}>

                        <ActivityIndicator

                            
                            size="large"
                            style={{
                                backgroundColor: "rgba(20,116,240,.8)",
                                height: 80,
                                width: 80,
                                zIndex: 999,
                                borderRadius: 15
                            }}
                            size="small"
                            color="#ffffff"
                        />
                    </View>}
        <Text style={{textAlign:'center',padding:25}}>Enter your OTP number</Text>
                <OTPInputView
                style={{width: '60%', height: 50,alignSelf:'center',}}
                pinCount={4}

                code={otp} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
                onCodeChanged = {otp =>  setOtp(otp)}
                autoFocusOnLoad
                codeInputFieldStyle={styles.underlineStyleBase}
                codeInputHighlightStyle={styles.underlineStyleHighLighted}
                // onCodeFilled = {(code => {
                //     console.log(`Code is ${code}, you are good to go!`)
                // })}
                />
          
        </View>
       

    )
}

export default OTP;

const styles = StyleSheet.create({
   
    borderStyleHighLighted: {
      borderColor: "#03DAC6",
    },
   
    underlineStyleBase: {
      width: 45,
      height: 45,
      borderWidth: 0,
      borderBottomWidth: 1,
      color:'#000',
      borderRadius:5,
      backgroundColor:'#fff'
    },
    
   
    underlineStyleHighLighted: {
      borderColor: "#03DAC6",
    },
   
  });
  
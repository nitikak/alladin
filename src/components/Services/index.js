import React, { useEffect, useState } from 'react';
import {
    Text, TextInput, View, StyleSheet, Image, Pressable, SafeAreaView, KeyboardAvoidingView, ImageBackground,
    ScrollView, useWindowDimensions, TouchableOpacity, FlatList, ActivityIndicator
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { useIsFocused, useNavigation } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import { CompanyList, GetServices } from '../../Redux/Action/ServiceAction';

function Services({ }) {
    const navigation = useNavigation();
    const { Services, companies, loading } = useSelector(state => state.Services);

    const dispatch = useDispatch();


    const Companies = (Id) => {
        console.log('??????????????', Id)

        dispatch(CompanyList(Id, '', navigation))

    }

    useEffect(() => {
        dispatch(GetServices())

    }, [])

    return (

        <View style={styles.container}>
            <View style={{ alignItems: "center", paddingBottom: 20 }}>
                {(loading) &&
                    <View style={{ flex: 1, justifyContent: 'center', position: 'absolute', top: '30%', left: '40%' }}>

                        <ActivityIndicator


                            size="large"
                            style={{
                                //  backgroundColor: "rgba(144,102,230,.8)",
                                height: 80,
                                width: 80,
                                zIndex: 999,
                                borderRadius: 15
                            }}
                            size="small"
                            color="rgba(144,102,230,.8)"
                        />
                    </View>}
                <FlatList
                    columnWrapperStyle={{ justifyContent: 'space-between', }}
                    data={Services}
                    keyExtractor={(item, index) => index}
                    horizontal={false}
                    numColumns={3}
                    renderItem={({ item, index }) => (

                        <TouchableOpacity style={styles.catBox}

                            onPress={() => Companies(item.id)}

                        >
                            <Image style={styles.caticon} source={{ uri: item.category_image_url }} />
                            <Text style={styles.ServiceText}>{item.name}</Text>

                        </TouchableOpacity>

                    )}

                />

                {/* <ScrollView>
                <View style={{padding:10,flexDirection:'row',alignItems:'center',width:wp('90%'),justifyContent:'center'}}>
              
                    <View style={styles.catBox}>
                        <Image style={styles.caticon} source={require('../../assets/6.png')}/>
                        <Text style={styles.ServiceText}>All Products</Text>
                    </View>
                    <View style={styles.catBox}>
                    <Image style={styles.caticon} source={require('../../assets/7.png')}/>
                        <Text style={styles.ServiceText}>All Products</Text>
                    </View>
                    <View style={styles.catBox}>
                    <Image style={styles.caticon} source={require('../../assets/8.png')}/>
                        <Text style={styles.ServiceText}>All Products</Text>
                    </View>
                  </View>
                  
              

                  <View style={{padding:10,flexDirection:'row',alignItems:'center',width:wp('90%'),justifyContent:'center'}}>
              
                    <TouchableOpacity style={styles.catBox} onPress={() => navigation.navigate('OtpScreen')}>
                        <Image style={styles.caticon} source={require('../../assets/9.png')}/>
                        <Text style={styles.ServiceText}>All service</Text>
                    </TouchableOpacity>
                    <View style={styles.catBox}>
                    <Image style={styles.caticon} source={require('../../assets/10.png')}/>
                        <Text style={styles.ServiceText}>All Products</Text>
                    </View>
                    <View style={styles.catBox}>
                    <Image style={styles.caticon} source={require('../../assets/11.png')}/>
                        <Text style={styles.ServiceText}>All Products</Text>
                    </View>
                  
                  
                  </View>  

                  <View style={{padding:10,flexDirection:'row',alignItems:'center',width:wp('90%'),justifyContent:'center'}}>
              
                    <View style={styles.catBox}>
                        <Image style={styles.caticon} source={require('../../assets/12.png')}/>
                        <Text style={styles.ServiceText}>All service</Text>
                    </View>
                    <View style={styles.catBox}>
                    <Image style={styles.caticon} source={require('../../assets/13.png')}/>
                        <Text style={styles.ServiceText}>All Products</Text>
                    </View>
                    <View style={styles.catBox}>
                    <Image style={styles.caticon} source={require('../../assets/14.png')}/>
                        <Text style={styles.ServiceText}>All Products</Text>
                    </View>
                  
                  
                  </View>  

                </ScrollView> */}
            </View>
        </View>
    )

}

export default Services;

const styles = StyleSheet.create({

    container: {
        // flex:1
        marginBottom: hp(4)
    },
    catBox: {
        height: 100,
        width: 80,

        //justifyContent:'center',
        marginRight: 15,
        marginLeft: 15,
        marginBottom: 10

        //alignSelf:'center',
        // backgroundColor:'yellow'
    },
    caticon: {
        borderWidth: 1,
        borderColor: '#c8c8c8',
        height: 50,
        borderRadius: 5,
        width: 50,
        padding: 30,
        alignSelf: 'center'
    },
    ServiceText: {
        color: '#000',
        textAlign: 'center',
        fontSize: 10,
        paddingTop: 10,
        fontFamily: 'Montserrat-Regular'
    }




})
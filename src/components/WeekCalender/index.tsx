import {addDays, format, getDate, isSameDay, startOfWeek} from 'date-fns';
import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';

type Props = {
  date: Date;
  onChange: (value: Date) => void;
};

const WeekCalendar: React.FC<Props> = ({date, onChange}) => {
  const [week, setWeek] = useState<WeekDay[]>([]);

  useEffect(() => {
    const weekDays = getWeekDays(date);
    setWeek(weekDays);
  }, [date]);

  return (
    <View style={styles.container}>
      {week.map((weekDay) => {
        const textStyles = [styles.label];
        const touchable = [styles.touchable];

        const sameDay = isSameDay(weekDay.date, date);
        if (sameDay) {
          textStyles.push(styles.selectedLabel);
          touchable.push(styles.selectedTouchable);
        }

        return (
          <TouchableOpacity style={{alignItems: 'center',
          borderWidth:1,
          borderColor: '#c2c2c2',
          borderRadius: 3,marginLeft:7, backgroundColor: sameDay ? '#9066e6': '#fff',...touchable}}  onPress={() => onChange(weekDay.date)}>
          {/* // style={styles.weekDayItem}> */}
          
            <View
              // onPress={() => onChange(weekDay.date)}
              style={touchable}
              >
              <Text style={textStyles}>{weekDay.day}</Text>
            </View>
            <Text style={{color: sameDay ? '#fff': '#000' ,
    marginBottom: 5,}} 
           >{weekDay.formatted}</Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingVertical: 10,
  },
  weekDayText: {
    color: 'gray',
    marginBottom: 5,
  },
  label: {
    fontSize: 14,
    color: 'black',
    textAlign: 'center',
  },
  selectedLabel: {
    color: 'white',
  },
  touchable: {
    borderRadius: 20,
    padding: 7.5,
    height: 35,
    width: 35,
  },
  selectedTouchable: {
    backgroundColor: '#9066e6',
  },
  weekDayItem: {
    alignItems: 'center',
    borderWidth:1,
    borderColor: '#000',
    borderRadius: 3,
  },
});

type WeekDay = {
  formatted: string;
  date: Date;
  day: number;
};

// get week days
export const getWeekDays = (date: Date): WeekDay[] => {
  const start = startOfWeek(date, {weekStartsOn: 1});

  const final = [];

  for (let i = 0; i < 7; i++) {
    const date = addDays(start, i);
    final.push({
      formatted: format(date, 'EEE'),
      date,
      day: getDate(date),
    });
  }

  return final;
};

export default WeekCalendar;

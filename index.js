/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import MainNavigation from './src/Navigation/MainNavigation'

AppRegistry.registerComponent(appName, () => MainNavigation);
